# Sample Models for the ModelSolver Toolkit #

This project contains sample models to demonstrate the Solver Toolkit.

The best starting point is the minimal implementation of the *Krusell & Smith (98)* model in the calibration of *Den Haan et al., 2010*. It can be found in

~~~~
src/main/scala/com/meliorbis/economics/incompletemarkets/IM_Minimal.scala
~~~~

or, if you are reading this online, [here](https://bitbucket.org/modelsolver/incomplete-markets-scala/src/4e1e97e1a33a54a30aee14929ddbe3d747a74048/src/main/scala/com/meliorbis/economics/incompletemarkets/IM_Minimal.scala?fileviewer=file-view-default).

The models discussed in Chapter's 2 and 3 are these:

- Explicit Aggregation
~~~~
src/main/scala/com/meliorbis/economics/incompletemarkets/IncompleteMarketsXPA.scala
~~~~
- Derivate Aggregation, including Representative Agent
~~~~
src/main/scala/com/meliorbis/economics/incompletemarkets/IncompleteMarkets.scala
~~~~
- Bonds
~~~~
src/main/scala/com/meliorbis/economics/incompletemarkets/bonds/IncompleteMarketsBonds.scala
~~~~
## Solving Models & Evaluating Results in the Pre-Compiled Package

### Solving
If you downloaded the pre-compiled package or already build the package as specific below, change directory to 

~~~~
src/main/R
~~~~

and run *solveModels.R*. It can be run non-interactively to solve all models/ For example, on a unix-like system:

~~~~
R --save < solveModels.R
~~~~

will calculate all the solutions and place results in the directory *Solutions*.

It can also executed in an R console and will allow selection of the model to solve.

### Evaluating Results

Once all models are solved, *evaluate.R*. This calculates the statistics and generates tables and graphs used in the paper & chapter 3.

~~~~
R --save < evaluate.R
~~~~

For chapter 2, there is some Matlab code to run the Dynare model and compute the statistics and graphs in

~~~~
src/main/matlab/chapter1
~~~~ 

## Compiling and Building

To compile the code and build the runnable package *maven* must be installed. In the root directory of the project call:

~~~~
mvn clean package
~~~~