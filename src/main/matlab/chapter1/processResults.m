% Calculate statistics for 10k-period ahead
disp('---------')
disp('-- XPA --')
disp('---------')
% Make sure the solutions are accessible at this path, for instance
% with a simlink
simAgg = load('Solutions/XPA/simAgg.mat');
sim = load('Solutions/XPA/sim.mat');
results.state = load('Solutions/XPA/state.mat');
results.states = sim.states;
results.aggStates = simAgg.states;
results.shocks = sim.shocks;
% add the parent directory's absolute path to the matlab path
% to make this work
checkResults(results);

clear
cd XPA
%ExlpicitAggr
load solved
cd ..
state = load('Solutions/XPA/state.mat');
kpSolver = flipdim(flipdim(permute(state.indTransition,[1,4,3,2,5]),4),5);
kpDiffs = (kpSolver-kpmat)./(1+kpmat);
fprintf('Max Diff kp: %g\n',max(abs(reshape(kpDiffs,numel(kpDiffs),1))));
fprintf('Mean Diff kp: %g\n',mean(abs(reshape(kpDiffs,numel(kpDiffs),1))));

KpSolver = flipdim(permute(state.aggTransition,[2,1,3,4]),3);
KpMat = cat(4,Kpu,Kpe);
KpDiffs = (KpSolver-KpMat)./(1+KpMat);
fprintf('Max Diff Kp: %g\n',max(abs(reshape(KpDiffs,numel(KpDiffs),1))));
fprintf('Mean Diff Kp: %g\n',mean(abs(reshape(KpDiffs,numel(KpDiffs),1))));
