function [ dynKp ] = constructDynareTransition( modelStateDir, oo_ )
%CONSTRUCTDYNARETRANSITION Summary of this function goes here
%   Detailed explanation goes here
    Kp = readFormattedCSV([modelStateDir '/Kp_agg.csv']);
    K = readFormattedCSV([modelStateDir '/K_agg.csv']);
    zLevels = log(readFormattedCSV([modelStateDir '/Z_agg.csv']));
    
    dynKp = zeros(size(Kp));
    
    order = 1 + isfield(oo_.dr, 'ghxx');
    
    for kInd = 1:length(K)
        for zInd = 1:length(zLevels)
            simState = simult_([0;K(kInd);0],oo_.dr,zLevels(zInd),order);
            dynKp(kInd,zInd) = simState(2);
        end
    end
end

