parameters alph gam delt bet aa G rho;
trend_var (growth_factor=G) z;
var (deflator=z) c k;
var x;
varexo e;

alph=1/3;
gam=1/2;
delt=0.025;
bet=1/0.98-1;
aa=1;
G = 1.025^(1/4);
rho = 0.979;


model;
c + k - aa*exp(x)*k(-1)^alph*z(-1)^(1-alph) - (1-delt)*k(-1);
c^(-gam) - (1+bet)^(-1)*(aa*alph*exp(x(+1))*k^(alph-1)*(z)^(1-alph) + 1 - delt)*c(+1)^(-gam);
x = x(-1)*rho + e;
end;

initval;
e = 0;
x = 0;
k = ((delt+bet)/(1.0*aa*alph))^(1/(alph-1));
c = aa*k^alph-delt*k;
end;

steady;

check;

shocks;
var e; stderr 0.0072;
end;

stoch_simul(order=2,nocorr,nomoments,IRF=0);
