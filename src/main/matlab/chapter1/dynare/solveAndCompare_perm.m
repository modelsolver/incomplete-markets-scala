alph=1/3;
gam=1/2;
delt=0.025;
bet=0.98;
aa=1;
G = 1.025^(1/4);
rho = 0.979;
P_0 = 1;
l = 1;

k_0 = P_0 * l * ((G^gam - bet*(1-delt))/(alph*bet))^(1/(alph-1));

fprintf('Stead-State k: %s\n',k_0);

save('ssK','k_0')

dynare ramst_perm
state = load('/Users/toby/git/economics/modelling/DemoSolutions/Solutions/RA_perm/state.mat');
simRes = load('/Users/toby/git/economics/modelling/DemoSolutions/Solutions/RA_perm/sim.mat');
shocks = log(simRes.shocks);
elementaryShocks = shocks(:,:) - [[0;shocks(1:9998,1)],zeros(9999,1)]*0.979;
dynAggs = simult_(oo_.dr.ys,oo_.dr,elementaryShocks,2);
figure(1);
plot(dynAggs(2,1:2000)./simRes.states(1:2000)'-1)
figure(2);
plot([dynAggs(2,1:2000)' simRes.states(1:2000)])

adjustBy = mean(simRes.states'./dynAggs(2,1:9999));

fprintf('Bias (%%): %s\n',(mean(dynAggs(2,1:9999)./simRes.states'-1)*100));
fprintf('Mean Abs Error: (%%): %s\n',(mean(abs(dynAggs(2,1:9999)./simRes.states()'-1))*100));

ssAggs= simult_(oo_.dr.ys,oo_.dr,zeros(10000,2),2);
fprintf('Dynare stoch steady state diff (%%): %s\n',(ssAggs(2,10000)/oo_.dr.ys(2)-1)*100);

simZeroRes = load('/Users/toby/git/economics/modelling/DemoSolutions/Solutions/RA_perm/simZeros.mat');
fprintf('Solver stoch steady state diff (%%): %s\n',(simZeroRes.states(9999)/oo_.dr.ys(2)-1)*100);

adjusted = dynAggs(2,1:9999)*adjustBy;

plot([adjusted(1:2000)'./simRes.states(1:2000)-1])

fprintf('Bias (%%): %s\n',(mean(adjusted./simRes.states'-1)*100));
fprintf('Abs Error: (%%): %s\n',(mean(abs(adjusted./simRes.states()'-1))*100));

denormalizedStates = simRes.states;
periods = length(simRes.states);
for i=1:periods
    denormalizedStates(i:periods) = denormalizedStates(i:periods)*G*simRes.shocks(i,2);
end
