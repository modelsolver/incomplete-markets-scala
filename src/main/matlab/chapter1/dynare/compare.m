function [ relDiff, relDiffShocks] = compare( simDir ,oo_,zLevels)
    % COMPARE - Reads the results from multiple simulations from the
    % supplied simulation directory 'simDir' and then simulates them using
    % a solved dynare model
    allAggs = readFormattedCSV([simDir '/states.csv']);
    allShocks = readFormattedCSV([simDir '/shocks.csv']);
    
    allDynAggs = zeros(size(allAggs));
    allDynShocks = zeros(size(allShocks));
    
    % Determine the order of the approximation
    order = 1 + isfield(oo_.dr, 'ghxx');
    initState = oo_.dr.ys;
    
    for runIndex = 1:size(allAggs,2)
        initState(2) = 17.9831;
        shocks = log(zLevels(allShocks(:,runIndex)+1));
        elementaryShocks = shocks - [0;shocks(1:(length(shocks)-1))]*0.979;
        dynAggs = simult_(initState,oo_.dr,elementaryShocks,order);
        allDynAggs(:,runIndex) = dynAggs(2,1:size(allAggs,1))';
        allDynShocks(:,runIndex) = dynAggs(3,1:size(allAggs,1))';
    end
    
    relDiff = (allDynAggs(1:size(allDynAggs,1),:)./allAggs(1:size(allDynAggs,1),:)-1);
    relDiffShocks = allDynShocks(1:size(allDynAggs,1),:)-log(zLevels(allShocks(1:size(allDynAggs,1),:)+1));
    
end

