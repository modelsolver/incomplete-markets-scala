% Adjust this for your local environment
addpath /usr/local/opt/dynare/lib/dynare/matlab

dynare ramst% Can't do this prior to dynare because it clears the variables

alph=1/3;
gam=1/2;
delt=0.025;
bet=0.98;
aa=1;
G = 1.025^(1/4);
rho = 0.979;
P_0 = 1;
l = 1;

k_0 = P_0 * l * ((G^gam - bet*(1-delt))/(alph*bet))^(1/(alph-1));

log(17.98110995946777/k_0)
fprintf('Stead-State k: %s\n',k_0);

save('ssK','k_0')

baseDir = '/Users/toby/git/economics/modelling/scala/ScalaIncompleteMarkets';
start = 1000;

state = load([baseDir '/Solutions/RA/state.mat']);
simRes = load([baseDir '/Solutions/RA/sim.mat']);
shocks = log(simRes.shocks);
elementaryShocks = shocks(:,1) - [0;shocks(1:9998,1)]*0.979;

% Calculate the transition functions at 0 shock level for both 
% dynare and Solver 

% First, calculate for both 1st and 2nd order in dynare, at the points on
% the solver aggregate capital grid
dynTrans1 = oo_.dr.ys(2) +  oo_.dr.ghx(1,1)*(state.aggStates.X_1-oo_.dr.ys(2));
dynTrans2 = dynTrans1 + (oo_.dr.ghs2(1) + oo_.dr.ghxx(1,1)*(state.aggStates.X_1-oo_.dr.ys(2)).^2)/2;

dynTransShocks1 = oo_.dr.ys(2) +  oo_.dr.ghx(1,2)*(log(state.aggShocks.Z_1));
dynTransShocks2 = dynTransShocks1 + (oo_.dr.ghs2(1) + oo_.dr.ghxx(1,4)*(log(state.aggShocks.Z_1)).^2)/2;


% The solver soltuion is already in array form
solverTrans = state.aggTransition(:,11);
solverTransShocks = state.aggTransition(16,:)';

dynTransGlobal = zeros(length(state.aggStates.X_1), length(state.aggShocks.Z_1));

for i = 1:length(state.aggStates.X_1)
    
    Krel = state.aggStates.X_1(i)-oo_.dr.ys(2);
    
    for j = 1:length(state.aggShocks.Z_1)
        
        Z = log(state.aggShocks.Z_1(j));
        
        currentStates = [Krel;Z];
        
        dynTransGlobal(i,j) = oo_.dr.ys(2)  + oo_.dr.ghx(1,:)*currentStates +(oo_.dr.ghs2(1)  + oo_.dr.ghxx(1,:) * kron(currentStates,currentStates))/2;
    end
end

dyn2ss = interp1(dynTrans2-state.aggStates.X_1,state.aggStates.X_1,0)

solverSS = interp1(solverTrans-state.aggStates.X_1,state.aggStates.X_1,0)

% calculate the %-age deviation of the 2nd order dynare from 1st order, and
% save
relTrans = log(dynTrans2./dynTrans1)'*100;
save('dynTrans', 'relTrans');

% calculate the %-age deviation of the solver from 1st order dynare, and
% save
relTrans = log(solverTrans./dynTrans1)'*100;
save('solverTrans', 'relTrans');

relTrans = log(dynTransShocks2./dynTransShocks1)'*100;
save('dynTransShocks', 'relTrans');


relTrans = log(solverTransShocks./dynTransShocks1)'*100;
save('solverTransShocks', 'relTrans');

% Also save the grid points used
Kpoints = log(state.aggStates.X_1./k_0);
save('Kpoints','Kpoints');

Zpoints = state.aggShocks.Z_1;
save('Zpoints','Zpoints');

solverFixed = interp1(solverTrans-state.aggStates.X_1,state.aggStates.X_1,0);
log(solverFixed/k_0)*100

dynFixed = interp1(dynTrans2-state.aggStates.X_1,state.aggStates.X_1,0);
log(dynFixed/k_0)*100

%plot(state.aggStates.X_1,[log(dynTrans2./dynTrans1)*100 log(solverTrans./dynTrans1)*100])

% Simulate the elementary shocks derived from the solver simulation in
% dynare, using 2-nd order approximation
dynAggs = simult_(oo_.dr.ys,oo_.dr,elementaryShocks,2);
range = 1:9999;
diffs = log(simRes.states(range)'./dynAggs(2,range))*100;
save('relDiffs.mat','diffs');
% plot([diffs]')
% figure(2);
% plot([dynAggs(2,start:9900)' simRes.states(start:9900)])

adjustBy = mean(simRes.states'./dynAggs(2,1:9999));


print(diffs(1:10));

fprintf('Bias (%%): %s\n',mean(diffs));
fprintf('Mean Abs Error: (%%): %s\n',mean(abs(diffs)));

figure(3);

scatter(log(simRes.states(range-1)/17.983128815303306),diffs,'.');
hold on;
scatter(log(simRes.states(range-1)/17.983128815303306),diffs1,'.g');
hold off;
%scatter(simRes.states(start:9998),log(dynAggs(2,(start+1):9999)./simRes.states((start+1):9999)')*100,'.');
figure(4);
scatter(shocks(start:9998),log(dynAggs(2,(start+1):9999)./simRes.states((start+1):9999)')*100,'.');

figure(5);

scatter(simRes.states(range-1),simRes.states(range),'.');
hold on;
scatter(dynAggs(2,range-1),dynAggs(2,range),'.g');
scatter(dynAggs1(2,range-1),dynAggs1(2,range),'.r');
hold off;

% ssAggs= simult_(oo_.dr.ys,oo_.dr,zeros(10000,1),2);
% fprintf('Dynare stoch steady state diff (%%): %s\n',(ssAggs(2,10000)/oo_.dr.ys(2)-1)*100);
% 
% simZeroRes = load('../Solutions/RA/simZeros.mat');
% fprintf('Solver stoch steady state diff (%%): %s\n',(simZeroRes.states(9999)/oo_.dr.ys(2)-1)*100);
% 
% adjusted = dynAggs(2,1:9999)*adjustBy;
% 
% plot([adjusted(1:2000)'./simRes.states(1:2000)-1])
% 
% fprintf('Bias (%%): %s\n',(mean(adjusted./simRes.states'-1)*100));
% fprintf('Abs Error: (%%): %s\n',(mean(abs(adjusted./simRes.states()'-1))*100));


