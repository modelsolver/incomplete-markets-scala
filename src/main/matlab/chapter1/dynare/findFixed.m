function [ f ] = findFixed( x, y )
    for i=1:(length(x)-1)
        % Are the ys on different sides of f(x) = x?
        if (y(i+1)-x(i+1))*(y(i)-x(i)) < 0
            f = (y(i)*x(i+1)-y(i+1)*x(i))/(x(i+1)-x(i)-y(i+1)+y(i));
            return;
        end
    end
    f = NaN;
end

