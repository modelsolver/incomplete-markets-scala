jarPath='../../../../target/IncompleteMarkets-0.0.1-SNAPSHOT-jar-with-dependencies.jar';

executions = {
    'XPA', '-xpa -stateDir Solutions/XPA -solve -dhdata -writeMatlab -simDir sim';
    'No-Agg Risk', '-da -solve -dhdata -writeMatlab -noAgg -stateDir Solutions/KS_NA';
    'Derivative Aggregation','-da -solve -simulate -dhdata -writeMatlab -initialState Solutions/KS_NA  -stateDir Solutions/KS -gradSimSteps 0 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Derivative Aggregation, grad. sim','-da -solve -simulate -dhdata -writeMatlab -initialState Solutions/KS_NA  -stateDir Solutions/KS_grad -gradSimSteps 24 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Derivative Aggregation, dTheta','-da -solve -simulate -dhdata -dtheta -writeMatlab -initialState Solutions/KS_NA  -stateDir Solutions/KS_dtheta -gradSimSteps 24 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Derivative Aggregation, 2nd-order','-da -solve -simulate -dhdata -secondOrder -writeMatlab -initialState Solutions/KS_NA  -stateDir Solutions/KS_2nd -gradSimSteps 0 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Derivative Aggregation, 2nd-order + dtheta','-da -solve -simulate -secondOrder -dtheta -dhdata -writeMatlab -initialState Solutions/KS_NA  -stateDir Solutions/KS_2nd_dtheta -gradSimSteps 24 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'No-Agg Risk, Large Shocks', '-da -solve -dhdata -writeMatlab -noAgg -largeShocks -stateDir Solutions/KS_NA_shocks';
    'Large Shocks','-da -solve -simulate -dhdata -writeMatlab -largeShocks -initialState Solutions/KS_NA_shocks  -stateDir Solutions/KS_shocks -gradSimSteps 0 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Large Shocks, grad. sim.','-da -solve -simulate -dhdata -writeMatlab -largeShocks -initialState Solutions/KS_NA_shocks  -stateDir Solutions/KS_shocks_grad -gradSimSteps 24 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Large Shocks, dtheta','-da -solve -simulate -dhdata -writeMatlab -largeShocks -initialState Solutions/KS_NA_shocks  -stateDir Solutions/KS_shocks_dtheta -gradSimSteps 24 -dtheta -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Large Shocks, 2nd order','-da -solve -simulate  -dtheta -writeMatlab -largeShocks -initialState Solutions/KS_NA_shocks  -secondOrder -stateDir Solutions/KS_shocks_2nd -gradSimSteps 0 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';
    'Large Shocks, 2nd order + dtheta','-da -solve -simulate -dhdata -writeMatlab -largeShocks -initialState Solutions/KS_NA_shocks  -secondOrder -dtheta -stateDir Solutions/KS_shocks_2nd_dtheta -gradSimSteps 24 -simDir sim -addClass com.meliorbis.economics.incompletemarkets.EulerErrorSimObserver';    
    };

execFn = @( params ) eval(['!java -Xmx4800M -Xms1024M -jar ' jarPath ' ' params]);

disp('Please select model(s) to solve:')
disp(' ')

for i=1:length(executions)
    disp([num2str(i) ') ' executions{i,1}])
end
disp(' ')
disp('or press return for all')

n = input('');

if isscalar(n) && n >=1 && n <= length(executions)
    disp(['Computing: ' executions{n,1}]);
    execFn(executions{n,2});
else
    for i=1:length(executions)
        disp(['Computing: ' executions{i,1}]);

        execFn(executions{i,2});

    end
end