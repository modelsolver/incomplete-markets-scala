function results = processResults()

    results = {};
    
    results.KS = processSolution('Solutions/KS','Basic', 'KS - Vanilla');
    results.KS_grad = processSolution('Solutions/KS_grad','Reference Dist Sim');
    results.KS_dtheta = processSolution('Solutions/KS_dtheta','DTheta');
    results.KS_2nd = processSolution('Solutions/KS_2nd','2nd-Order');
    results.KS_2nd_dtheta = processSolution('Solutions/KS_2nd_dtheta','2nd-Order + dtheta');
    
    results.KS_shocks = processSolution('Solutions/KS_shocks','Basic', 'KS - Large Shocks');
    results.KS_shocks_grad = processSolution('Solutions/KS_shocks_grad','Reference Dist Sim');
    results.KS_shocks_dtheta = processSolution('Solutions/KS_shocks_dtheta','DTheta');
    results.KS_shocks_2nd = processSolution('Solutions/KS_shocks_2nd','2nd Order');
    results.KS_shocks_2nd_dtheta = processSolution('Solutions/KS_shocks_2nd_dtheta','2nd Order + DTheta');

end

function results = processSolution( path ,  run,  group)

    if nargin > 2
        disp('=========')
        disp(group)
        disp('=========')
    end
    
    if nargin > 1
        disp('---------')
        disp(run)
        disp('---------')
    end
    
    results = {};
    
    % Read the simulated time series
    simAgg = load([path '/simAgg.mat']);
    sim = load([path '/sim.mat']);
    
    % Read the solution itself
    results.state = load([path '/state.mat']);
    
    % Add the aggregate state time series to the results
    results.states = sim.states;
    results.aggStates = simAgg.states;
    results.shocks = sim.shocks;
    
    % calculate the K used to perform derivative aggregation in each
    % productivity state
    K = calcCapital(results.state.finalGradDensity,results.state.indStates.x_1);
    K_1 = calcCapital(results.state.finalGradDensity_1,results.state.indStates.x_1);
    
    % Add K, K' to the state
    results.state.gradients = [K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,1),K);K_1,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,2),K_1)];
    
    % Add the one-period-ahead forecasts
    results = addOnePeriodAhead(results);
    
    % Perform the result comparison
    checkResults(results); 

end

% % Calculate statistics for 10k-period ahead
% disp('---------')
% disp('1st Order')
% disp('---------')
% 
% 
% euler = load('Solutions/KS/dynamicEuler.mat');
% cdiffs = abs(euler.c./euler.cTilde -1)*100;
% fprintf('\ncdiffs(mean,max) = (%s,%s)\n',mean(cdiffs),max(cdiffs));
% kdiffs = abs((euler.k-euler.kTilde)./(mean(euler.kTilde)))*100;
% fprintf('kdiffs(mean,max) = (%s,%s)\n\n\n',mean(kdiffs),max(kdiffs));
% 
% disp('------------')
% disp('LARGE SHOCKS')
% disp('------------')
% 
% % Calculate statistics for 10k-period ahead
% disp('---------')
% disp('1st Order')
% disp('---------')
% simAgg = load('Solutions/KS_shocks/simAgg.mat');
% sim = load('Solutions/KS_shocks/sim.mat');
% results.state = load('Solutions/KS_shocks/state.mat');
% results.states = sim.states;
% results.aggStates = simAgg.states;
% results.shocks = sim.shocks;
% K = calcCapital(results.state.finalGradDensity,results.state.indStates.x_1);
% K_1 = calcCapital(results.state.finalGradDensity_1,results.state.indStates.x_1);
% results.state.gradients = [K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,1),K);K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,2),K_1)];
% results = addOnePeriodAhead(results);
% checkResults(results);
% 
% plot1PDiffs(results,1);
% figure;
% 
% euler = load('Solutions/KS_shocks/dynamicEuler.mat');
% cdiffs = abs(euler.c./euler.cTilde -1)*100;
% fprintf('\ncdiffs(mean,max) = (%s,%s)\n',mean(cdiffs),max(cdiffs));
% kdiffs = abs((euler.k-euler.kTilde)./(mean(euler.kTilde)))*100;
% fprintf('kdiffs(mean,max) = (%s,%s)\n\n\n',mean(kdiffs),max(kdiffs));
% 
% disp('---------')
% disp('1st Order - dtheta')
% disp('---------')
% simAgg = load('Solutions/KS_shocks_dtheta/simAgg.mat');
% sim = load('Solutions/KS_shocks_dtheta/sim.mat');
% results.state = load('Solutions/KS_shocks_dtheta/state.mat');
% results.states = sim.states;
% results.aggStates = simAgg.states;
% results.shocks = sim.shocks;
% K = calcCapital(results.state.finalGradDensity,results.state.indStates.x_1);
% K_1 = calcCapital(results.state.finalGradDensity_1,results.state.indStates.x_1);
% results.state.gradients = [K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,1),K);K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,2),K_1)];
% results = addOnePeriodAhead(results);
% checkResults(results);
% 
% plot1PDiffs(results,1);
% figure;
% 
% disp('---------')
% disp('2nd Order')
% disp('---------')
% simAgg = load('Solutions/KS_shocks_2nd/simAgg.mat');
% sim = load('Solutions/KS_shocks_2nd/sim.mat');
% results.state = load('Solutions/KS_shocks_2nd/state.mat');
% results.states = sim.states;
% results.aggStates = simAgg.states;
% results.shocks = sim.shocks;
% K = calcCapital(results.state.finalGradDensity,results.state.indStates.x_1);
% K_1 = calcCapital(results.state.finalGradDensity_1,results.state.indStates.x_1);
% results.state.gradients = [K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,1),K);K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,2),K_1)];
% results = addOnePeriodAhead(results);
% checkResults(results);
% 
% plot1PDiffs(results,1);
% 
% euler = load('Solutions/KS_shocks_2nd/dynamicEuler.mat');
% cdiffs = abs(euler.c./euler.cTilde -1)*100;
% fprintf('\ncdiffs(mean,max) = (%s,%s)\n',mean(cdiffs),max(cdiffs));
% kdiffs = abs((euler.k-euler.kTilde)./(mean(euler.kTilde)))*100;
% fprintf('kdiffs(mean,max) = (%s,%s)\n\n\n',mean(kdiffs),max(kdiffs));
% 
% disp('---------')
% disp('2nd Order - DTHeta')
% disp('---------')
% simAgg = load('Solutions/KS_shocks_2nd_dtheta/simAgg.mat');
% sim = load('Solutions/KS_shocks_2nd_dtheta/sim.mat');
% results.state = load('Solutions/KS_shocks_2nd_dtheta/state.mat');
% results.states = sim.states;
% results.aggStates = simAgg.states;
% results.shocks = sim.shocks;
% K = calcCapital(results.state.finalGradDensity,results.state.indStates.x_1);
% K_1 = calcCapital(results.state.finalGradDensity_1,results.state.indStates.x_1);
% results.state.gradients = [K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,1),K);K,interp1(results.state.aggStates.X_1,results.state.aggTransition(:,2),K_1)];
% results = addOnePeriodAhead(results);
% checkResults(results);
% 
% plot1PDiffs(results,1);
% 
% % BONDS
% 
% disp('-----')
% disp('Bonds')
% disp('-----')
% 
% simAgg = load('Solutions/bonds_KS_8qtr/simEulerAgg.mat');
% sim = load('Solutions/bonds_KS_8qtr/simEuler.mat');
% results.state = load('Solutions/bonds_KS_8qtr/state.mat');
% state = results.state;
% 
% % The aggregate transition is not affected by the control, so select away 
% % that dimension
% transition = squeeze(state.aggTransition(:,1,:));
% 
% simAgg.states1P = zeros(length(sim.states),1);
% simAgg.states1P(1) = sim.states(1);
% 
% for j = 2:length(sim.states)
%     %    results.statesAgg1P(j) = interp1(K,transition(:,shocks1(j-1,1)+1),states1(j-1));
%     simAgg.states1P(j) = interp1(state.aggStates.X_1,transition(:,sim.shocks(j-1,1)+1),sim.states(j-1));
% end
%         
% results.states = sim.states;
% results.aggStates = simAgg.states;
% 
% disp('---')
% disp('10k-period ahead K predictions')
% disp('---')
% % Calculate statistics for 10k-period ahead
% checkResults(results);
% 
% results.aggStates = simAgg.states1P;
% 
% disp('---')
% disp('1-period ahead K predictions')
% disp('---')
% 
% % Calculate statistics for 1-period ahead
% checkResults(results);
% 
% % The bond prices implied by agg states and controls is the inverse of the
% % returns
% bondPrices = 1./state.bondReturns;
% 
% sim.bondPrices = zeros(length(sim.controls),1);
% 
% % Calculate the bond prices obtained in both simulations from the control
% for j = 1:length(sim.bondPrices)
%     sim.bondPrices(j) = interp2(state.aggStates.X_1,state.aggControls.C_1,squeeze(bondPrices(sim.shocks(j)+1,1,:,:))',sim.states(j,1),sim.controls(j,1));
% end
% 
% simAgg.bondPrices = zeros(length(simAgg.controls),1);
% 
% for j = 1:length(simAgg.bondPrices)
%     simAgg.bondPrices(j) = interp2(state.aggStates.X_1,state.aggControls.C_1,squeeze(bondPrices(simAgg.shocks(j)+1,1,:,:))',simAgg.states(j,1),simAgg.controls(j,1));
% end
% 
% results.states = sim.bondPrices;
% results.aggStates = simAgg.bondPrices;
% 
% disp('---')
% disp('10k-period ahead p predictions')
% disp('---')
% % Calculate statistics for 1-period ahead
% checkResults(results);
% 
% simAgg.bondPrices1P = zeros(length(simAgg.controls),1);
% simAgg.bondPrices1P(1) = simAgg.bondPrices(1);
% 
% for j = 2:length(simAgg.bondPrices1P)
%     predictedControl = interp1(state.aggStates.X_1,squeeze(state.aggExpectedControls(simAgg.shocks(j-1)+1,:,1,simAgg.shocks(j)+1)),sim.states(j-1,1));
%     simAgg.bondPrices1P(j) = interp2(state.aggStates.X_1,state.aggControls.C_1,squeeze(bondPrices(simAgg.shocks(j)+1,1,:,:))',simAgg.states1P(j,1),predictedControl);
% end
% 
% results.aggStates = simAgg.bondPrices1P;
% 
% disp('---')
% disp('1-period ahead p predictions')
% disp('---')
% % Calculate statistics for 1-period ahead
% checkResults(results);
% 
%     
% 
% 
