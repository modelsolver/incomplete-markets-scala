package com.meliorbis.economics.util

import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.numerics.scala.DoubleArray._
import java.util.Arrays
import com.meliorbis.economics.infrastructure.Base
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.DoubleArrayFactories._

/**
 * @author toby
 */
object Util extends Base {
  
  def determineTransformation(source: DiscretisedDistribution, target: DiscretisedDistribution, levels: DoubleArray) : DoubleArray = {
    
    // Check that the distributions have the same size
    if(!Arrays.equals(source._density.size(), target._density.size())) {
      throw new RuntimeException("The source and target distribution must have the same size")
    }
    
    // Check that the levels specified have the correct length
    if(source._density.size()(0) != levels.numberOfElements()) {
      throw new RuntimeException("The number of levels must equal the distributions' first dimension size")
    }
    
    // Create an array to hold the result
    val result = createArrayOfSize(source._density.size():_*)
    
    // Calculate the cumulative density function for the target distribution
    var targetCdf = toCumulativeDensityFn( target )
    
    // Handle each individual productivity level separately
    for (indShock <- 0 until source._density.size()(1)) {

      var currentPos = 0
      var cummulative = 0d

      var lastki =  0d
      
      // For each invidiual wealth point...
      for (indK <- 0 until levels.numberOfElements ) {
          
        // calculate the cdf up to that point in the original dist
        cummulative += source._density(indK, indShock)

        // find the first point in the transformed distribution to exceed that cumulative probability
        while (currentPos < levels.numberOfElements() &&
                (cummulative > targetCdf(currentPos, indShock))) {
          currentPos += 1
        }

        // If the identified point in the transformed distribution is the first one (i.e. the constraint binds), then 
        // the constraint value is the transformed value.
        if (currentPos == 0) {
          result(indK, indShock) = levels(0)
        }
        else {
          val lowerDensity = targetCdf(currentPos - 1, indShock)
          val lowerK = levels(currentPos - 1)

          val (upperK, upperDensity) = 
            // If the transformed grid was exceeded, interpolate to the point between the uppermost wealth level and
            // overflow implied by the two cdf values
            if (currentPos == levels.numberOfElements()) {
              
              // The cumulative density will not be 1 because of the proportions in other shock states
              (target._overflowAverages(0, indShock), lowerDensity + target._overflowProportions(0,indShock))
            } 
            // Otherwise, interpolate between the point identified and the prior point, which lie either side of the
            // source cdf value
            else {
              (levels(currentPos), targetCdf(currentPos, indShock))
            }
          
          val upperProportion =  (cummulative - lowerDensity) / (upperDensity - lowerDensity)
          
          result(indK, indShock) = upperProportion * upperK + (1d - upperProportion) * lowerK
          
        }
      }
    }
      
    return result
  }
  
  /**
   * Calculates the cumulative density fn along the wealth dimension from the 
   * provided probability density fn
   */
  def toCumulativeDensityFn(pdf: DiscretisedDistribution) : DoubleArray = {
    
    var cdf = createArrayOfSize(pdf._density.size(): _*)

    var cumulative = createArrayOfSize(pdf._density.size()(1))

    // Calculate the cumulative density function of the distribution
    for (i <- 0 until cdf.size()(0)) {
      
      cumulative +=  pdf._density(i,$)

      cdf(i,$) << cumulative
    }
    
    cdf
  }
  
}