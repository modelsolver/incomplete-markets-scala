/**
 *
 */
package com.meliorbis.economics.incompletemarkets

import java.io.File
import org.apache.commons.cli.{ CommandLine, Options }
import org.apache.commons.math3.linear.{ Array2DRowRealMatrix, LUDecomposition, RealMatrix }
import com.meliorbis.economics.aggregate.AggregateSolverBase
import com.meliorbis.economics.core.{ CRRAUtility, CobbDouglasProduction, ProductionFunction, UtilityFunction }
import com.meliorbis.economics.incompletemarkets.IncompleteMarketsModelXPA.{ AggregateSolver, IndividualSolver, Model }
import com.meliorbis.economics.individual.IndividualProblemState
import com.meliorbis.economics.individual.egm.EGMIndividualProblemSolver
import com.meliorbis.economics.infrastructure.{ AbstractModel, AbstractStateBase, ModelConfigBase }
import com.meliorbis.economics.infrastructure.simulation.{ DiscretisedDistribution, SimState, SimulationResults, SimulationObserver }
import com.meliorbis.economics.model.ModelRunner
import com.meliorbis.economics.scala.ModelHelpers.arrayChangedConv
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.numerics.generic.primitives.impl.Interpolation._
import com.meliorbis.numerics.io.NumericsReader
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.utils.Utils
import com.meliorbis.economics.core.LogUtility
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.IntArrayFactories._
import com.meliorbis.numerics.Numerics
import com.meliorbis.numerics.convergence.Criterion
import math._

/**
 * An implementation of the standard incomplete markets model (''Krusell & Smith (98)''), 
 * using:
 * 
 * - The ''Method of Endogenous Gridpoints'' (Carroll 2006) to solve the individual problem
 * - ''Explicit Aggregation'' (Den Haan & Rendahl 2010) to update the aggregate forecasting function
 * 
 * This code is essentially a port of the matlab code of Den Haan & Rendahl 2010 to the Solver framework
 *
 * @author Tobias Grasl
 */
object IncompleteMarketsModelXPA {

  def main(args: Array[String]) {
    
    println(s"Running ${this.getClass.getName}")

    Runner.run(args)
  }

  object Runner extends ModelRunner[Model, Config, State] {

    var _useDHData = false

    override def createOptions(): Options = {
      val options = super.createOptions();

      options.addOption("dhdata", false, "Use the shock sequence and initial distribution from the den Haan comparison paper")
      
      return options
    }

    override def simulateModel(model: Model, state: State, periods: Int, burnIn: Int, stateDir: File, simPath: String): SimulationResults[DiscretisedDistribution, Integer] = {

      // Was simulation with the den Haan data requested?
      if (!_useDHData) {
        // No - call the normal simulation
        return super.simulateModel(model, state, periods, burnIn, stateDir, simPath).asInstanceOf[SimulationResults[DiscretisedDistribution, Integer]]

      } else {
        // Yes! Read the data, and simulate the relevant shocks
        var zStream = ClassLoader.getSystemResourceAsStream("Z_Formatted.txt")

        var shocksFromCSV = Numerics.instance().readFormattedCSV[Integer](zStream).asInstanceOf[IntegerArray]

        // Adjust the shocks because our numbering scheme differs from that
        // in the data
        var allShocks = createIntArrayOfSize(shocksFromCSV.size()(0), 1)
        allShocks.at(-1, 0).fill(shocksFromCSV.add(Integer.valueOf(-1)))

        return getSimulator().simulateShocks(model.initialDensity(state), allShocks, model,
          state, SimulationObserver.silent(), stateDir, simPath);
      }
    }

    override def createConfig(commandLine: CommandLine): Config = {

      if (commandLine.hasOption("dhdata")) {
        _useDHData = true
      }

      val config = new Config()

      config._discountRate = 0.99

      val capitalShare = 0.36
      val depreciationRate = 0.025d

      config._utilityFunction = new CRRAUtility(1.0d)
      config._productionFunction = new CobbDouglasProduction(capitalShare, 1d - capitalShare, depreciationRate);

      config.setIndividualExogenousStates(createArray(0.15, (1 - 0.15 * 0.05) / 0.95));

      val capLevels = nLogSequence(200, 250, 1);
      val simCapLevels = nLogSequence(200, 2001, 0);

      config.setIndividualEndogenousStates(capLevels);
      config.setIndividualEndogenousStatesForSimulation(simCapLevels);

      // Create a capital grid with 15 points spaced around the mean 39.85, but with the points closer to the mean a little denser

      val Kumin = 33;
      val Kemin = 35;
      val Kumax = 42.5;
      val Kemax = 43.5;

      config.setAggregateEndogenousStates(createArray(Utils.sequence(Kumin, Kumax, 12): _*), createArray(Utils.sequence(Kemin, Kemax, 12): _*))

      var transition = createTransition()      

//      val altTrans = createArrayOfSize(2, 2, 2, 2)
//      altTrans << Array(21d / 40d, 1d / 32d, 7d / 20d, 3d / 32d,
//        3d / 32d, 7d / 24d, 1d / 32d, 7d / 12d,
//        7d / 180d, 1d / 480d, 301d / 360d, 59d / 480d,
//        7d / 768d, 7d / 288d, 89d / 768d, 245d / 288d)
//
//        
//      println(maximumRelativeDifference(transition, altTrans))

      
      config.setAggregateExogenousStates(createArray(.99d, 1.01d));
      
      // No permanent shock so nothing to multiply in
      config.setExogenousStateTransiton(transition);

       // The initial exogenous state is 0 (no need to set the value)
      config.setInitialExogenousStates(getNumerics().newIntArray(1))
      
      return config
    }
    
    def createTransition() : DoubleArray = {
      
      val durationUnempG = 1.5;
      val durationUnempB = 2.5;
      val corr = .25;

      val unempG = .04;
      val unempB = .1;

      val durationZG = 8
      val durationZB = 8

      val pZG = 1d-1d/durationZG
      val pZB = 1d-1d/durationZB

      val PZ = Array(pZG, 1-pZG, 1-pZB, pZB)

      var p00 = 1-1/durationUnempG
      var p01 = 1-p00
      var p11 = ((1-unempG)-unempG*p01)/(1-unempG)

      val P11 = createArrayOfSize(2,2)
      P11 << Array(p00,p01,1d-p11,p11)

      p00 = 1-1/durationUnempB
      p01 = 1-p00
      p11 = ((1-unempB)-unempB*p01)/(1-unempB)

      val P00 = createArrayOfSize(2,2)
      P00 << Array(p00,p01,1d-p11,p11)

      p00 = (1+corr)*p00
      p01 = 1-p00
      p11 = ((1-unempB)-unempG*p01)/(1-unempG)

      val P10 = createArrayOfSize(2,2)
      P10 << Array(p00,p01,1d-p11,p11)

      p00 = (1-corr)*(1-1/durationUnempG);
      p01 = 1-p00
      p11 = ((1-unempG)-unempB*p01)/(1-unempB)

      val P01 = createArrayOfSize(2,2)
      P01 << Array(p00,p01,1d-p11,p11)

      var P = createArrayOfSize(2,2,2,2)
      P($,0,$,0) += P00*PZ(3)
      P($,0,$,1) += P01*PZ(2)
      P($,1,$,0) += P10*PZ(1)
      P($,1,$,1) += P11*PZ(0)
      
      return P
    }
 
    override def getModelClass(): Class[Model] = classOf[Model]
  }

  class Model() extends AbstractModel[Config, State]() {

    var _employDist: DoubleArray = _
    var _aggLabour: Array[Double] = _
    var _wageRatios: DoubleArray = _

    var _wages: DoubleArray = _
    
    // These arrays are for convenience - they are constant and are used to calculate wages and returns
    var _expectedLabour: DoubleArray = _
    var _expectedProductivity: DoubleArray = _

    var _interestMatrix: DoubleArray = _
    var _discountMatrix: DoubleArray = _
    
    var _conditionalTransition: DoubleArray = _

    override def initialise(): Unit = {

      super.initialise()

      _interestMatrix = createArrayOfSize(
        _config.getAggregateUnempCapitalLevels().numberOfElements(),
        _config.getAggregateEmpCapitalLevels().numberOfElements(),
        _config.getAggregateProductivityShocks.numberOfElements())

      // Calculate the proportion of employed/unemployed in each aggregate state, and also the resulting
      // aggregate labour supply
      initEmploymentDist()

      // Determine the relative wages earned by employed/unemployed in each state
      initWageRatios()

      // These arrays are for convenience - they are constant and are used to calculate wages and returns
      _expectedLabour = createAggregateExpectationGrid(1);
      _expectedLabour\(1) << _aggLabour

      _expectedProductivity = createAggregateExpectationGrid(1);
      _expectedProductivity.across(1) << _config.getAggregateProductivityShocks()

      // The probabilities of moving between individual employment states
      // given the aggregate state transition
      _conditionalTransition = _config.getExogenousStateTransition().across(0, 1, 3).divide(_config.getExogenousStateTransition().across(2).sum());
    }

    /**
     * Always update aggregates
     */
    override def shouldUpdateAggregates(state: State): Boolean = state.getPeriod() > 200

    override def calculateAggregateStates(distribution: SimState, aggregateExogenousStates: IntegerArray, state: State): Array[Double] = {

      val dist = distribution.asInstanceOf[DiscretisedDistribution]
      val sums = (dist._density.across(0).multiply(_config.getIndividualEndogenousStatesForSimulation().get(0)).across(0).sum() +
        dist._overflowAverages * dist._overflowProportions)

      // Make it into averages over each group by dividing by the masses
      sums /= (dist._density.across(0).sum() + dist._overflowProportions)

      if(sums(0) > _config.getAggregateUnempCapitalLevels().last() ||
          sums(0) < _config.getAggregateUnempCapitalLevels().first()) {
        println("Unemp capital outside of range")
      }
      
      if(sums(1) > _config.getAggregateEmpCapitalLevels().last() ||
          sums(1) < _config.getAggregateEmpCapitalLevels().first()) {
        println("Emp capital outside of range")
      }
      
      sums.toArray()
    }

    def initEmploymentDist() = {

        var ggTrans = _config.getExogenousStateTransition()($, 1, $, 1)
        var bbTrans = _config.getExogenousStateTransition()($, 0, $, 0)

        // Normalise them so that total probs from each state are 1
        ggTrans = ggTrans.across(0).divide(ggTrans.across(1).sum())
        bbTrans = bbTrans.across(0).divide(bbTrans.across(1).sum())

        // Find the steady state of the stochastic processes
        for (i <- 1 until 30) {
          ggTrans = ggTrans %* ggTrans
          bbTrans = bbTrans %* bbTrans
        }

        // Normalise again to get rid of small errors
        ggTrans.modifying().across(0).divide(ggTrans.across(1).sum())
        bbTrans.modifying().across(0).divide(bbTrans.across(1).sum())

        // Create an array to hold the steady state distributions
        _employDist = createArrayOfSize(2, 2)

        // Fill it with the steady states for bad and good aggregate productivity respectively
        _employDist($, 0) << bbTrans(0, $)
        _employDist($, 1) << ggTrans(0, $)

        // Make sure the employment proportions add to 1 in each state
        _employDist(0, $) << _employDist(1, $) * (-1) + 1d

        // Also calculate the aggregate labour supply
        // This is normalised to one in the bad state, so that hours worked per employee is not one!
        _aggLabour = Array(1, _employDist(1, 1) / _employDist(1, 0));
    }

    override def readAdditional(state: State, reader: NumericsReader): Unit = {
      // Calculate expectations from the aggregate transition
      adjustExpectedAggregates(state)
    }

    /**
     * Calculates the after-tax wages implied by aggregate employment, unemployment insurance and
     * a book-balancing government
     */
    def initWageRatios() = {
      _wageRatios = createArrayOfSize(_employDist.size(): _*);

      // The hours worked, so that aggregate labour supply is 1 in bad state
      val hoursWorked = 1d / _employDist(1, 0);

      // Tax per hour to pay for unemployment insurance
      val taxes = _employDist(0, $).divide(_employDist(1, $)) * _config._unemploymentInsuranceRate

      // Unemployed just get the unemployment insurance
      _wageRatios(0, $) += _config._unemploymentInsuranceRate * hoursWorked

      // employed get the taxed hourly wage times hours worked
      _wageRatios(1, $) += (taxes * (-1) + 1) * hoursWorked
    }

    // Solution is found when the errors are below the maximum error configured
    def isDone(state: State): Boolean = {
      if (state.getPeriod() % 100 == 0) {
        println("Ind. Error: " + state._indError)
        println("Agg. Error: " + state._aggError)
      }
      
     // For compatibility with MATLAB implementation, which uses a different criterion, stop
     // after the same number of periods
     state.getPeriod == 423//_indError < _config._maxIndError && state._aggError < _config._maxAggError
    }

    override def adjustExpectedAggregates(state: State): Unit = {

      var expectedAggStates = createAggregateExpectationGrid(2)

      for (idx <- 0 until 8) {
        fillExpectedTransition(expectedAggStates, state, idx / 4, (idx / 2) & 1, idx & 1)
      }

      // This needs to be done before the employed/unemployed capital levels are constrained to the grid for compatibility 
      // with MATLAB implementation
      var expectedCapital = expectedAggStates.across(1,4).multiply(_employDist.transpose(0,1)).asInstanceOf[DoubleArray]

      expectedCapital = expectedCapital.across(4).sum()
      
      val Array(w, r) = (expectedCapital :: _expectedLabour :: _expectedProductivity) :-> (in => {
          val Array(k,l,p) = in

          Array(wage(p, k, l), grossInterest(p,k,l))
        })

      state._expectedInterest = r
      state._expectedWage = w
        
      // Make sure the expected transitions are not beyond our upper and lower
      // bounds.
      expectedAggStates($, $, $, $, 0).modifying().map(cutToBounds(_config.getAggregateUnempCapitalLevels().first(),
        _config.getAggregateUnempCapitalLevels().last()))

      expectedAggStates($, $, $, $, 1).modifying().map(cutToBounds(_config.getAggregateEmpCapitalLevels().first(),
        _config.getAggregateEmpCapitalLevels().last()))

      val oldExpStates = state.getExpectedAggregateStates()

      state.setExpectedAggregateStates(expectedAggStates)

      notifyAggregateExpectationListeners(oldExpStates, expectedAggStates, state)
    }

    def fillExpectedTransition(expectedAggs: DoubleArray, state: State, currentState: Int, futureState: Int, employed: Int): Unit = {

      val aggStateTransition = state.getAggregateTransition()

      val popTransitions = _conditionalTransition($, currentState, employed, futureState) * _employDist($, currentState)
      
      popTransitions /= popTransitions.sum()
      expectedAggs(currentState, futureState, $, $, employed).
        fill(
            aggStateTransition($, $, currentState, $).across(2).multiply(
                popTransitions).across(2).sum())
    }

    def initialDensity(state: State): com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution = {

      val youngDist = getNumerics().readCSV(ClassLoader.getSystemResourceAsStream("pdistyoung.txt"), '\t')

      /*
       * Copy the second two columns, the first contains cap levels
       */
      val initialDist = createArrayOfSize(2001, 2)

      initialDist($, 0) << youngDist($, 1)
      initialDist($, 1) << youngDist($, 2)

      initialDist.modifying().across(1).divide(initialDist.across(0).sum())

      initialDist.modifying().across(1).multiply(_employDist($, 0))

      val simState = getSimulator().createState()

      simState._density = initialDist
      simState._overflowProportions = createArrayOfSize(1, 2)
      simState._overflowAverages = createArrayOfSize(1, 2)

      return simState
    }

    def initialState(): State = {

      // Create a state object for this configuration 
      val state = new State(_config)

      /* Create the initial guess for the individual capital transition
      */
      val initialTransition = createIndividualTransitionGrid()

      initialTransition.across(0) << _config.getIndividualCapitalLevels()

      val delta = 0.025;

      // Adjust it according to individual shock
      val kpAdjust = createArray(0.3 * (1d - delta), 1d - delta);

      initialTransition.modifying().across(1).multiply(kpAdjust)

      // Set the transition on the initial state
      state.setIndividualPolicy(initialTransition)

      /* Prepare the grids of aggregate prices, which vary along the aggregate capital and
       * productivity levels
       */
      val wageMatrix = createArrayOfSize(_interestMatrix.size(): _*)

      for (Ke_idx <- 0 until _config.getAggregateEmpCapitalLevels().numberOfElements()) {
        for (Ku_idx <- 0 until _config.getAggregateUnempCapitalLevels().numberOfElements()) {
          for (P_idx <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {

            // Get the capital and Labour supply at the current point
            val K = _employDist.get(0, P_idx) * _config.getAggregateUnempCapitalLevels().get(Ku_idx) +
              _employDist.get(1, P_idx) * _config.getAggregateEmpCapitalLevels().get(Ke_idx)
            
            _interestMatrix(Ku_idx, Ke_idx, P_idx) = grossInterest(P_idx, K)
            wageMatrix(Ku_idx, Ke_idx, P_idx) = wage(P_idx, K)
          }
        }
      }
      
      _discountMatrix = 1d/_interestMatrix

      val individualWages = createArrayOfSize(_config.getIndividualExogenousStates().get(0).numberOfElements(),
        _config.getAggregateUnempCapitalLevels().numberOfElements(),
        _config.getAggregateEmpCapitalLevels().numberOfElements(),
        _config.getAggregateProductivityShocks().numberOfElements())

      // Fill the wage matrix across the aggregate capital & productivity dimensions
      individualWages.across(1, 2, 3) << wageMatrix

      // Multiply the aggregate wage by the actual wage individual gets, due to unemp benefit and taxes
      individualWages.modifying().across(0, 3).multiply(_wageRatios)

      // The liquid assets at the beginning of the period are prior capital with interest + wages
      state._normalisedLiquidAssets = createIndividualTransitionGrid()
      state._normalisedLiquidAssets.across(0) << _config.getIndividualCapitalLevels()

      // Mutliply the capital by the rate of return
      state._normalisedLiquidAssets.modifying().across(2, 3, 4).multiply(_interestMatrix)

      // Add the wage
      state._normalisedLiquidAssets.modifying().across(1, 2, 3, 4).add(individualWages)

      _wages = createIndividualTransitionGrid()
      _wages.modifying().across(1, 2, 3, 4).add(individualWages)
      
      // We need to do the same for the simulation grid, which may have a different size
      val laSize = state._normalisedLiquidAssets.size().clone()
      laSize(0) = _config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements()

      val simLA = createArrayOfSize(laSize: _*)
      simLA.across(0) << _config.getIndividualEndogenousStatesForSimulation().get(0)

      // Mutliply the capital by the rate of return
      simLA.modifying().across(2, 3, 4).multiply(_interestMatrix)

      // Add the wage
      simLA.modifying().across(1, 2, 3, 4).add(individualWages)

      state._simLiquidAssets = simLA
      initAggregateTransition(state)

      // Calculate expectations from the aggregate transition
      adjustExpectedAggregates(state)

      // The end of period states are just the capital levels to be carried over
      val simCapitalGrid = createSimulationGrid()

      simCapitalGrid.across(0) << (_config.getIndividualEndogenousStatesForSimulation().get(0))

      state._simCapitalGrid = simCapitalGrid

      // Initialise the simulation density
      _config.setInitialSimState(initialDensity(state))
      
      return state
    }
    
    def capital(Ku: Double, Ke: Double, A: Int): Double = {
      _employDist.get(0, A) * Ku + _employDist.get(1, A) * Ke
    }

    private def initAggregateTransition(state: State): Unit = {

      if (state.getAggregateTransition == null) {
        val inverses = new Array[RealMatrix](2)

        inverses(0) = new LUDecomposition(new Array2DRowRealMatrix(Array(
          Array(_conditionalTransition(1, 0, 1, 0), _conditionalTransition(1, 0, 0, 0)),
          Array(_conditionalTransition(0, 0, 1, 0), _conditionalTransition(0, 0, 0, 0))))).getSolver().getInverse();

        inverses(1) = new LUDecomposition(new Array2DRowRealMatrix(Array(
          Array(_conditionalTransition(1, 1, 1, 1), _conditionalTransition(1, 1, 0, 1)),
          Array(_conditionalTransition(0, 1, 1, 1), _conditionalTransition(0, 1, 0, 1))))).getSolver().getInverse();

        
        val transition = createAggregateVariableGrid(2)

        val transIter = transition.iterator()

        while (transIter.hasNext()) {
          transIter.nextDouble()

          val idx = transIter.getIndex()

          // First deal with unemployed capital
          val unempVal = inverses(idx(2)).getEntry(1, 0) * _config.getAggregateEmpCapitalLevels()(idx(1)) +
            inverses(idx(2)).getEntry(1, 1) * _config.getAggregateUnempCapitalLevels()(idx(0))
            
          transIter.set(unempVal)

          // Then with employed capital
          transIter.nextDouble()
          
          val empVal = inverses(idx(2)).getEntry(0, 0) * _config.getAggregateEmpCapitalLevels()(idx(1)) +
            inverses(idx(2)).getEntry(0, 1) * _config.getAggregateUnempCapitalLevels()(idx(0))
            
          transIter.set(empVal)          
        }

        state.setAggregateTransition(transition)
      }
    }

    /**
     * The transition from employed to unemployed has to be handled here
     */
    def adjustTransitionedAggs(currentAggState: com.meliorbis.numerics.generic.primitives.DoubleArray[_], priorShockIndex: Array[Integer], currentShockIndex: Array[Integer], state: State): Unit = {
      
      val conditionalTransition = _conditionalTransition($, priorShockIndex(0).asInstanceOf[Int], $, currentShockIndex(0).asInstanceOf[Int]).
                                            across(0).multiply(_employDist($, priorShockIndex(0).asInstanceOf[Int]))

      val updated = conditionalTransition.across(0).multiply(currentAggState).across(0).sum().divide(conditionalTransition.across(0).sum())            
      
      currentAggState << updated
    }

    def grossInterest(pIdx: Int, K: Double, L: Double): Double = grossInterest(_config.getAggregateProductivityShocks()(pIdx), K, L)

    def wage(pIdx: Int, K: Double, L: Double): Double = wage(_config.getAggregateProductivityShocks()(pIdx), K, L)

    def grossInterest(pIdx: Int, K: Double): Double = grossInterest(_config.getAggregateProductivityShocks()(pIdx), K, _aggLabour(pIdx))

    def wage(pIdx: Int, K: Double): Double = wage(_config.getAggregateProductivityShocks()(pIdx), K, _aggLabour(pIdx))

    def grossInterest(productivity: Double, K: Double, L: Double): Double = 1 + _config._productionFunction.netReturn(productivity, K, L)

    def wage(productivity: Double, K: Double, L: Double): Double = _config._productionFunction.wage(productivity, K, L)
  }

  class IndividualSolver(model: Model, config: Config) extends EGMIndividualProblemSolver[Config, State, Model](model, config) {
    
    setEndOfPeriodStates(_model.createIndividualVariableGrid()\(0) << 
        _config.getIndividualCapitalLevels())
    
    setEndOfPeriodStates(_model.createIndividualVariableGrid()\0
                                << _config.getIndividualCapitalLevels())

    setStartOfPeriodStates(_model.createIndividualTransitionGrid()\0 
        << _config.getIndividualCapitalLevels())

    def calculateFutureConditionalExpectations(exogenousTransition: Array[Int], currentAggs: Array[Int], state: State): DoubleArray = {

      val currentAggStateIndex = exogenousTransition(1)
      val futureIndStateIndex = exogenousTransition(2)
      val futureAggStateIndex = exogenousTransition(3)

      val aggUnempCapIndex = currentAggs(0)
      val aggEmpCapIndex = currentAggs(1)

      /* The expected future capital carried over for individuals in this state
       */
      val kpp = state.getExpectedIndividualTransition()(currentAggStateIndex, futureAggStateIndex, aggUnempCapIndex, aggEmpCapIndex, $, futureIndStateIndex)

      /* Collect expected future aggregates
       */
      val R = state._expectedInterest(currentAggStateIndex, futureAggStateIndex, aggUnempCapIndex, aggEmpCapIndex)

      // Need to adjust the aggregate wage for the individuals employment state and dt
      val w = state._expectedWage(currentAggStateIndex, futureAggStateIndex, aggUnempCapIndex, aggEmpCapIndex) * _model._wageRatios(futureIndStateIndex, futureAggStateIndex)

      /* Calculate the value of the euler condition conditional on the future states
       */
      return calculateConditionalFutureEuler(kpp, R, w, _config._discountRate)
    }

    def calculateConditionalFutureEuler(kpp: DoubleArray, futureR: Double, futureW: Double, discountRate: Double): DoubleArray = {
      /* First, calculate consumption as starting capital times interest plus wages minus ending capital
       */
      val individualCapitalLevels = _config.getIndividualCapitalLevels()

      // Calculate consumption at this point
      val c = (individualCapitalLevels :: kpp) -> ((kp: Double, kpp: Double) => kp * futureR + futureW - kpp);

      val effectiveDiscount = _config._discountRate * futureR
      /* Now determine the future euler value, i.e. the discounted marginal utility of consumption
     */
      val fce = c map ((c: Double) =>  effectiveDiscount * _config._utilityFunction.marginalUtility(0, c))

      // Return that value
      fce
    }

    /**
     * Given expectations over the future euler, determine the implied current liquid assets
     */
    def calculateImpliedStartOfPeriodState(individualExpectations: com.meliorbis.numerics.generic.primitives.DoubleArray[_], state: State): DoubleArray = {

      val indExp : DoubleArray = individualExpectations
      
      // The last dimension is of size 1, so select it away to match sizes
      val impliedLiquidAssets =
        (indExp :: _eopStates) -> ((futureEuler: Double, kp: Double) => {

          // We calculated the marginal utility in the euler calculation, now invert it for c
          val c = _config._utilityFunction.inverseMarginalUtility(futureEuler)

          // Liquid assets must be equal to consumption plus final capital, adjusted for growth
          c + kp
        })

      return (impliedLiquidAssets - _model._wages).across(2, 3, 4).multiply(_model._discountMatrix)
    }

    override def afterPolicyUpdate(oldPolicy: com.meliorbis.numerics.generic.primitives.DoubleArray[_], newPolicy: com.meliorbis.numerics.generic.primitives.DoubleArray[_], state: State): Unit = {

      state._policyForSimulation = null

      if (state.getPeriod() % 10 == 5) {

        state._indError = maximumRelativeDifferenceSpecial(newPolicy(20, $), oldPolicy(20, $))

      }

      return
    }
  }

  class AggregateSolver(model: Model, config: Config) extends AggregateSolverBase[Config, State, Model](model, config) {

    // Adjustment constants to apply in each iteration
    val DE = .01257504725554
    val DU = .03680683961167
    
    // This is handled instead when expected aggs are calculated
    setConstrainToGrid(false)
    
    addTransitionListener(arrayChangedConv((oldVal: DoubleArray, newVal: DoubleArray, state: State) => {
      state.asInstanceOf[State]._aggError = maximumRelativeDifference(oldVal, newVal)
    }))

    override def calculateAggregatePolicies(state: State): com.meliorbis.utils.Pair[DoubleArray, DoubleArray] = {

      val newTransition = _model.createAggregateVariableGrid(2)

      val transIter = newTransition.iterator()

      while (transIter.hasNext()) {
        transIter.nextDouble()

        val index = transIter.getIndex()

        // Deal with the case of unemployment
        transIter.set(interpDimension(state.getIndividualPolicy()($, 0, index(0), index(1), index(2)),
          _config.getIndividualCapitalLevels(), _config.getAggregateUnempCapitalLevels()(index(0)), 0).get(0) + DU)

        transIter.nextDouble()

        // Deal with the case of employment
        transIter.set(interpDimension(state.getIndividualPolicy()($, 1, index(0), index(1), index(2)),
          _config.getIndividualCapitalLevels(), _config.getAggregateEmpCapitalLevels()(index(1)), 0).get(0) + DE)

      }

      return new com.meliorbis.utils.Pair(newTransition, null)
    }
  }

  class State(config: Config) extends AbstractStateBase[Config](config) {
    
    var _aggError: Double = 1d
    var _indError: Double = 1d

    var _normalisedLiquidAssets: DoubleArray = _

    var _simCapitalGrid: DoubleArray = _
    var _simLiquidAssets: DoubleArray = _

    var _expectedInterest: DoubleArray = _
    var _expectedWage: DoubleArray = _

    var _policyForSimulation: DoubleArray = _


    override def getIndividualPolicyForSimulation(): DoubleArray = {

      // Need to update the policy for the simulation grid
      if (_policyForSimulation == null) {
        _policyForSimulation = interpolateFunction(_normalisedLiquidAssets, getIndividualPolicy(), 0, _simLiquidAssets, new Params().constrained());
      }
      return _policyForSimulation
    }

    override def getEndOfPeriodStatesForSimulation(): DoubleArray = _simCapitalGrid
    
    override def getConvergenceCriterion(): Criterion = {
      new Criterion{
        override def getValue: Double = 423-getPeriod()//For compatibility with Paper, and to get sufficiently good fit max(_indError, _aggError)
        
        override def toString(): String = "Ind: %.2e Agg: %.2e".format(_indError, _aggError)
      }
    }
  }

  class Config extends ModelConfigBase {

    // Maximum error to converge to
    var _maxAggError: Double = 1e-6
    var _maxIndError: Double = 5e-7

    var _discountRate: Double = 0.99
    var _unemploymentInsuranceRate = 0.15

    var _utilityFunction: UtilityFunction = _
    var _productionFunction: ProductionFunction = _

    def getIndividualCapitalLevels(): DoubleArray = {
      return getIndividualEndogenousStates().get(0);
    }

    def getAggregateEmpCapitalLevels(): DoubleArray = {
      return getAggregateEndogenousStates().get(1);
    }

    def getAggregateUnempCapitalLevels(): DoubleArray = {
      return getAggregateEndogenousStates().get(0);
    }

    def getAggregateProductivityShocks(): DoubleArray = {
      return getAggregateExogenousStates().get(0);
    }

    override def isConstrained(): Boolean = true

    override def readParameters(x$1: com.meliorbis.numerics.io.NumericsReader): Unit = {

    }

    override def writeParameters(x$1: com.meliorbis.numerics.io.NumericsWriter): Unit = {

    }

    def getAggregateProblemSolver(): Class[AggregateSolver] = classOf[AggregateSolver]

    def getIndividualSolver(): Class[IndividualSolver] = classOf[IndividualSolver]
  }
}

