/**
 *
 */
package com.meliorbis.economics.incompletemarkets.bonds

import java.io.File
import java.util.Arrays
import org.apache.commons.cli.Options
import org.apache.commons.lang.ArrayUtils
import com.meliorbis.economics.aggregate.derivagg.DerivativeAggregationSolver
import com.meliorbis.economics.aggregate.derivagg.DerivAggCalcState
import com.meliorbis.economics.core.CRRAUtility
import com.meliorbis.economics.core.CobbDouglasProduction
import com.meliorbis.economics.core.ProductionFunction
import com.meliorbis.economics.core.UtilityFunction
import com.meliorbis.economics.individual.egm.EGMIndividualProblemSolver
import com.meliorbis.economics.infrastructure.AbstractModelWithControls
import com.meliorbis.economics.infrastructure.AbstractStateBase
import com.meliorbis.economics.infrastructure.simulation.SimulationObserver
import com.meliorbis.economics.infrastructure.ModelConfigBase
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulator
import com.meliorbis.economics.infrastructure.simulation.HasConditionalTransition
import com.meliorbis.economics.infrastructure.simulation.SimulationResults
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord
import com.meliorbis.economics.model._
import com.meliorbis.economics.scala.ModelHelpers.arrayChangedConv
import com.meliorbis.numerics.fixedpoint.UnivariateBoundedNewtonFixedPointFinder
import com.meliorbis.numerics.function.MultiVariateVectorFunction
import com.meliorbis.numerics.generic.impl.GenericBlockedArray
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.numerics.io.NumericsReader
import com.meliorbis.numerics.io.NumericsWriter
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.numerics.generic.primitives.impl.Interpolation._
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.utils.Timer
import com.meliorbis.utils.Utils
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.economics.infrastructure.simulation.SimState
import com.meliorbis.numerics.generic.MultiDimensionalArray
import com.meliorbis.economics.infrastructure.Base
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.IntArrayFactories._
import com.meliorbis.numerics.Numerics
import com.meliorbis.economics.infrastructure.simulation.Simulator
import com.meliorbis.numerics.convergence.Criterion
import com.meliorbis.economics.individual.egm.EGMIndividualProblemSolverWithControls
import com.meliorbis.numerics.io.matlab.MatlabReaderFactory

/**
 * An implementation of the incomple markets model with bonds a la Krusell & Smith (97) using the
 * method of endogenous gridpoints and derivative aggregation
 *
 * @author Tobias Grasl
 */
object IMWithBonds {

  type DerivationInformation = (DoubleArray, DoubleArray, DoubleArray, DoubleArray)

  /**
   * Converts a closure accepting an array of doubles and returning a double
   * to a DoubleNaryOp
   */
  implicit def fnConv(fn: (Double) => Double) =
    new MultiVariateVectorFunction[java.lang.Double] {
      def call(vals: java.lang.Double*) = Array(fn(vals(0)))

      def call(vals: Array[java.lang.Object]) = (Array[java.lang.Double](fn(vals(0).asInstanceOf[Double]).asInstanceOf[java.lang.Double])).asInstanceOf[Array[Object]]
    }

  def main(args: Array[String]) {
    println(s"Running ${this.getClass.getName}")

    BondsRunner.run(args)
  }

  object BondsRunner extends ModelRunner[Model, Config, State] {

    var _useDHData = false

    override def createOptions(): Options =
      {
        val options = super.createOptions();

        options.addOption("noAgg", false, "Solve a model without aggregate uncertainty")
        options.addOption("dhdata", false, "Use the shock sequence and initial distribution from the den Haan comparison paper")

        return options
      }

    override def simulateModel(model: Model, state: State, periods: Int, burnIn: Int, stateDir: File, simPath: String): SimulationResults[_, _] =
      {
        if (!_useDHData) {
          return super.simulateModel(model, state, periods, burnIn, stateDir, simPath).asInstanceOf[SimulationResults[DiscretisedDistribution, Integer]]
        } else {
          var zStream = ClassLoader.getSystemResourceAsStream("Z_Formatted.txt")

          var shocksFromCSV = Numerics.instance().readFormattedCSV[Integer](zStream).asInstanceOf[IntegerArray]

          var allShocks = createIntArrayOfSize(shocksFromCSV.size()(0), 2)
          allShocks.at(-1, 0).fill(shocksFromCSV.add(Integer.valueOf(-1)))

          return getSimulator().simulateShocks(model.getConfig.getInitialSimState.asInstanceOf[DiscretisedDistribution], allShocks, model,
            state, SimulationObserver.silent(), stateDir, simPath);
        }
      }

    override def createConfig(commandLine: org.apache.commons.cli.CommandLine): Config = {

      if (commandLine.hasOption("dhdata")) {
        _useDHData = true
      }

      val aggRisk = !commandLine.hasOption("noAgg")

      val config = new Config()

      config._discountRate = 0.99

      config._capitalShare = 0.36
      config._depreciation = 0.025d

      config._utilityFunction = new CRRAUtility(1.0d)
      config._productionFunction = new CobbDouglasProduction(config._capitalShare, 1d - config._capitalShare, config._depreciation);

      // No growth
      config._growth = 1;

      config.setIndividualExogenousStates(createArray(0.15, (1 - 0.15 * 0.05) / 0.95));

      val capLevels = nLogSequence(200, 250, 0);
      val simCapLevels = nLogSequence(200, 2001, 0);

      config.setIndividualEndogenousStates(capLevels);
      config.setIndividualEndogenousStatesForSimulation(simCapLevels);

      // Create a capital grid with 15 points spaced around the mean 39.85, but with the points closer to the mean a little denser
      //config.setAggregateEndogenousStates(pullToMean(createArray(Utils.sequence(0.85 * 39, 1.15 *  39, 15): _*), 0.2));
      config.setAggregateEndogenousStates(createArray(Utils.sequence(0.85 * 39, 1.15 * 39, 15): _*));

      var transition = createArrayOfSize(2, 2, 2, 2, 1)

      transition << Array(21d / 40d, 1d / 32d, 7d / 20d, 3d / 32d,
        3d / 32d, 7d / 24d, 1d / 32d, 7d / 12d,
        7d / 180d, 1d / 480d, 301d / 360d, 59d / 480d,
        7d / 768d, 7d / 288d, 89d / 768d, 245d / 288d)

      config._bondBorrowingLimit = 2.4

      if (aggRisk) {

        config.setAggregateExogenousStates(createArray(.99d, 1.01d))

        val reverseControls = 0.9999 - nLogSequence(100, 9, 3) / 2000d

        val controls = createArrayOfSize(reverseControls.size(): _*)

        for (i <- 0 until controls.numberOfElements) {
          controls(i) = reverseControls(controls.numberOfElements() - 1 - i)
        }

        // Creates bond returns that are more tightly clustered close to the average return on capital,
        // where the impact becomes much steeper
        config.setAggregateControls(controls)

        // THe target is always 0 (net bond demand)
        config.setControlTargets(createArrayOfSize(controls.size():_*))


      } else {

        config.setAggregateExogenousStates(createArray(1d));

        val indTrans = transition($, 0, $, 0) + transition($, 1, $, 1)

        indTrans.modifying().across(1).divide(indTrans.across(1).sum())

        transition = createArrayOfSize(2, 1, 2, 1, 1)

        transition << indTrans

      }

      // No permanent shock so nothing to multiply in
      config.setExogenousStateTransiton(transition);

      // No permament shocks
      config.setAggregateNormalisingExogenousStates(createArray(1d));

      if (aggRisk) {
        config._daDist = new DiscretisedDistribution( new File("Solutions/bonds_KS_NA/ergodicDist.mat") )
      }

      // The initial exogenous state is 0 (no need to set the value)
      config.setInitialExogenousStates(getNumerics().newIntArray(1))

      return config
    }

    override def getModelClass(): Class[Model] = classOf[Model]
  }

  class Model extends AbstractModelWithControls[Config, State] with HasConditionalTransition[Config, State] {

    var _employDist: DoubleArray = _
    var _aggLabour: Array[Double] = _
    var _wageRatios: DoubleArray = _

    var _interestMatrix: DoubleArray = _

    // These arrays are for convenience - they are constant and are used to calculate wages and returns
    var _expectedLabour: DoubleArray = _
    var _expectedProductivity: DoubleArray = _

    override def initialise(): Unit = {
      super.initialise()

      if (!_config.getAggregateControls().isEmpty()) {
      }

      _interestMatrix = createArrayOfSize(_config.getAggregateCapitalLevels().numberOfElements(), _config.getAggregateProductivityShocks.numberOfElements())

      // Calculate the proportion of employed/unemployed in each aggregate state, and also the resulting
      // aggregate labour supply
      initEmploymentDist()

      // Determine the relative wages earned by employed/unemployed in each state
      initWageRatios()

      _expectedLabour = createAggregateExpectationGrid(1)
      _expectedLabour.across(1) << createArray(_aggLabour: _*)

      _expectedProductivity = createAggregateExpectationGrid(1)
      _expectedProductivity.across(1) << _config.getAggregateProductivityShocks()

      addAggregateExpectationListener(afterAggregateExpectationUpdate _)
    }

    /**
     * Start updating aggregates after 20 iterations
     */
    def shouldUpdateAggregates(state: IMWithBonds.this.State): Boolean = state.getPeriod() > 20

    override def calculateAggregateStates(distribution: SimState, aggregateExogenousStates: IntegerArray, state: IMWithBonds.this.State): Array[Double] = {

      val timer = new Timer().start("calcAggs")

      // The capital may already have been calculated in the prior iteration
      var capital = if(state._lastCalculatedCapital != null) state._lastCalculatedCapital.get() else 0

      /* This part is to ensure that the prior calculation was indeed for this state; it calculates the capital by an alternative 
       * method under the assumption that the obtained value is correct, and checks that it coincides within a reasonable margin
       * to that value
       */
      // Calculate the implied wealth from the distribution
      var meanWealth = distribution.mean(_config.getIndividualEndogenousStatesForSimulation().get(0))

      // If no capital was received, a recalc will be necessary - use a reasonable starting point
      if (capital == 0.0) {
        capital = meanWealth - _config._bondBorrowingLimit
      }

      /// Calculate the R implied by the previously calculated capital level
      val impliedR = grossInterest(aggregateExogenousStates.get(0), capital, _aggLabour(aggregateExogenousStates.get(0)))

      // Calculate the capital from the wealth level (which is K*R + B) under the assumption the R is correct
      var capitalAlt = (meanWealth - _config._bondBorrowingLimit) / impliedR

      // If the values are reasonably close, use the received capital
      if (Math.abs(capital / capitalAlt - 1) > 1e-6) {

        // Otherwise, perform a fixed point interation to find the capital level that satisfier K*R+B = W
        capital = new UnivariateBoundedNewtonFixedPointFinder(1e-6, 1e-6).findFixedPoint((value: Double) => {
          var R = grossInterest(aggregateExogenousStates.get(0), value.asInstanceOf[Double], _aggLabour(aggregateExogenousStates.get(0)))

          (meanWealth - _config._bondBorrowingLimit) / R
        }, Array(capitalAlt))(0)

      }

      // Warn if the capital level is outside the grid bounds
      if (capital < _config.getAggregateCapitalLevels().first() || capital > _config.getAggregateCapitalLevels().last()) {
        println("Aggregate capital outside of grid range: " + capital)
      }

      timer.stop()
      // Return the sole aggregate state, wealth
      return Array(capital)
    }

    /**
     * Calculates the difference between the prior transition and the newly calculated one, and saves it for future
     * reference
     */
    def afterAggregateTransitionUpdate(oldExpAggs: DoubleArray, newExpAggs: DoubleArray, state: State) {
      state._aggError = maximumRelativeDifference(oldExpAggs, newExpAggs)
    }

    /**
     * Calculates the steady-state employment levels in the good and bad shock states from the transition probabilities
     */
    def initEmploymentDist() = {

      if (getConfig().hasAggUncertainty()) {
        // Get individual transition probabilities given aggregate good->good or bad->bad transition
        var ggTrans: DoubleArray = _config.getExogenousStateTransition()($, 1, $, 1, 0).copy()
        var bbTrans: DoubleArray = _config.getExogenousStateTransition()($, 0, $, 0, 0).copy()

        // Normalise them so that total transition probs from each state are 1 (since this is now a conditional
        // transition conditional on the aggregate transition, and the overall exo trans is unconditional)
        ggTrans.across(0) /= ggTrans.across(1).sum()
        bbTrans.across(0) /= bbTrans.across(1).sum()

        // Find the steady state of the stochastic processes by repeated multiplication
        for (i <- 1 until 30) {
          ggTrans = ggTrans %* ggTrans
          bbTrans = bbTrans %* bbTrans
        }

        // Normalise again to get rid of tiny errors
        ggTrans.across(0) /= ggTrans.across(1).sum()
        bbTrans.across(0) /= bbTrans.across(1).sum()

        // Create an array to hold the steady state distributions
        _employDist = createArrayOfSize(2, 2)

        // Fill it with the steady states for bad and good aggregate productivity respectively
        _employDist($, 0) << bbTrans(0, $)
        _employDist($, 1) << ggTrans(0, $)

        // Make sure the employment proportions add to 1 in each state
        _employDist(0, $) << _employDist(1, $) * (-1) + 1d

        // Also calculate the aggregate labour supply
        // This is normalised to one in the bad state, so that hours worked per employee is not one!
        _aggLabour = Array(1, _employDist(1, 1) / _employDist(1, 0))

      } else {
        var trans : DoubleArray = _config.getExogenousStateTransition()($, 0, $, 0, 0).copy()

        // Normalise to total probs from each state are 1
        trans\(0) /= (trans\(1) sum )

        // Converge the transition matrix to the steady state
        for (i <- 1 to 30) {
          trans = trans %* trans
        }

        // Normalise again to get rid of small errors
        trans\(0) /= (trans\(1) sum )

        // Create an array to hold the ergodic employment distribution
        _employDist = createArrayOfSize(2, 1)
        _employDist << trans(0, $)

        // Make sure the proportions add to 1
        _employDist(0, 0) = 1d - _employDist(1, 0);
        _aggLabour = Array(1d);
      }
    }

    /**
     * Calculates the after-tax wages implied by aggregate employment, unemployment insurance and
     * a book-balancing government
     */
    def initWageRatios() = {
      _wageRatios = createArrayOfSize(_employDist.size(): _*);

      // The hours worked, so that aggregate labour supply is 1 in bad state
      val hoursWorked = 1d / _employDist(1, 0);

      // Tax per hour to pay for unemployment insurance
      val taxes = (_employDist(0, $)\(0) / _employDist(1, $)) * _config._unemploymentInsuranceRate

      // Unemployed just get the unemployment insurance
      _wageRatios(0, $) += _config._unemploymentInsuranceRate * hoursWorked

      // employed get the taxed hourly wage times hours worked
      _wageRatios(1, $) += (taxes * (-1) + 1) * hoursWorked
    }

    /**
     *  Write some additional values not captured by the framework to the saved state
     */
    override def writeAdditional(state: State, writer: NumericsWriter): Unit = {

      if (getConfig().hasAggUncertainty()) {
        writer.writeArray("indCapitalPolicy", state._capitalPolicy)
        writer.writeArray("indBondsPolicy", state._bondsPolicy)
        writer.writeArray("indCapitalPolicySim", state._capitalPolicyForSimulation)
        writer.writeArray("indBondsPolicySim", state._bondsPolicyForSimulation)
        
        writer.writeArray("bondReturns", state._bondReturns)
        writer.writeArray("expectedInterest", state._expectedInterest)
        writer.writeArray("aggControlsPolicy", state._aggControlsPolicy)
        writer.writeArray("aggExpectedControls", state.getExpectedAggregateControls())

        state._gradSimStates(0).write(writer, "finalGradDensity")

//
//        val derivation = createArrayOfSize(2, 3)
//
//        for (i <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {
//
//          derivation(i, 0) = state._derivativeCalcInfo(i)._1(0)
//          derivation(i, 1) = state._derivativeCalcInfo(i)._2(0)
//          derivation(i, 2) = state._derivativeCalcInfo(i)._3(0)
//
//          //          if( _config._secondOrder) {
//          //            derivation(i, 3) = state._derivativeCalcInfo(i)._4(0)
//          //          }
//        }
//
//        writer.writeArray("derivation", derivation)
      }
    }

    /**
     * Read additional values, probably written above.
     *
     * Also handles the case of reading the end state of a no-agg-risk calculation at the start of
     * an agg-risk calculation
     */
    override def readAdditional(state: State, reader: NumericsReader): Unit = {

      // If the policy that has been read has only one shock level it is an NA state - need to expand
      if (state.getIndividualPolicy().size()(3) == 1) {

        val indPolicy = createIndividualTransitionGrid()
        indPolicy.across(0, 1, 2) << state.getIndividualPolicy()($, $, $, 0, 0, 0)

        state.setIndividualPolicy(indPolicy)

        // Also need to initialise the aggregate transition, becasuet that will be the NA (constant) transition
        initAggregateTransition(state)
      } else {
        // need to read the additional arrays the framework does not know about
        state._capitalPolicy = reader.getArray("indCapitalPolicy").asInstanceOf[DoubleArray]
        state._bondsPolicy = reader.getArray("indBondsPolicy").asInstanceOf[DoubleArray]
        state._bondReturns = reader.getArray("bondReturns").asInstanceOf[DoubleArray]
        state._bondPrices = 1d / state._bondReturns
        state.setCurrentControlsPolicy(reader.getArray("aggControlsPolicy").asInstanceOf[DoubleArray])
        state.setExpectedAggregateControls(reader.getArray("aggExpectedControls").asInstanceOf[DoubleArray])
      }
      
//      val oldReader = new MatlabReaderFactory().create("transOld.mat");
//      state._aggControlsPolicy = oldReader.getArray("aggCtrl").asInstanceOf[DoubleArray]
//      state.setAggregateTransition(oldReader.getArray("aggTrans").asInstanceOf[DoubleArray])
//      
//      state._aggError = 1e-7
//
//      state._gradSimStates = new Array(1)
//      state._gradSimStates(0) = new DiscretisedDistribution(new File("transOld.mat"))
//      _config.setInitialSimState( state._gradSimStates(0))
//      
      // The state was read but this only includes the aggregate transition, not the aggregate expectations.
      // Create them.
      adjustExpectedAggregates(state)

      // Try to read the final distribution from the derivative aggregation process, if present, to use as
      // a starting point for further calculation/simulation
      try {
        var gradSimState = new DiscretisedDistribution( )
        gradSimState.read(reader, "finalGradDensity")

        _config.setInitialSimState(gradSimState)
      } catch {
        case e: Exception => {
          println(s"""No grad density read: ${e.getMessage}""")
        }
      }
      
      
      
    }

    

    /**
     * Provides the initial distribution for simulation, also used by the derivative aggregation process
     */
    def initialDensity(state: State): com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution = {

      if (getConfig().hasAggUncertainty()) {

        // If the final density from DA was found, use that
        if (state._gradSimStates != null) {
          return state._gradSimStates(0)
        }

        // Otherwise use the initial distribution configured for DA
        return adjustDistributionToState(_config._daDist, 0)

      } else {
        // With no aggregate risk, use the initial dist from the young paper and adjust to our requirements
        val youngDist = getNumerics().readCSV(ClassLoader.getSystemResourceAsStream("pdistyoung.txt"), '\t')

        /*
  			 * Copy the second two columns, the first contains cap levels
  			 */
        val initialDist = createArrayOfSize(2001, 2)

        initialDist($, 0) << youngDist($, 1)
        initialDist($, 1) << youngDist($, 2)

        initialDist.modifying().across(1).divide(initialDist.across(0).sum())

        initialDist.modifying().across(1).multiply(_employDist($, 0))

        val simState = getSimulator().createState()

        simState._density = initialDist
        simState._overflowProportions = createArrayOfSize(1, 2)
        simState._overflowAverages = createArrayOfSize(1, 2)

        return simState
      }
    }

    /**
     * Initialises the State object prior to commencing the calculation
     */
    def initialState(): IMWithBonds.this.State = {

      // Create a state object for this configuration 
      val state = new State(_config)

      /* Create the initial guess for the individual capital transition
      */
      val initialTransition = createIndividualTransitionGrid()

      initialTransition.across(0) << (_config.getIndividualWealthLevels() / _config._growth)

      // Adjust the transition for possible future permanent shocks
      // This dimension is the second to last (the last is of size 1 and is the number of variables in the
      // grid/function)
      initialTransition.modifying().across(initialTransition.numberOfDimensions - 2).divide(
        _config.getPermanentAggregateShockLevels())

      val delta = 0.025;

      // Adjust it according to individual shock
      val kpAdjust = createArray(1d - delta, 1d + delta / 10d);

      initialTransition.modifying().across(1).multiply(kpAdjust)

      // Set the transition on the initial state
      state.setIndividualPolicy(initialTransition)

      state._growthFactor = _config._growth

      state._derivativeCalcInfo = new Array[DerivationInformation](2)

      /* Prepare the grids of aggregate prices, which vary along the aggregate capital and
       * productivity levels
       */
      val wageMatrix = createArrayOfSize(_interestMatrix.size(): _*)

      for (K_idx <- 0 until _config.getAggregateCapitalLevels().numberOfElements()) {
        for (P_idx <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {

          // Get the capital and Labour supply at the current point
          val K = _config.getAggregateCapitalLevels()(K_idx)
          val L = _aggLabour(P_idx)

          _interestMatrix(K_idx, P_idx) = grossInterest(P_idx, K, L)
          wageMatrix(K_idx, P_idx) = wage(P_idx, K, L)
        }
      }

      val individualWages = createArrayOfSize(_config.getIndividualExogenousStates().get(0).numberOfElements(),
        _config.getAggregateCapitalLevels().numberOfElements(), _config.getAggregateProductivityShocks().numberOfElements())

      // Fill the wage matrix across the aggregate capital & productivity dimensions
      individualWages\(1, 2) << wageMatrix

      // Multiply the aggregate wage by the actual wage individual gets, due to unemp benefit and taxes
      individualWages\(0, 2) *= _wageRatios

      // The liquid assets at the beginning of the period are prior capital with interest + wages
      state._normalisedLiquidAssets = createIndividualTransitionGrid()
      state._normalisedLiquidAssets\(0) << _config.getIndividualWealthLevels()

      // Normalize the capital carried over by the permanent shock
      state._normalisedLiquidAssets.\(state._normalisedLiquidAssets.numberOfDimensions - 2) /=
        _config.getAggregateNormalisingExogenousStates().get(0)

      // Add the wage
      state._normalisedLiquidAssets.across(1, 2, 4) += individualWages

      // We need to do the same for the simulation grid, which may have a different size
      val laSize = state._normalisedLiquidAssets.size().clone()
      laSize(0) = _config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements()

      val simLA = createArrayOfSize(laSize: _*)
      simLA.across(0) << (_config.getIndividualEndogenousStatesForSimulation().get(0))

      simLA.across(simLA.numberOfDimensions - 2) /= _config.getAggregateNormalisingExogenousStates().get(0)

      // Add the wage
      simLA.across(1, 2, 4) += individualWages

      state.setStartOfPeriodStatesForSimulation(simLA)

      initAggregateTransition(state)

      // The end of period states are just the wealth levels to be carried over
      val simCapitalGrid = createSimulationGrid()

      simCapitalGrid.across(0) << (_config.getIndividualEndogenousStatesForSimulation().get(0))

      state._simCapitalGrid = simCapitalGrid

      _config.setInitialSimState(initialDensity(state))

      return state
    }

    /**
     * Initialises the assumed aggregate state and control policy functions
     */
    private def initAggregateTransition(state: com.meliorbis.economics.incompletemarkets.bonds.IMWithBonds.State): Unit = {

      val aggTrans = createAggregateVariableGrid()

      /* Now initialise the aggregate capital transition 
       */
      if (getConfig().hasAggUncertainty) {
        aggTrans.across(0) << (squeezeToMean(_config.getAggregateCapitalLevels(), 0.5))

        // Then adjust it for the expected shock
        aggTrans.modifying().across(2).multiply(_config.getAggregateProductivityShocks()).
          // Finally, make sure none of the capital levels transitioned to is outside the grid bounds
          map(cutToBounds(_config.getAggregateCapitalLevels().first, _config.getAggregateCapitalLevels().last))

        /* Also need to initialise the expecations of bond prices - which we initialise to the mean value over the array
         */
        val meanPrice = _config.getRiskFreeDiscount().sum() / _config.getRiskFreeDiscount().numberOfElements()

        val expectedBondPrice = createAggregateExpectationGrid()
        expectedBondPrice << meanPrice

        state.setExpectedAggregateControls(expectedBondPrice)

        state._aggControlsPolicy = createAggregateVariableGrid()
        state._aggControlsPolicy << meanPrice
      } else {
        // In the case with no aggregate uncertainty,  
        // K is constant so any capital level just stays the same
        aggTrans.across(0) << (_config.getAggregateCapitalLevels())
      }

      //      aggTrans.<<(0,1)(_numerics.readFormattedCSV[Double]("java_agg_trans.csv").asInstanceOf[DoubleArray])
      state.setAggregateTransition(aggTrans)

      if (getConfig().hasAggUncertainty()) {
        // Create expectations from the initialised transition
        adjustExpectedAggregates(state)
      } else {
        val expectedAggs = createAggregateExpectationGrid()

        // Future expectation is same as current value
        expectedAggs.across(3) << _config.getAggregateCapitalLevels()

        state.setExpectedAggregateStates(expectedAggs)

        // calculate relevant interest and wages
        afterAggregateExpectationUpdate(null, null, state)
      }
    }

    /* Helper methods to calculate wages and interest
     */
    def grossInterest(pIdx: Int, K: Double): Double = grossInterest(_config.getAggregateProductivityShocks()(pIdx), K, _aggLabour(pIdx))

    def wage(pIdx: Int, K: Double): Double = wage(_config.getAggregateProductivityShocks()(pIdx), K, _aggLabour(pIdx))

    def grossInterest(pIdx: Int, K: Double, L: Double): Double = grossInterest(_config.getAggregateProductivityShocks()(pIdx), K, L)

    def wage(pIdx: Int, K: Double, L: Double): Double = wage(_config.getAggregateProductivityShocks()(pIdx), K, L)

    def grossInterest(productivity: Double, K: Double, L: Double): Double = 1 + _config._productionFunction.netReturn(productivity, K, L)

    def wage(productivity: Double, K: Double, L: Double): Double = _config._productionFunction.wage(productivity, K, L)

    /**
     * For this model, the only control is the bond price, and the determinant for it is the net bond demand - which must be 0, since there is
     * only inter-agent lending.
     */
    override def calculateControlDeterminants(distribution: SimState, transitionAtAggs: com.meliorbis.numerics.generic.primitives.DoubleArray[_], currentAggs: Array[Double],
      currentShocks: IntegerArray, state: State): DoubleArray = {

      val timer = new Timer().start("controls")

      // First, restrict the bonds policy to the current shocks - the zero is for the permanent shock, currently not used
      // The second 0 is to select the only variable in the policy
      var policy = state.bondsPolicyForSimulation($, $, $, $, currentShocks.get(0).doubleValue(), 0, 0)

      // Then, interpolate it to the current aggregate capital level
      policy = interpDimension(policy, _config.getAggregateCapitalLevels(), currentAggs(0), 2)

      // The start-of-period states don't depend on the control, so we can index 0 there
      val sopStatesAtPoint = interp(state.getStartOfPeriodStatesForSimulation()($, $, $, 0, currentShocks.get(0).doubleValue(), 0, 0),
        spec(2, _config.getAggregateCapitalLevels(), currentAggs(0)))

      val determinants = (sumAcrossDistribution(policy, sopStatesAtPoint, distribution.asInstanceOf[DiscretisedDistribution]))

      if (determinants.first() * determinants.last() > 0) {
        println("Bond demand does not intersect 0!")
      }
      timer.stop()
      return determinants
    }

//    val _capLevels = new ThreadLocal[DoubleArray]
//    val _bondLevels = new ThreadLocal[DoubleArray]

    /**
     * This method is used during simulation to calculate the state at the end of this period relative to the grid at the beginning of next period
     *
     * Because the next period interest is paid only capital but not bonds, this method 'discounts' bonds by the interest rate in the next period so
     * that then the mechanism for moving to next period states from current start-of-period states will work appropriately (by paying interest on everything)
     */
    override def getTransitionAtAggregateState[N <: Number](
        distribution: SimState, 
        state: State, 
        aggregateStates: JavaDoubleArray, 
        aggregateControls: JavaDoubleArray, 
        currentShocks: MultiDimensionalArray[N, _], 
        nextPeriodShocks: MultiDimensionalArray[N, _]): DoubleArray = {

      var shocks = currentShocks.asInstanceOf[IntegerArray]
      var futureShocks = nextPeriodShocks.asInstanceOf[IntegerArray]

      if (getConfig().hasAggUncertainty()) {
        val timer = new Timer().start("getTrans")

        // Create appropriate interpolation structures to interpolate to the aggregate variables
        val dimSpecs = Array(spec(2, _config.getAggregateCapitalLevels(), aggregateStates(0)), spec(3, _config.getRiskFreeDiscount(), aggregateControls(0)))

        val capitalAtPoint = interp(state.capitalPolicyForSimulation($, $, $, $, shocks.get(0).doubleValue(), 0, $), dimSpecs: _*)
        val bondsAtPoint = interp(state.bondsPolicyForSimulation($, $, $, $, shocks.get(0).doubleValue(), 0, $), dimSpecs: _*)
        val sopStatesAtPoint = interp(state.getStartOfPeriodStatesForSimulation()($, $, $, $, shocks.get(0).doubleValue(), 0, $), dimSpecs: _*)

        // From the capital policy one can calculate next period's capital
        val nextPeriodCapital = sumAcrossDistribution(capitalAtPoint, sopStatesAtPoint, distribution.asInstanceOf[DiscretisedDistribution])(0)
        val nextPeriodBonds = sumAcrossDistribution(bondsAtPoint, sopStatesAtPoint, distribution.asInstanceOf[DiscretisedDistribution])(0)

        val nextPeriodInterest = grossInterest(futureShocks.get(0), nextPeriodCapital, _aggLabour(futureShocks.get(0)))

        if( nextPeriodCapital > _config.getAggregateCapitalLevels().last() ||
            nextPeriodCapital < _config.getAggregateCapitalLevels().first() ) {
          println(s"Error: Capital out of bounds ${nextPeriodCapital}")
        }
        // remember the calculated capital level
        state._lastCalculatedCapital.set(nextPeriodCapital)

//        _capLevels.set(capitalAtPoint)
//        _bondLevels.set(bondsAtPoint)

        // Interest will be added to the overall wealth by the simulation mechanism; for capital this is fine, but not for bonds, so we need to un-add it!
        val trans = (capitalAtPoint * nextPeriodInterest) + bondsAtPoint + _config._bondBorrowingLimit

        trans ->= (a => if (a < -1e-5) throw new RuntimeException("Underflow") else if (a < 0) 0 else a)

        timer.stop()

        trans
      } else {
        // Create appropriate interpolation structures to interpolate to the aggregate variables
        val dimSpecs = Array(spec(2, _config.getAggregateCapitalLevels(), aggregateStates(0)))

        val capitalAtPoint = interp(state.getIndividualPolicyForSimulation()($, $, $, $, shocks.get(0).doubleValue(), 0, $), dimSpecs: _*)
        val sopStatesAtPoint = interp(state.getStartOfPeriodStatesForSimulation()($, $, $, $, shocks.get(0).doubleValue(), 0, $), dimSpecs: _*)

        // From the capital policy one can calculate next period's capital
        val nextPeriodCapital = sumAcrossDistribution(capitalAtPoint, sopStatesAtPoint, distribution.asInstanceOf[DiscretisedDistribution])(0) - _config._bondBorrowingLimit

        // remember the calculated capital level
        state._lastCalculatedCapital.set(nextPeriodCapital)

        val nextPeriodInterest = grossInterest(futureShocks.get(0), nextPeriodCapital, _aggLabour(futureShocks.get(0)))

        // Here, the end of period states need to be adjusted for the expected interest rate!
        //        val simCapitalGrid = createSimulationGrid()
        //
        //        simCapitalGrid.across(0) << ((config.getIndividualEndogenousStatesForSimulation().get(0))/nextPeriodInterest + _config._bondBorrowingLimit*(1d-1d/nextPeriodInterest))
        //        
        //        state._simCapitalGrid = simCapitalGrid

        // Both capital and bonds are held at cost, but the next period needs the sale value
        val trans = capitalAtPoint * nextPeriodInterest + _config._bondBorrowingLimit * (1d - nextPeriodInterest)

        trans.modifying().map((a: Double) => if (a < -1e-3) throw new RuntimeException("Underflow") else if (a < 0) 0 else a)

        trans
      }
    }

   
    def adjustDistributionToState(distribution: DiscretisedDistribution, state: Int): DiscretisedDistribution = {
      val simState = distribution.clone()

      val unemp = _employDist(0, state)

      // Get the unemployment level in the provided distribution
      val empProps = determineEmploymentDistribution(simState);

      // Adjust the distribution to the appropriate unemployment level
      if (empProps(0) - unemp > 1e-10) {
        simState._density($, 1).modifying().add(simState._density($, 0).multiply(1d - unemp / empProps.get(0)))
        simState._density($, 0).modifying().multiply(unemp / empProps.get(0))

        simState._overflowProportions($, 1).modifying().add(simState._overflowProportions($, 0).multiply(1d - unemp / empProps.get(0)))
        simState._overflowProportions($, 0).modifying().multiply(unemp / empProps.get(0))
      } else {
        simState._density($, 0).modifying().add(simState._density($, 1).multiply(1d - (1d - unemp) / empProps.get(1)));
        simState._density($, 1).modifying().multiply((1d - unemp) / empProps.get(1));

        simState._overflowProportions($, 0).modifying().add(simState._overflowProportions($, 1).multiply(1d - (1d - unemp) / empProps.get(1)));
        simState._overflowProportions($, 1).modifying().multiply((1d - unemp) / empProps.get(1));
      }

      return simState;
    }

    def determineEmploymentDistribution(distribution: DiscretisedDistribution): DoubleArray = {
      distribution._density.across(0).sum().add(distribution._overflowProportions)
    }

    /**
     * Adjusts the expected capital level for permanent shocks (in the next period) and also calculates
     * expected prices given the expected factor inputs
     */
    def afterAggregateExpectationUpdate(oldVal: DoubleArray, newVal: DoubleArray, state: State) {

      // Adjust for future permanent shocks
      state.getExpectedAggregateStates().modifying().across(2).divide(_config.getAggregateNormalisingExogenousStates().get(0))

      // Update expected prices
      val Array(w, r) = (state.getExpectedAggregateStates() :: _expectedLabour :: _expectedProductivity) :-> (in => {
          val Array(k,l,p) = in

          Array(wage(p, k, l), grossInterest(p,k,l))
        })

      state._expectedInterest = r
      state._expectedWage = w

      if (getConfig().hasAggUncertainty()) {
        updateBondReturns(state)
      }
    }

    def updateBondReturns(state: State) {
      // Calculate the bonds yields from expected interest and the risk free discount
      val aggTransProbs = _config.getExogenousStateTransition()(0, $).across(1).sum()

      // Expected interest does not vary by current discount rate...
      val uncondExpectedInterest = (state._expectedInterest \ (0, 1, 2) * aggTransProbs) \ (1, 2) sum

      // ... but the bond returns do
      state._bondReturns = createArrayOfSize((_config.getRiskFreeDiscount().numberOfElements() +: uncondExpectedInterest.size()): _*)
      state._bondReturns \ (1 until state._bondReturns.numberOfDimensions(): _*) << uncondExpectedInterest
      state._bondReturns \ (1 until state._bondReturns.numberOfDimensions(): _*) -= state._expectedInterest($, 0, 0)
      
      // Note that returns only depend very mildly on the risk free discount because the number multiplied HERE is small compared to...
      state._bondReturns \ (0) *= _config.getRiskFreeDiscount()
      // ... the number added HERE!
      state._bondReturns \ (1 until state._bondReturns.numberOfDimensions(): _*) += state._expectedInterest($, 0, 0)
      // The bond price is one over the return
      state._bondPrices = 1d / state._bondReturns
      
    }

  }
  
  

  class IM_EGM_Solver(model: Model, config: Config) extends EGMIndividualProblemSolverWithControls[Config, State, Model](model, config) {
    
    override def initialise(state: State) {
        setStartOfPeriodStates(state._normalisedLiquidAssets)
      // The end of period states are just the capital levels to be carried over
      val wealthGrid = _model.createIndividualVariableGrid()

      // Adjust for the possible borrowing in bonds, which affects interest payments
      wealthGrid\0 << (_config.getIndividualWealthLevels() - _config._bondBorrowingLimit)

      if (!_model.getConfig().hasAggUncertainty()) {
        // Divide through by net return to get the actual investment made to achieve next period's wealth
        wealthGrid\(2, 3) /= _model._interestMatrix
      }

      // Readjust for the lower bound in bold holdings
      wealthGrid += _config._bondBorrowingLimit
      setEndOfPeriodStates(wealthGrid)
    }
    
    def calculateFutureConditionalExpectations(exogenousTransition: Array[Int], currentAggs: Array[Int], state: State): DoubleArray = {

      val currentAggStateIndex = exogenousTransition(1);
      val futureIndStateIndex = exogenousTransition(2);
      val futureAggStateIndex = exogenousTransition(3);

      val futurePermShockIndex = exogenousTransition(4);

      val aggCapIndex = currentAggs(0)

      if (_model.getConfig().hasAggUncertainty()) {

        /* The expected future amount invested in total
    		 */
        val xpp = state.getExpectedIndividualTransition()(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, $, futureIndStateIndex)

        /* Collect expected future aggregates
    		 */
        val R = state._expectedInterest(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, 0)

        // Need to adjust the aggregate wage for the individuals employment state and dt
        val w = state._expectedWage(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, 0) * _model._wageRatios(futureIndStateIndex, futureAggStateIndex)
        val permShock = _config.getAggregateNormalisingExogenousStates().get(0)(futurePermShockIndex)

        /* Calculate the value of the euler condition conditional on the future states
         */
        return calculateConditionalFutureEuler(xpp, R, w, permShock, _config._discountRate, _config._growth)
      } else {
        /* The expected future amount invested in total
    		 */
        val xpp = state.getExpectedIndividualTransition()(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, $, futureIndStateIndex)

        /* Collect expected future aggregates
    		 */
        val R = state._expectedInterest(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, 0)

        // Need to adjust the aggregate wage for the individuals employment state and dt
        val w = state._expectedWage(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, 0) * _model._wageRatios(futureIndStateIndex, futureAggStateIndex)
        val permShock = _config.getAggregateNormalisingExogenousStates().get(0)(futurePermShockIndex)

        /* Calculate the value of the euler condition conditional on the future states
         */
        return calculateConditionalFutureEuler(xpp, R, w, permShock, _config._discountRate, _config._growth)
      }
    }

    def calculateConditionalFutureEuler(xpp: DoubleArray, futureR: Double, futureW: Double, futureStochasticNormalisation: Double,
      discountRate: Double, growth: Double): DoubleArray =
      {
        /* First, calculate consumption as starting wealth WTH NTEREST plus wages minus ending wealth
       */
        val individualWealthLevels = _config.getIndividualWealthLevels()

        // Calculate consumption at this point
        val c = individualWealthLevels ~ xpp -> ((xp: Double, xpp: Double) => xp + futureW - xpp);

        /* Now determine the future euler value, i.e. the discounted marginal utility of consumption
       */
        val fce = c map ((c: Double) => _config._utilityFunction.marginalUtility(0, c))

        // Return that value
        fce
      }

    /**
     * Given conditional expectations, conditional on both future individual and aggregate variables,
     * returns a tuple of twp arrays: the first gives the expn given that the future aggregate shock is
     * bad, the second assumes it is good; the future individual shock has been collapsed.
     *
     * Both arrays have the probability of the aggregate transitions to the relevant state multiplied in
     * already
     */
    def summariseByAggregate(fce: DoubleArray, state: State): (DoubleArray, DoubleArray) = {

      /* First, multiply the expected values by the probability of their occurring, and then summarise over
       * future individual states so the future is only conditional on aggregate states
       * 
       * NOTE: Separately for currently employed and unemployed, since that dimension does not exist on the input array
       */
      val reducedExpnUnemp: DoubleArray = (fce \ (0, 1, 2, 3) * _config.getExogenousStateTransition()(0, $)) \ (1, 3) sum
      val reducedExpnEmp: DoubleArray = (fce \ (0, 1, 2, 3) * _config.getExogenousStateTransition()(1, $)) \ (1, 3) sum

      // Create an array with an additional dimension for the current bond price
      val sizeWithBondReturns = _config.getRiskFreeDiscount().numberOfElements() +: reducedExpnEmp.size()
      val expnIfGood = createArrayOfSize(sizeWithBondReturns: _*)

      // ... and fill it with the expectations conditional on a good future aggregate shock
      expnIfGood($, 0, $) \ (1, 2, 3, 4) << reducedExpnUnemp($, 1)
      expnIfGood($, 1, $) \ (1, 2, 3, 4) << reducedExpnEmp($, 1)

      // Ditto for a bad future aggregate shock
      val expnIfBad = createArrayOfSize(sizeWithBondReturns: _*)
      expnIfBad($, 0, $) \ (1, 2, 3, 4) << reducedExpnUnemp($, 0)
      expnIfBad($, 1, $) \ (1, 2, 3, 4) << reducedExpnEmp($, 0)

      (expnIfBad, expnIfGood)
    }

    /**
     * The multiplier to apply to expectations assuming a bad future state which makes them
     * equivalent to expectations assuming a good future state, in the sense that equal values
     * would mean identical choices in the prior period. See Paper, equation A.46
     * 
     * Returns: (R'- - 1/p)/(1/p - R'+)
     * 
     */
    def badExpectationMultiplier(state: State): DoubleArray = {
      /* Expectations are in general not dependent on bond price, so we need
       * add that dimension
       */
      val intermediate = createArrayOfSize(_config.getRiskFreeDiscount().numberOfElements() +: state._expectedInterest.size: _*)
      
      // First add the expected interest, which is conditional on future agg state
      intermediate \ (1 until intermediate.numberOfDimensions(): _*) << state._expectedInterest
      // then subtract the current bond returns
      intermediate \ (0, 1, 4, 5) -= state._bondReturns

      // Now take the slice for a bad *future* state and divide it by the negative value
      // of the good future state, to get the required value
      val multiplier = intermediate($, $, 0) / ((-1d) * intermediate($, $, 1))

      multiplier
    }

    override def individualUnconditionalExpectation(fce: JavaDoubleArray, state: State): DoubleArray = {

      val futureConditionalExpectations = fce

      if (_model.getConfig().hasAggUncertainty()) {

        /* Calculate expected outcomes conditional on future agg shock realisation, but not future ind shock
         * 
         * NOTE: the fce array does not have a dimension for current ind shock, so we have to do it separately
         * for the two shock realisation
         */
        val (expnIfBad, expnIfGood) = summariseByAggregate(futureConditionalExpectations, state)

        /* Above expectations are on : p_t, e_t, A_t, Z_t+1, K_t, k_t
         */
        val multiplier = badExpectationMultiplier(state)

        // Adjust the expected utility conditional on bad aggregate state by the multiplier - now same values in the
        // good and bad arrays indicate same prior decisions!
        val equivExpIfBad = expnIfBad \ (0, 2, 3, 5) * multiplier($, $, 0)

        if (equivExpIfBad.min() < 0) {
          println("Problem: u'(c) < 0!")
        }

        val xGrid = createArrayOfSize(equivExpIfBad.size(): _*)
        xGrid.across(4) << _config.getIndividualWealthLevels()

        /* This gives the wealth levels in the bad state correspond to the wealth levels on the xGrid in the good state
         */
        val badX = interpolateFunction(equivExpIfBad, xGrid, 4, expnIfGood)

        val Rdiff = state._expectedInterest($, 1, 0, $, 0) - state._expectedInterest($, 0, 0, $, 0)

        /* Given the wealth levels in the good state (xGrid) and those in the bad state (badX)
         * we can then calculate the implied levels of capital and bond holdings from the differing
         * interest paid in each state - of course taking constraints into account
         */
        // Capital that would be optimally held
        var impliedK = ( /*good x'*/ xGrid - badX).across(2, 3) / Rdiff

        // Capital constraint
        impliedK.modifying().map((k: Double) => if (k < 0) 0 else k)

        // Implied bond holdings 
        val impliedBonds = xGrid - (impliedK \ (2, 3) * state._expectedInterest($, 1, 0, $, 0))

        // Bond borrowing constraint
        impliedBonds.modifying().map((b: Double) => if (b < 0) 0 else b)

        // Need to readjust k for those where bonds were constrained
        impliedK = (xGrid - impliedBonds) \ (2, 3) / state._expectedInterest($, 1, 0, $, 0)

        /* The portfolio choices of the agent in the current period have been determined
         */
        state._impliedCapital = impliedK

        // So far everything was measured from the limit; adjust
        state._impliedBonds = impliedBonds - _config._bondBorrowingLimit

        /* Because of the constraints, the badX discovered so far will not be the equivalent to actual
         *  outcomes in a good state. We need to calculate the equivalent ones directly using the assets!
         */
        // The realised xprimes in the bad future aggregate state
        val xprimeIfBad = impliedK \ (2, 3) * state._expectedInterest($, 0, 0, $, 0) + impliedBonds

        // The consequent contributions to expectations of future outcomes in the bad state
        val adjustedExpnIfBad = interpolateFunction(xGrid, expnIfBad, 4, xprimeIfBad, params.constrained)

        // The discounting depends on the constraints: if the capital is constrained, discount by bonds rate.
        // If bonds are also constrained then the agent is contrained and will not save, which will be 
        // handled in calculateImpliedStartOfPeriodState
        val discountByR = impliedK -> ((k: Double) => (if (k == 0d) 0d else 1d))

        val discountByP = 1d - discountByR

        val discountedIfGood = expnIfGood * (discountByR \ (2, 3) * (state._expectedInterest($, 1, 0, $, 0) * _config._discountRate) +
          discountByP \ (0, 2, 3) * (state._bondReturns * _config._discountRate))

        val discountedIfBad = adjustedExpnIfBad * (discountByR \ (2, 3) * (state._expectedInterest($, 0, 0, $, 0) * _config._discountRate) +
          discountByP \ (0, 2, 3) * (state._bondReturns * _config._discountRate))

        val expectations = discountedIfGood + discountedIfBad

        return expectations
      } else {
        // Since bonds and capital pay the same return, the only difference is that bond holdings can be negative - which is only
        // relevant when aggregating.
        // We do have to adjust the future utility for the future interest, however (discounting, in effect)
        futureConditionalExpectations.across(0, 2, 3, 4).modifying().multiply(state._expectedInterest * _config._discountRate)

        return super.individualUnconditionalExpectation(futureConditionalExpectations, state)
      }
    }

    /**
     * Given expectations over the future euler, determine the implied current liquid assets
     */
    def calculateImpliedStartOfPeriodState(individualExpectations: com.meliorbis.numerics.generic.primitives.DoubleArray[_], state: State): DoubleArray = {

      if (_model.getConfig().hasAggUncertainty()) {
        // From capital holdings (bought for 1) and bond holdings (bought for the bond price), figure out the wealth invested this period
        // Note that we must perform here the adjustment for the fact that bonds can be negative but at a price
        _eopStates = state._impliedCapital + ((state._impliedBonds).across(0, 2, 3) * state._bondPrices)

        // Adjust back to a positive level (i.e. relative to minimum wealth at SOP) for interpolations
        _eopStates += _config._bondBorrowingLimit

      }

      // Then we can use the marginal utility condition to calculate the implied current consumption, which together with wealth carried over
      // yields the implied start of period wealth (including income)
      val impliedLiquidAssets = individualExpectations ~ _eopStates -> ((futureEuler: Double, xp: Double) => {

        // We calculated the marginal utility in the euler calculation, now invert it for c
        val c = _config._utilityFunction.inverseMarginalUtility(futureEuler);

        // consumption plus end of period assets give start of period assets
        c + xp
      })

      if (_model.getConfig().hasAggUncertainty()) {
        val ila = _model.createIndividualTransitionGrid()
        ila << impliedLiquidAssets.arrangeDimensions(Array(4, 1, 3, 0, 2, 5))
  
        val eopStates = _eopStates
        _eopStates = _model.createIndividualTransitionGrid()
        _eopStates << eopStates.arrangeDimensions(Array(4, 1, 3, 0, 2, 5))


        var endingCapital = _model.createIndividualTransitionGrid()
        endingCapital << state._impliedCapital.arrangeDimensions(Array(4, 1, 3, 0, 2, 5))
        var endingBonds = _model.createIndividualTransitionGrid()
        endingBonds << state._impliedBonds.arrangeDimensions(Array(4, 1, 3, 0, 2, 5))

        // Interpolate both the capital and bonds policy to the wealth grid
        state._capitalPolicy = interpolateIndividualChoiceToGrid(ila, endingCapital)
        state._bondsPolicy = interpolateIndividualChoiceToGrid(ila, endingBonds)
        
        return ila
      }
      else {
        return impliedLiquidAssets
      }
    }

    override def updateError(oldPolicy: JavaDoubleArray, newPolicy: JavaDoubleArray, state: State){

      state._policyForSimulation = null

      if (state.getPeriod() % 10 == 5) {
        state._indError = maximumRelativeDifferenceSpecial(newPolicy, oldPolicy)
      }

    }
  }

  class IM_DA_Solver(model: Model, config: Config, 
      sim: DiscretisedDistributionSimulator) extends 
        DerivativeAggregationSolver[Config, State, Model](model, config, sim) {

    // Use a log linear rather than a linear transition
    useLogs(true)

 //   secondOrder(true)
    
    setNewWeight(1)

    // This grid needs to be the size of the simulation grid
    var _ones: DoubleArray = _

    var _zeros: DoubleArray = _

    // This grid needs to be the size of the simulation grid
    var _thetaDeriv: DoubleArray = _
    
     val thetaDists = new Array[DiscretisedDistribution](2)

    var _state: State = _
    addTransitionListener(model.afterAggregateTransitionUpdate _)

    override def initialise(state: State) = {
      
      _ones = model.createSimulationGrid() += 1
      _zeros = model.createSimulationGrid()

      _thetaDeriv = model.createSimulationGrid()\0 << config.getIndividualEndogenousStatesForSimulation.get(0)
      
      _state = state
    }

    override def deriveDeterminantAggregationByAggregateState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      _zeros
    }

    override def deriveAggregationByAggregateState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      // Nothing to do - only one aggregation
      null
    }

    override def deriveAggregationByIndividualState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double], current: Boolean): DoubleArray = {

      // The individual states include interest, which must be deducted.
      // Note that the bond holdings do not impact here since they must always sum to 0 in total

      
      // THIS CONSTRUCT DEALS WITH THE FACT THAT CURRENT PERIOD AND FUTURE PERIOD RELATIONS
      if (current) {
      // ARE DIFFERENT
        // Current
        val a = _config._capitalShare
        val z = _config.getAggregateProductivityShocks()(aggExoStates(0))

        //deriv += 1d/ _model.grossInterest(aggExoStates(0), aggregateStates(0), _model._aggLabour(aggExoStates(0)))
        _model.createSimulationGrid() << 1d / (1 - _config._depreciation + a * a * z * Math.pow(aggregateStates(0) / _model._aggLabour(aggExoStates(0)), a - 1))

      } else {
        // Future
        _ones
      }
    }

    override def deriveIndividualTransformationByTheta(transformation: Int, indState: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      
      //_thetaDeriv
      val currentProdLevel = aggExoStates(0)
      
      val centralDist = _state._gradSimStates( currentProdLevel )
      val centralK = aggregateStates(0)
      
      // Get the distribution used for DA with the requested level of productivity
      var dist = thetaDists(currentProdLevel)//_state._gradSimStates(aggExoStates(0))
      
   //   _state._dthetaDists(2*currentProdLevel) = dist
      
      // Simulate the same level of productivity for one more period
      val shocks = getNumerics().newIntArray(2,2)
      
      shocks.set(currentProdLevel,0,0)
      shocks.set(currentProdLevel,1,0)

      var exoStatesArray = createIntArray(aggExoStates:_*)
      
      var secondDist = _simulator.simulateShocks(centralDist, shocks, 
                _model, _state, SimulationObserver.silent()).getFinalState()
      
  //    _state._dthetaDists(2*currentProdLevel+1) = secondDist
      
      val K = _model.calculateAggregateStates(dist, exoStatesArray, _state)(0)
      val secondK = _model.calculateAggregateStates(secondDist, exoStatesArray, _state)(0)
      
      // Calculate the transformed k_i for each k_i, and then the numerical derivative implied
      // Note that this assumes dtheta to be 1 in this case.
      
      val ki = com.meliorbis.economics.util.Util.determineTransformation(dist, secondDist, _config.getIndividualEndogenousStatesForSimulation().get(0))
      val dki = (ki)\(0) - _config.getIndividualEndogenousStatesForSimulation().get(0)

      dki(0,$) << 0d
      
      // Measuring the dki in the direction away from dist
      dki /= ( secondK - K )
      
      /* The commented section below is an attempt at achieving a second order dtheta, but to no avail
       */
//      if(_config._secondOrder) {
//        
//        val dki1 = transformedki(centralDist,secondDist)\(0) - _config.getIndividualEndogenousStatesForSimulation().get(0)
//        val dK1 = (secondK - centralK) 
//        dki1 /= dK1
//        dki1(0,$) << 0d
//        
//        val dki2 = transformedki(dist,centralDist)\(0) - _config.getIndividualEndogenousStatesForSimulation().get(0)
//        
//        val dK2 = (centralK - K)
//        // Measuring dki2 in the direction away from dist
//        dki2 /= dK2 
//        dki2(0,$) << 0d
//        
//        val factor = 1d/((secondK - K)/2d)*(if(currentProdLevel == 0) -1d else 1d) //* This is from assuming the derivs are accurate at dtheta = 1/2 and (dtheta2)/2*/
//        
//        _state._doubleDerivs(currentProdLevel) = (dki1~dki2)->((a: Double, b: Double) => if(a == 0d || b == 0d) 0d else (a-b)*factor)
//      }
      
//      _state._dtheta(currentProdLevel) = dki
      
      dki
    }

    override def deriveDeterminantAggregationByIndividualControl(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double], foo: JavaDoubleArray): DoubleArray = {
      _ones
    }

    override def prepareAggregatePolicyCalculation(state: State) {

      /* Need to update the distributions for calculating grad here
       */

      // Don't do this in parallel as the starting point of successive states is the initial point of the last
      for (level <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {

        updateGradDensity(level, state)

      }
    }

    def updateGradDensity(level: Int, state: State) {

      val timer = new Timer().start("gradDensity")

      if (state._gradSimStates == null) {
        state._gradSimStates = prepareDistributionsForGrad(_config._daDist)
      }
 
      val startingProdLevel = (if(level == 0) _config.getAggregateProductivityShocks().numberOfElements() else level) - 1
      val startingDist = state._gradSimStates(startingProdLevel)

      // Expected duration of good/bad periods in this calibration
      // Note that the first period is the 'starting' period and does not count
      val periods = 8 + 1;

      var shockArray = createIntArrayOfSize(periods-1, 2)
      var fillArray = createIntArrayOfSize(periods-1).add(level.asInstanceOf[Integer])
      
      // Set the shock to be simulated to the level the density was requested for
      shockArray.at(-1, 0).fill(fillArray)

      // The first 4 should be the other shock state
      for (j <- 0 until (5)) {
        shockArray.set(startingProdLevel.asInstanceOf[Integer],j,0)
      }        
      
      // Then alternatingly 8 periods each until the last for, which are this shock state
      for (j <- 0 until (periods / 8)) {
      	if (j % 2 == 1) {
          for (i <- j * 8+5 until (j + 1) * 8 + 5) {
            shockArray.set(startingProdLevel.asInstanceOf[Integer], i, 0)
          }
        }
      }
        
        thetaDists(level) = _simulator.simulateShocks(startingDist, shockArray,
            _model, state, 
            SimulationObserver.silent[DiscretisedDistribution, Integer]()).
              getFinalState()

        shockArray = createIntArrayOfSize(2, 2)
        fillArray = createIntArrayOfSize(2).add(level.asInstanceOf[Integer])
        shockArray.at(-1, 0).fill(fillArray)

        state._gradSimStates(level) = _simulator.simulateShocks(
            thetaDists(level), shockArray, _model, state, 
            SimulationObserver.silent[DiscretisedDistribution, Integer]()).
              getFinalState()
  
        if (level == 0) {
        _config.setInitialSimState(state._gradSimStates(0))
      }
      
//      if (state._gradSimStates == null) {
//        state._gradSimStates = prepareDistributionsForGrad(_config._daDist)
//      }
//         
//      val otherAggProdLevel = (level + 1) % 2
//      val startingDist = state._gradSimStates(otherAggProdLevel)
//
//      // Expected duration of good/bad periods in this calibration
//      // Note that the first period is the 'starting' period and does not count
//      val periods = 25;
//
//      var shockArray = getNumerics().newIntArray(periods, 2).asInstanceOf[GenericBlockedArray[Integer, _ <: GenericBlockedArray[Integer, _]]]
//      shockArray.at(-1, 0).fill(ArrayUtils.toObject(Utils.repeatArray(level, periods)): _*)
//
//      shockArray.set(otherAggProdLevel, 0, 0)
//
//      for (i <- 10 until 17) {
//        shockArray.set(otherAggProdLevel, i, 0)
//      }
//
//      state._gradSimStates(level) = getSimulator().simulateShocks(startingDist, shockArray, _model, state, SimulationObserver.silent[DiscretisedDistribution, Integer]()).getFinalState()
//      timer.stop()
//
//      // Update the initial sim density to match the one used for derivative aggregation
//      if (level == 0) {
//        _config.setInitialSimState(state._gradSimStates(0))
//      }
    }

    /**
     * Creates distributions appropriate to determine the gradient in each state, by adjusting the
     * level of unemployment expected in that state
     */
    def prepareDistributionsForGrad(simState: DiscretisedDistribution): Array[DiscretisedDistribution] = {
      val aggProdStates = _config.getAggregateProductivityShocks();

      val gradSimStates = new Array[DiscretisedDistribution](aggProdStates.numberOfElements())

      for (i <- 0 until aggProdStates.numberOfElements()) {
        gradSimStates(i) = _model.adjustDistributionToState(simState, i);
      }

      return gradSimStates
    }
  }

  class State(config: Config) extends AbstractStateWithControls[Config](config) with DerivAggCalcState[Config] with StateWithControls[Config] {
    var _growthFactor: Double = _
    var _aggError: Double = 0d
    var _indError: Double = 1d

    var _normalisedLiquidAssets: DoubleArray = _

    var _simCapitalGrid: DoubleArray = _
    var _simLiquidAssets: DoubleArray = _

    var _expectedInterest: DoubleArray = _
    var _expectedWage: DoubleArray = _

    var _policyForSimulation: DoubleArray = _
    var _bondsPolicyForSimulation: DoubleArray = _
    var _capitalPolicyForSimulation: DoubleArray = _

    var _impliedCapital: DoubleArray = _
    var _impliedBonds: DoubleArray = _

    var _capitalPolicy: DoubleArray = _
    var _bondsPolicy: DoubleArray = _

    var _bondReturns: DoubleArray = _

    // Calculate the bond prices at each agg control point using the expected interest along with the price ratios
    var _bondPrices: DoubleArray = _

    var _aggControlsPolicy: DoubleArray = _

    var _expectedBondPrices: DoubleArray = _
    var _actualBondPrices: DoubleArray = _

    var _derivativeCalcInfo: Array[DerivationInformation] = _

    // During simulation the capital should be remembered from when the transition is calulated, because later wealth
    // levels already include interest and calculating capital from them is difficult
    val _lastCalculatedCapital = new ThreadLocal[Double]

    var _gradSimStates: Array[DiscretisedDistribution] = _
    
    def getInitialDistributionForAggregation(): com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution = {
      getConfig()._daDist
    }

    override def getIndividualPolicyForSimulation(): DoubleArray = {

      // Need to update the policy for the simulaiton grid, which is used by derivative aggregation
      if (_policyForSimulation == null) {
        updateSimPolicies()
      }

      return _policyForSimulation
    }

    def bondsPolicyForSimulation = {
      if (_policyForSimulation == null) {
        updateSimPolicies()
      }

      _bondsPolicyForSimulation
    }

    def capitalPolicyForSimulation: DoubleArray = {
      if (_policyForSimulation == null) {
        updateSimPolicies()
      }

      _capitalPolicyForSimulation
    }

    def updateSimPolicies(): Unit = {

      val timer = new Timer()
      val stoppable = timer.start("interpSimPolicies")

      val constrained = params.constrained

      _policyForSimulation = interpolateFunction(_normalisedLiquidAssets, getIndividualPolicy(), 0, _simSOPStates, constrained)

      if (_capitalPolicy != null) {
        _capitalPolicyForSimulation = interpolateFunction(_normalisedLiquidAssets, _capitalPolicy, 0, _simSOPStates, constrained)
        _bondsPolicyForSimulation = interpolateFunction(_normalisedLiquidAssets, _bondsPolicy, 0, _simSOPStates, constrained)
      }
      stoppable.stop()

    }

    override def getEndOfPeriodStatesForSimulation(): DoubleArray = _simCapitalGrid

    override def setCurrentControlsPolicy(newPolicy: com.meliorbis.numerics.generic.primitives.DoubleArray[_]): Unit = {
      _aggControlsPolicy = newPolicy
    }

    override def getCurrentControlsPolicy(): DoubleArray = {
      _aggControlsPolicy
    }

    override def getIndividualControlsPolicyForSimulation(): DoubleArray = {
      bondsPolicyForSimulation
    }

    def getDistributionForState(shockLevels: Array[Int]): DiscretisedDistribution = {
      return _gradSimStates(shockLevels(0))
    }

    def setDerivatives(shockIndex: Array[Int], currentAggs: JavaDoubleArray, futureAggs: JavaDoubleArray, derivatives: JavaDoubleArray*): Unit = {

      // Store the set of arrays for the shock level, so they can be saved later
      _derivativeCalcInfo(shockIndex(0)) = (currentAggs, futureAggs, derivatives(0), null)

    }
    
    
    override def getConvergenceCriterion(): Criterion = {

      new Criterion {
        override def getValue(): Double = Math.max(_indError, _aggError)

        override def toString(): String = "Ind: %.2e Agg: %.2e".format(_indError, _aggError)
      }

    }
  }

  class Config extends ModelConfigBase {

    var _capitalShare: Double = _
    var _depreciation: Double = _


    var _growth: Double = 1
    var _discountRate: Double = 1
    var _unemploymentInsuranceRate = 0.15
    var _bondBorrowingLimit = 2.4

    var _utilityFunction: UtilityFunction = _
    var _productionFunction: ProductionFunction = _

    var _daDist: DiscretisedDistribution = _

    def getIndividualWealthLevels(): DoubleArray = {
      return getIndividualEndogenousStates().get(0);
    }

    def getAggregateCapitalLevels(): DoubleArray = {
      return getAggregateEndogenousStates().get(0);
    }

    def getAggregateProductivityShocks(): DoubleArray = {
      return getAggregateExogenousStates().get(0);
    }

    def getRiskFreeDiscount(): DoubleArray = {
      return getAggregateControls().get(0)
    }

    def getPermanentAggregateShockLevels(): DoubleArray = {
      return getAggregateNormalisingExogenousStates().get(0);
    }

    override def isConstrained(): Boolean = true

    override def readParameters(x$1: com.meliorbis.numerics.io.NumericsReader): Unit = {

    }

    override def writeParameters(x$1: com.meliorbis.numerics.io.NumericsWriter): Unit = {

    }

    def getAggregateProblemSolver(): Class[IM_DA_Solver] = if (hasAggUncertainty()) { classOf[IM_DA_Solver] } else { null }

    def getIndividualSolver(): Class[IM_EGM_Solver] = classOf[IM_EGM_Solver]
  }
}

