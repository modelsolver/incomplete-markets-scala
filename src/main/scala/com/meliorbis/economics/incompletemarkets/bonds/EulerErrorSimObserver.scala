/**
 *
 */
package com.meliorbis.economics.incompletemarkets.bonds

import com.meliorbis.economics.infrastructure.simulation.SimulationObserver
import com.meliorbis.economics.model.State
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.numerics.generic.impl.GenericBlockedArray
import com.meliorbis.economics.incompletemarkets.bonds.IMWithBonds.Model
import com.meliorbis.numerics.function.primitives.DoubleGridFunction
import com.meliorbis.numerics.io.NumericsWriterFactory
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.economics.infrastructure.Base
import java.io.File
import com.meliorbis.economics.infrastructure.simulation.SimulationResults
import java.util.Arrays
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions
import com.meliorbis.economics.infrastructure.simulation.SimState
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.IntArrayFactories._
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord

/**
 * This class can be used to observer a simulation of the incomplete markets model and calculate
 * the dynamic euler equation errors that result of a particular household subject to a pre-defined
 * sequence of idiosyncratic shocks
 * 
 * @author Tobias Grasl
 */
class EulerErrorSimObserver( writerFactory: NumericsWriterFactory ) 
    extends Base( writerFactory ) 
    with SimulationObserver[DiscretisedDistribution, Integer] {
  
  // Arrays to hold the standard model-generated values
  var _k: DoubleArray = _
  var _c: DoubleArray = _
  var _b: DoubleArray = _
  
  // Arrays to hold the alternate values demanded by dynamic euler-equation error calc
  var _kTilde: DoubleArray = _
  var _cTilde: DoubleArray = _
  var _bTilde: DoubleArray = _
  
  var _kpPolicy : DoubleGridFunction = _
  var _bpPolicy : DoubleGridFunction = _
  var _xpPolicy : DoubleGridFunction = _
  
  var _KpPolicy : DoubleGridFunction = _
  var _SpPolicy : DoubleGridFunction = _
  
  var _bondPrices: DoubleGridFunction = _
    
  var _model: Model = _
  
  var _indEmpLevels: DoubleArray = _
  var _aggProdLevels: DoubleArray = _
  
  var _kErrMax : Double = _
  var _kErrMean : Double = _
  
  var _bErrMax : Double = _
  var _bErrMean : Double = _
  
  var _cErrMax : Double = _
  var _cErrMean : Double = _
  
  var _borrowingLimit: Double = _
  
  // read the switch file from den haan
  val _individualStateSequence = getNumerics().readIntCSV(ClassLoader.getSystemResourceAsStream("ind_switch.txt"), '\t').add(-1.asInstanceOf[Integer]).asInstanceOf[GenericBlockedArray[Integer,_]]
			
  /**
   * Called with the initial state of the simulation prior to commencement, this function initialises
   * some structures the observes needs 
   */
  def beginSimulation(initialDistribution: DiscretisedDistribution, state: com.meliorbis.economics.model.State[_], model: com.meliorbis.economics.model.Model[_, _], numPeriods: Int): Unit = {
    
    _model = model.asInstanceOf[Model]
    _k = createArrayOfSize(numPeriods)
    _kTilde = createArrayOfSize(numPeriods)

    _b = createArrayOfSize(numPeriods)
    _bTilde = createArrayOfSize(numPeriods)

    // One less for c because the final period is terminal and not calculated
    _c = createArrayOfSize(numPeriods - 1)
    _cTilde = createArrayOfSize(numPeriods - 1)
    
    // Initialise both of the capital and bonds series to the same value
		_k(0) = 43
		_b(0) = -_model.getConfig()._bondBorrowingLimit
		
		_kTilde(0) = _k(0)
		_bTilde(0) = _b(0)
    
    _model = model.asInstanceOf[Model]
    
    _indEmpLevels = _model.getConfig().getIndividualExogenousStates().get(0)
    _aggProdLevels = _model.getConfig().getAggregateProductivityShocks()
    
    _borrowingLimit = _model.getConfig()._bondBorrowingLimit
    
    var calcState = state.asInstanceOf[IMWithBonds.State]
    
    _xpPolicy = _functionFactory.createFunction(Arrays.asList(
                                _model.getConfig().getIndividualEndogenousStatesForSimulation().get(0),
                                _model.getConfig().getIndividualExogenousStates().get(0),
                                _model.getConfig().getAggregateCapitalLevels(),
                                _model.getConfig().getRiskFreeDiscount(),
                                _model.getConfig().getAggregateProductivityShocks()).asInstanceOf[java.util.List[DoubleArray]],
        calcState.getIndividualPolicyForSimulation()($,$,$,$,$,0))
    
    _kpPolicy = _functionFactory.createFunction(Arrays.asList(
                                _model.getConfig().getIndividualEndogenousStatesForSimulation().get(0),
                                _model.getConfig().getIndividualExogenousStates().get(0),
                                _model.getConfig().getAggregateCapitalLevels(),
                                _model.getConfig().getRiskFreeDiscount(),
                                _model.getConfig().getAggregateProductivityShocks()).asInstanceOf[java.util.List[DoubleArray]],
        calcState.capitalPolicyForSimulation($,$,$,$,$,0))
    
    _bpPolicy = _functionFactory.createFunction(Arrays.asList(
                                _model.getConfig().getIndividualEndogenousStatesForSimulation().get(0),
                                _model.getConfig().getIndividualExogenousStates().get(0),
                                _model.getConfig().getAggregateCapitalLevels(),
                                _model.getConfig().getRiskFreeDiscount(),
                                _model.getConfig().getAggregateProductivityShocks()).asInstanceOf[java.util.List[DoubleArray]],
        calcState.bondsPolicyForSimulation($,$,$,$,$,0))
    
    _KpPolicy = _functionFactory.createFunction(Arrays.asList(
                                _model.getConfig().getAggregateCapitalLevels(),
                                _model.getConfig().getRiskFreeDiscount(),
                                _model.getConfig().getAggregateProductivityShocks()).asInstanceOf[java.util.List[DoubleArray]]
                             ,state.getAggregateTransition())
    
    _SpPolicy = _functionFactory.createFunction(Arrays.asList(
                                // Depends on both current and future shocks
                                _model.getConfig().getAggregateProductivityShocks(),
                                _model.getConfig().getAggregateProductivityShocks(),
                                _model.getConfig().getAggregateCapitalLevels()
                                ).asInstanceOf[java.util.List[DoubleArray]]
                                // Select away the unused permanent shock
                             ,state.asInstanceOf[IMWithBonds.State].getExpectedAggregateControls()($,$,0,$))
    
    _bondPrices = _functionFactory.createFunction(Arrays.asList( 
                                _model.getConfig().getRiskFreeDiscount(),
                                _model.getConfig().getAggregateProductivityShocks(),
                                _model.getConfig().getAggregateCapitalLevels()
                                ).asInstanceOf[java.util.List[DoubleArray]],
                                state.asInstanceOf[IMWithBonds.State]._bondPrices)
    
  }

  def endSimulation(finalDistribution: DiscretisedDistribution, state:com.meliorbis.economics.model.State[_]): Unit = {
    
    val abs = Math.abs _:Double => Double
    
    // Here, we can calculate the summary statistics
    val cDiffs = ((_c / _cTilde - 1) * 100).map(abs)
    
    _cErrMax = cDiffs.max()
    _cErrMean = cDiffs.sum()/cDiffs.numberOfElements()
    
    val kDiffs = ((_k - _kTilde)/(_kTilde.sum()/_kTilde.numberOfElements())).map(abs)*100
    
    _kErrMax = kDiffs.max()
    _kErrMean = kDiffs.sum()/kDiffs.numberOfElements()
    
    // Measue the bond diffs relative to the borrowing limit, not 0
    val bDiffs = ((_b - _bTilde)/(_bTilde.sum()/_bTilde.numberOfElements() + _borrowingLimit)).map(abs)*100
    
    _bErrMax = bDiffs.max()
    _bErrMean = bDiffs.sum()/bDiffs.numberOfElements()
  }

  def periodSimulated(currentDistribution: DiscretisedDistribution, transitionRecord: TransitionRecord[DiscretisedDistribution, Integer], state: com.meliorbis.economics.model.State[_], currentPeriod: Int): Unit = {
    calculateModelStandardValues(transitionRecord, state, currentPeriod);
		calculateAlternateValues(transitionRecord, state, currentPeriod);
  }

  def wroteSimulation(results: SimulationResults[DiscretisedDistribution, _ <: Integer], state: com.meliorbis.economics.model.State[_], directory: File): Unit = {
        
    val writer = getNumericsWriter(new File(directory.getParentFile(),"dynamicEuler.mat"));
    writer.writeArray("k", _k)
    writer.writeArray("c", _c)
    writer.writeArray("kTilde", _kTilde)
    writer.writeArray("cTilde", _cTilde)
    writer.writeArray("b", _b)
    writer.writeArray("bTilde", _bTilde)
    
    writer.writeArray("stats", createArray(_kErrMean, _kErrMax, _cErrMean, _cErrMax, _bErrMax, _bErrMean))
   
    writer.close()
  }

  /**
   * Calculates for the given period the values for the individual variables generated by the model
   */
  def calculateModelStandardValues(transitionRecord: TransitionRecord[DiscretisedDistribution, Integer], state: com.meliorbis.economics.model.State[_], currentPeriod: Int) = {
  
      // Get the individual's capital at the beginning of the period
			val k = _k(currentPeriod)
			val b = _b(currentPeriod)
			
			// Gets the individual's exogenous current period employment state
		  val individualState = _individualStateSequence.get(currentPeriod,0);
			
			// Aggregate capital and productivity for the simulated period
			val K = transitionRecord.getStates()(0)
			val s = transitionRecord.getControls()(0)
			val A = transitionRecord.getShocks().asInstanceOf[IntegerArray].get(0)

			// Calculate the consumption from the budget constraint
			val w = _model.wage(A, K) * _model._wageRatios(individualState, A)
			val R = _model.grossInterest(A, K)
			
			// Calculate the individual future capital at the given state, which is the 'normal' time series
			val kp = calckp(k*R + b + _borrowingLimit, individualState, K, s, A)
			val bp = calcbp(k*R + b + _borrowingLimit, individualState, K, s, A)
			
			// Store the calculated capital for the beginning of the next period
			_k(currentPeriod+1) = kp
			_b(currentPeriod+1) = bp

			var p = _bondPrices.call(s, _aggProdLevels(A), K)(0)
			
			// Calculate implied consumption, and save it
			val c = R*k + b + w - kp - bp*p
			
			_c(currentPeriod) = c			
  }

  def calcbp(k: Double, employmentStatus: Int, K: Double, s: Double,  A: Int) : Double = {
    _bpPolicy.call(k, _indEmpLevels(employmentStatus), K, s, _aggProdLevels(A))(0)
  }
  
  def calckp(k: Double, employmentStatus: Int, K: Double, s: Double, A: Int) : Double = {
    _kpPolicy.call(k, _indEmpLevels(employmentStatus), K, s,_aggProdLevels(A))(0)
  }
  
  def calcxp(k: Double, employmentStatus: Int, K: Double, s: Double, A: Int) : Double = {
    _xpPolicy.call(k, _indEmpLevels(employmentStatus), K, s,_aggProdLevels(A))(0)
  }
  
  def calcKp(K: Double, s: Double, A: Int) : Double = {
    _KpPolicy.call(K, s, _aggProdLevels(A))(0)
  }
  
  /**
   * Calculates for the given period the values for the individual variables as defined in the dynamic euler-error specificat_ion
   */
  def calculateAlternateValues(transitionRecord: TransitionRecord[DiscretisedDistribution, Integer], state: com.meliorbis.economics.model.State[_], currentPeriod: Int) = {
      
      val k = _kTilde.get(currentPeriod)
      val b = _bTilde(currentPeriod)
      
			val individualState = _individualStateSequence.get(currentPeriod,0)
			
			val K = transitionRecord.getStates()(0)
			val s = transitionRecord.getControls()(0)
			val A = transitionRecord.getShocks().asInstanceOf[IntegerArray].get(0)
			
			// From that the implied future capital
      val w = _model.wage(A, K) * _model._wageRatios(individualState, A)
			val R = _model.grossInterest(A, K)
			
			val wealth = k*R + b + _borrowingLimit
			
			// Calculate the individual future capital implied by the model given kTilde
			val kpHat = calckp(wealth, individualState, K, s, A)
			val bpHat = calcbp(wealth, individualState, K, s, A)
			
			val xpHat = calcxp(wealth, individualState, K, s, A)
			val p = _bondPrices.call(s,_aggProdLevels(A),K)(0)
			val diff = xpHat - kpHat - (bpHat+_borrowingLimit)/p
			
			//Now look ahead from the assumed state, and get also the implied aggregate future capital
			val Kp = calcKp(K, s, A)
			
			// Future expected bond price is conditional on the future shock also!
			val sp = _SpPolicy.restrict(_aggProdLevels(A), /* unknown future shock*/Double.NaN, K)
			
			// Create arrays to hold expected wages and gross interest. Note the former depends on both individual and aggregate state
			val wp = createArrayOfSize(2,2)
			val Rp = createArrayOfSize(2,2)
			
			// Calculate expected future wages and interest, conditional on futer productivity level
			for(i <- 0 until 2) {
			  wp($,i) += _model.wage(i,Kp)
			  Rp($,i) += _model.grossInterest(i,Kp)
			}

      // Adjust the wage to the individuals employment status
      wp *= _model._wageRatios
      
      // Find expected future transition conditional on the future state
      val expectedMucForState = createArrayOfSize(2)
      
      for(i <- 0 until 2) {
        val kppForState = _kpPolicy.restrict(kpHat*Rp(0,i) + bpHat + _borrowingLimit,Double.NaN,Kp,sp.getValues()(i),_aggProdLevels(i)).getValues()
        val bppForState = _bpPolicy.restrict(kpHat*Rp(0,i) + bpHat + _borrowingLimit,Double.NaN,Kp,sp.getValues()(i),_aggProdLevels(i)).getValues()
        
        val xppForState = _xpPolicy.restrict(kpHat*Rp(0,i) + bpHat + _borrowingLimit,Double.NaN,Kp,sp.getValues()(i),_aggProdLevels(i)).getValues()
        
        val pp = _bondPrices.call(sp.getValues()(i), _aggProdLevels(i),Kp )(0)
        val cpForState = Rp(0,i)*kpHat + bpHat + wp($,i) - kppForState - bppForState*pp
        
        val mucForState = cpForState.map((cp: Double) => _model.getConfig()._utilityFunction.marginalUtility(0, cp)) * _model.getConfig()._discountRate * Rp(0,i)
        
        // Get the transition probabilities conditional on the current states
        var probs = _model.getConfig().getExogenousStateTransition()(individualState.asInstanceOf[Int].asInstanceOf[Double],A.asInstanceOf[Int].asInstanceOf[Double],$,i,0)
        
        expectedMucForState(i) = (mucForState * probs).sum()
      }
      
      // Unconditional expected marginal utility
      val expectedMUC = expectedMucForState.sum()
      
      // Implied current consumption
      var cTilde = _model.getConfig()._utilityFunction.inverseMarginalUtility(expectedMUC)
			
      val xpTilde = wealth + w - cTilde
      
      var kpTilde = 0d
      var bpTilde = 0d
      
      // If the borrowing constraint is violated - NOTE THAT THE BORROWNG LMT S THE FACE VALUE OF BONDS 
      if(xpTilde < -_model.getConfig()._bondBorrowingLimit*p) {
        // future capital is actually 0, and all liquid assets are consumed
				cTilde =  R*k +w
				
				kpTilde = 0
				bpTilde = -_model.getConfig()._bondBorrowingLimit
			}
      else {
        // Get the policy for total wealth carried over for the current state
        val xpForState = _xpPolicy.restrict(Double.NaN, _indEmpLevels(individualState), K, s,_aggProdLevels(A)).getValues()
        
        // Find the implied current wealth of an agent given aggregates and exogenous vars who would carry over xpTilde in wealth
        val impliedCurrentWealth = _functionFactory.createFunction(
                          Arrays.asList(xpForState($,0)).asInstanceOf[java.util.List[DoubleArray]],
                          _model.getConfig().getIndividualEndogenousStatesForSimulation().get(0)).call(xpTilde).get(0)
                          
        val xpRecovered = calcxp(impliedCurrentWealth, individualState, K, s, A)
        // Using that level, determine the current implied capital and bonds
        kpTilde = calckp(impliedCurrentWealth, individualState, K, s, A)
        bpTilde = calcbp(impliedCurrentWealth, individualState, K, s, A)
      }
      
      _kTilde(currentPeriod + 1) = kpTilde
      _bTilde(currentPeriod + 1) = bpTilde
      _cTilde(currentPeriod) = cTilde
   }
}