

package com.meliorbis.economics.incompletemarkets

import com.meliorbis.economics.incompletemarkets.bonds.IMWithBonds

/**
 * Dispatches execution to the appropriate model based on arguments. 
 * 
 * ''This class exists so that an executable jar can be created for the whole project; as source, 
 * it is uninteresting.''
 * 
 * @author Tobias Grasl
 */
object Dispatcher {
  
  def main(args: Array[String]) {
    
    if(args.length == 0) {
      error()
    }
    
    // The model
    val modelName = args(0)
    
    // Arguments for the model
    var remainingArgs = args.tail
    
    var mainMethod = modelName match {
      case "-xpa" => IncompleteMarketsModelXPA.main _
      case "-bonds" => IMWithBonds.main _
      case "-da" => IncompleteMarketsModel.main _
      case default => error()
    }

    // Strip the first argument which was already used
    mainMethod(args.tail)
  }
  
  def error() : (Array[String] => Unit)  = {
    println("""The first argument must be one of:
        | -xpa to run the KS model with Explicit Aggregation
        | -da to run the KS model with Derivative Aggregation
        | -bonds to run the model with bonds and Derivative Aggregation""".stripMargin)
        
        System.exit(-1)
        
        // Prevent complaints about return val
        throw new RuntimeException()
  }
  
}