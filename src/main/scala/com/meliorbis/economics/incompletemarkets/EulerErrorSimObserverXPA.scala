/**
 *
 */
package com.meliorbis.economics.incompletemarkets

import com.meliorbis.economics.infrastructure.simulation.SimulationObserver
import com.meliorbis.economics.model.State
import com.meliorbis.economics.model.Model
import com.meliorbis.numerics.io.NumericsReader
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.numerics.generic.impl.GenericBlockedArray
import com.meliorbis.numerics.io.NumericsWriterFactory
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.economics.infrastructure.Base
import java.io.File
import com.meliorbis.economics.infrastructure.simulation.SimulationResults
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord;

import java.util.Arrays
import com.meliorbis.economics.infrastructure.simulation.SimState
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.numerics.function.primitives.DoubleGridFunction
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.IntArrayFactories._

/**
 * This class can be used to observer a simulation of the incomplete markets model and calculate
 * the dynamic euler equation errors that result of a particular household subject to a pre-defined
 * sequence of idiosyncratic shocks
 * 
 * @author Tobias Grasl
 */
class EulerErrorSimObserverXPA( writerFactory: NumericsWriterFactory ) 
    extends Base( writerFactory ) 
    with SimulationObserver[DiscretisedDistribution, Integer] {
  
  // Arrays to hold the standard model-generated values
  var _k: DoubleArray = _
  var _c: DoubleArray = _
  
  // Arrays to hold the alternate values demanded by dynamic euler-equation error calc
  var _kTilde: DoubleArray = _
  var _cTilde: DoubleArray = _
  
  var _1PForecasts: DoubleArray = _
  
  var _kpPolicy : DoubleGridFunction = _
  var _KpPolicy : DoubleGridFunction = _
  
  var _model: com.meliorbis.economics.incompletemarkets.IncompleteMarketsModelXPA.Model = _
  
  var _indEmpLevels: DoubleArray = _
  var _aggProdLevels: DoubleArray = _
  
  var _kErrMax : Double = _
  var _kErrMean : Double = _
  var _cErrMax : Double = _
  var _cErrMean : Double = _
  
  
  // read the switch file from den haan
  val _individualStateSequence = getNumerics().readIntCSV(ClassLoader.getSystemResourceAsStream("ind_switch.txt"), '\t').add(-1.asInstanceOf[Integer]).asInstanceOf[GenericBlockedArray[Integer,_]]
			
  /**
   * Called with the initial state of the simulation prior to commencement, this function initialises
   * some structures the observes needs 
   */
  def beginSimulation(initialDistribution: DiscretisedDistribution, state: State[_], model: Model[_, _], numPeriods: Int): Unit = {
    _k = createArrayOfSize(numPeriods)
    _kTilde = createArrayOfSize(numPeriods)

    // One less for c because the final period is terminal and not calculated
    _c = createArrayOfSize(numPeriods - 1)
    _cTilde = createArrayOfSize(numPeriods - 1)
    
    _1PForecasts = createArrayOfSize(numPeriods,2)
    
    _1PForecasts(0,$) << model.asInstanceOf[IncompleteMarketsModelXPA.Model].calculateAggregateStates(initialDistribution, createIntArray(0), 
        state.asInstanceOf[IncompleteMarketsModelXPA.State])
        
    // Initialise both of the capital series to the same value
		_k(0) = 43
		_kTilde(0) = 43
    
    _model = model.asInstanceOf[com.meliorbis.economics.incompletemarkets.IncompleteMarketsModelXPA.Model]
    
    _indEmpLevels = _model.getConfig().getIndividualExogenousStates().get(0)
    _aggProdLevels = _model.getConfig().getAggregateProductivityShocks()
    
    val kpDomain : java.util.List[DoubleArray] = Arrays.asList(
                                _model.getConfig().getIndividualEndogenousStatesForSimulation().get(0),
                                _model.getConfig().getIndividualExogenousStates().get(0),
                                _model.getConfig().getAggregateUnempCapitalLevels(),
                                _model.getConfig().getAggregateEmpCapitalLevels(),
                                _model.getConfig().getAggregateProductivityShocks())
                                
    _kpPolicy = _functionFactory.createFunction(kpDomain, state.getIndividualPolicyForSimulation())
        
     val KpDomain : java.util.List[DoubleArray] = Arrays.asList(
                                _model.getConfig().getAggregateUnempCapitalLevels(),
                                _model.getConfig().getAggregateEmpCapitalLevels(),
                                _model.getConfig().getAggregateProductivityShocks())
                                
    _KpPolicy = _functionFactory.createFunction(KpDomain, state.getAggregateTransition())
    
    
  }

  def endSimulation(finalDistribution: DiscretisedDistribution, state: State[_]): Unit = {
    
    val abs = Math.abs _:Double => Double
    
    // Here, we can calculate the summary statistics
    val cDiffs = ((_c / _cTilde - 1) * 100).map(abs)
    
    _cErrMax = cDiffs.max()
    _cErrMean = cDiffs.sum()/cDiffs.numberOfElements()
    
    val kDiffs = ((_k - _kTilde)/(_kTilde.sum()/_kTilde.numberOfElements())).map(abs)*100
    
    _kErrMax = kDiffs.max()
    _kErrMean = kDiffs.sum()/kDiffs.numberOfElements()
  }

  override def periodSimulated(currentDistribution: DiscretisedDistribution, transitionRecord: TransitionRecord[DiscretisedDistribution, Integer], state: State[_], currentPeriod: Int): Unit = {
    calculateModelStandardValues(transitionRecord, state, currentPeriod);
		calculateAlternateValues(transitionRecord, state, currentPeriod);
  }

  def wroteSimulation(results: SimulationResults[DiscretisedDistribution, _ <: Integer], state: State[_], directory: File): Unit = {
        
    val writer = getNumericsWriter(new File(directory.getParentFile(),"dynamicEuler.mat"));
    writer.writeArray("k", _k)
    writer.writeArray("c", _c)
    writer.writeArray("kTilde", _kTilde)
    writer.writeArray("cTilde", _cTilde)
    writer.writeArray("forecasts1P", _1PForecasts)
    
    writer.writeArray("stats", createArray(_kErrMean, _kErrMax, _cErrMean, _cErrMax))
   
    writer.close()
  }

  /**
   * Calculates for the given period the values for the individual variables generated by the model
   */
  def calculateModelStandardValues(transitionRecord: TransitionRecord[DiscretisedDistribution, Integer], state: State[_], currentPeriod: Int) = {
  
      // Get the individual's capital at the beginning of the period
			val k = _k(currentPeriod)
			
			// Gets the individual's exogenous current period employment state
		  val individualState = _individualStateSequence.get(currentPeriod,0);
			
			// Aggregate capital and productivity for the simulated period
			val Ku = transitionRecord.getStates()(0)
			val Ke = transitionRecord.getStates()(1)
      val A = transitionRecord.getShocks().asInstanceOf[IntegerArray].get(0)
			
			// Calculate the individual future capital at the given state, which is the 'normal' time series
			val kp = calckp(k, individualState, Ku, Ke, A)
			
			// Store the calculated capital for the beginning of the next period
			_k(currentPeriod+1) = kp
			
			
			// Calculate the consumption from the budget constraint
      val K = _model.capital(Ku, Ke, A)
      
			val w = _model.wage(A, K)
			val R = _model.grossInterest(A, K)
			
			// Calculate implied consumption, and save it
			val c = R*k + w * _model._wageRatios(individualState, A) - kp;
			
			_c(currentPeriod) = c			
  }

  def calckp(k: Double, employmentStatus: Int, Ku: Double, Ke: Double, A: Int) : Double = {
    _kpPolicy.call(k, _indEmpLevels(employmentStatus), Ku, Ke, _aggProdLevels(A))(0)
  }
  
  def calcKp(Ku: Double, Ke: Double, A: Int) : DoubleArray = {
    _KpPolicy.call(Ku, Ke, _aggProdLevels(A))
  }
  
  /**
   * Calculates for the given period the values for the individual variables as defined in the dynamic euler-error specificat_ion
   */
  def calculateAlternateValues(transitionRecord: TransitionRecord[DiscretisedDistribution, Integer], state: State[_], currentPeriod: Int) = {
      
      val k = _kTilde.get(currentPeriod)
			val individualState = _individualStateSequence.get(currentPeriod,0)
			
			val Ku = transitionRecord.getStates()(0)
      val Ke = transitionRecord.getStates()(1)
      
			val A = transitionRecord.getShocks().asInstanceOf[IntegerArray].get(0)
			
      val K = _model.capital(Ku, Ke, A)
      
			// Calculate the individual future capital implied by the model given kTilde
			val kpHat = calckp(k, individualState, Ku, Ke, A)
			
			//Now look ahead from the assumed state, and get also the implied aggregate future capital
			val futKs = calcKp(Ku, Ke, A)
      
      // NOTE THAT THESE ARE NOT YET CORRECT Kp VALUES SINCE THOSE DEPEND ON FUTURE SHOCKS TOO!
			val Kp = (futKs * _model._employDist($,A.asInstanceOf[Int].asInstanceOf[Double])).sum()
        
			// Create arrays to hold expected wages and gross interest. Note the former depends on both individual and aggregate state
			
      val mucByStates = createArrayOfSize(2,2)
      
      val actualFutureShock = transitionRecord.getFutureShocks().asInstanceOf[IntegerArray].get(0)
      
			// Calculate expected future wages and interest, conditional on future productivity level
			for(i <- 0 until 2) {
        
        val futKsForShock = futKs.copy()
        _model.adjustTransitionedAggs(futKsForShock, Array(A), Array(i),state.asInstanceOf[IncompleteMarketsModelXPA.State])
        
        // Store the 1-period ahead forecast
        if(i == actualFutureShock) {
          _1PForecasts(currentPeriod+1,$) << futKsForShock
        }
        
        var Array(kpu, kpe) = futKsForShock.toArray()
        
        val wp = _model.wage(i,Kp)
			  val Rp = _model.grossInterest(i,Kp)
        
        val kppByStates = _kpPolicy.restrict(kpHat, Double.NaN, kpu, kpe, _aggProdLevels(i)).getValues()
        val cpByStates = Rp*kpHat + wp*_model._wageRatios($,i) - kppByStates
       
        // Calculate the discounted conditional expected future marginal utilities
        mucByStates($,i) << cpByStates->(_model.getConfig()._utilityFunction.marginalUtility(0, _:Double)* _model.getConfig()._discountRate * Rp) 
      }
      
      
      // Get the transition probabilities conditional on the current states
      var probs = _model.getConfig().getExogenousStateTransition()(individualState.asInstanceOf[Int].asInstanceOf[Double],A.asInstanceOf[Int].asInstanceOf[Double],$,$,0)
      
      // Normalise to sum to 1
      probs /= probs.sum()
      
      // Unconditional expected marginal utility
      val expectedMUC = (mucByStates * probs).sum()
      
      // Implied current consumption
      var cTilde = _model.getConfig()._utilityFunction.inverseMarginalUtility(expectedMUC);
      
      
      
      // From that the implied future capital
      val w = _model.wage(A, K) * _model._wageRatios(individualState, A)
			val R = _model.grossInterest(A, K)
			
      var kpTilde = R * k + w - cTilde
      
      // If the borrowing constraint is violated
      if(kpTilde < 0)
			{
        // future capital is actually 0, and all liquid assets are consumed
				kpTilde = 0
				cTilde =  R*k +w;
			}
      
      _kTilde(currentPeriod + 1) = kpTilde
      _cTilde(currentPeriod) = cTilde
   }
}