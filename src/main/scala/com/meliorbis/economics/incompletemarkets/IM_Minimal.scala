package com.meliorbis.economics.incompletemarkets

import java.io.File

import scala.collection.JavaConversions.asScalaBuffer

import org.apache.commons.cli.CommandLine

import com.meliorbis.economics.aggregate.derivagg.{ DerivAggCalcState, DerivativeAggregationSolver }
import com.meliorbis.economics.core.{ CRRAUtility, CobbDouglasProduction, ProductionFunction, UtilityFunction }
import com.meliorbis.economics.individual.egm.EGMIndividualProblemSolver
import com.meliorbis.economics.infrastructure.{ AbstractModel, AbstractStateBase, ModelConfigBase }
import com.meliorbis.economics.infrastructure.simulation.{ DiscretisedDistribution, SimState, SimulationObserver, SimulationResults }
import com.meliorbis.economics.model.ModelRunner
import com.meliorbis.economics.scala.ModelHelpers.arrayChangedConv
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.IntArrayFactories._
import com.meliorbis.numerics.Numerics
import com.meliorbis.numerics.convergence.Criterion
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.numerics.generic.primitives.impl.Interpolation._
import com.meliorbis.numerics.io.NumericsReader
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.utils.Pair
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulatorImpl
import com.meliorbis.numerics.io.NumericsWriter
import com.meliorbis.economics.aggregate.derivagg.DefaultDASolver
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulator

/**
 * An implementation of the standard incomplete markets model a 
 * ''la Krusell & Smith (98)'' using the method of endogenous gridpoints and 
 * derivative aggregation
 *
 * @author Tobias Grasl
 */
object IM_Minimal {
  
  def main(args: Array[String]) {
    println(s"Running ${this.getClass.getName}")

    Runner.run(args)
  }
  
  object Runner extends ModelRunner[Model, Config, State] {
    
    /**
     * Override the default simulation mechanism to read pre-created shock 
     * sequence (from Den Haan et al 2010)
     */
    override def simulateModel(
        model: Model, 
        state: State, 
        periods: Int, 
        burnIn: Int, 
        stateDir: File, 
        simPath: String): SimulationResults[_, _] = {

      // Read the data, and simulate the relevant shocks
        var zStream = getClass.getResourceAsStream("/Z_Formatted.txt")

        var shocksFromCSV = Numerics.instance().
              readFormattedCSV[Integer](zStream).asInstanceOf[IntegerArray]

        // Adjust the shocks because the numbering scheme differs from that 
        // in the data
        var allShocks = createIntArrayOfSize(shocksFromCSV.size()(0), 2)
        allShocks.at(-1, 0).fill(shocksFromCSV.add(Integer.valueOf(-1)))

        val initialDist = state.daDistributionByState(0)
        
         // Get an appropriate simulator for this model
        val simulator = getSimulator(initialDist,0.asInstanceOf[Integer])
        
        return simulator.simulateShocks(initialDist, allShocks, model,
          state, SimulationObserver.silent(), stateDir, simPath)
    }    

    override def createConfig(commandLine: CommandLine): Config = {
     
      val config = new Config()

      // Initial Shock Levels (transient and permanent)
      config.setInitialExogenousStates(createIntArray(0,0))

      // Individual shocks (values actually not used)
      config.setIndividualExogenousStates(
                createArray(0.15, (1 - 0.15 * 0.05) / 0.95))

      // For solving, use log-distributed points of individual wealth
      config.setIndividualEndogenousStates(
                nLogSequence(200, 250, 1))
      
      // For Simulation, make the grid more dense and use even distribution
      config.setIndividualEndogenousStatesForSimulation(
                nLogSequence(200, 2001, 0))

      // Create a capital grid with 15 points spaced around the mean 39.85, but with the points closer to the mean a little 
      config.setAggregateEndogenousStates(pullToMean(
                createArray(0.9 * 39.85 to (1.1 * 39.85, 15): _*), 0.2))
            
      // Good and bad productivity levels for agg shock
      config.setAggregateExogenousStates(createArray(.99d, 1.01d))

      // No normalising (permanent) shock
      config.setAggregateNormalisingExogenousStates(createArray(1d))
      
      // Markov Transition from denHaan comparison paper
      config.setExogenousStateTransiton( createArrayOfSize(2, 2, 2, 2, 1) << (
                21d / 40d, 1d / 32d, 7d / 20d, 3d / 32d,
                3d / 32d, 7d / 24d, 1d / 32d, 7d / 12d,
                7d / 180d, 1d / 480d, 301d / 360d, 59d / 480d,
                7d / 768d, 7d / 288d, 89d / 768d, 245d / 288d))


      // Indicate where the initial state should be read from
      setInitialStatePath("Solutions/KS_NA/state.mat")

      // The distribution to be used for derivative aggregation
      config.noAggRiskSteadyState = new DiscretisedDistribution(
                new File("Solutions/KS_NA/ergodicDist.mat"))
      
      config
    }

    override def getModelClass = classOf[Model]
  }
  
  /**
   * The Config class holds the configuration - some model specific fields and
   * some generic ones, setup in the Runner
   */
  class Config extends ModelConfigBase {

    // Model-specific params
    var growthRate: Double = 1
    var discountRate: Double = 0.99
    var unemploymentInsuranceRate = 0.15
    var utilityFunction: UtilityFunction = new CRRAUtility(1.0)
    var productionFunction: ProductionFunction = 
      new CobbDouglasProduction(.36, 1d-.36, .025)
    var noAggRiskSteadyState: DiscretisedDistribution = _

    // Expose defined variables, which have standard accessors, with meaningful names
    def individualCapitalLevels = getIndividualEndogenousStates()(0)
    def individualCapitalLevelsForSimulation = getIndividualEndogenousStatesForSimulation()(0)
    def individualShockLevels = getIndividualExogenousStates()(0)
    def aggregateCapitalLevels = getAggregateEndogenousStates()(0)
    def aggregateProductivityShocks = getAggregateExogenousStates()(0)
    def permanentAggregateShockLevels = getAggregateNormalisingExogenousStates()(0)
    
    // Wealth is constrained at 0
    override def isConstrained = true
    
    // Defined the solvers to solve individual and aggregate probelms
    override def getIndividualSolver = classOf[IM_EGM_Solver]
    override def getAggregateProblemSolver = classOf[IM_DA_Solver]
  }
  
  /**
   * The State holds the state of the ongoing calculation is updated by 
   * Solver classes as well as model-specific code
   */
  class State(config: Config) extends AbstractStateBase[Config](config) 
                                  with DerivAggCalcState[Config] 
  {
    var normalisedLiquidAssets: DoubleArray = _

    var simCapitalGrid: DoubleArray = _
    var simLiquidAssets: DoubleArray = _

    var expectedR: DoubleArray = _
    var expectedW: DoubleArray = _

    var daDistributionByState: Array[DiscretisedDistribution] = _
    
    override def getIndividualPolicyForSimulation() = {

      if (_individualTransitionForSimulation == null) {
        _individualTransitionForSimulation = 
          interpolateFunction(normalisedLiquidAssets, 
                              getIndividualPolicy(), 
                              0, 
                              simLiquidAssets, 
                              params.constrained)
      }

      // Need to update the policy for the simulation grid, which is used by 
      // derivative aggregation
      _individualTransitionForSimulation
    }

    override def getEndOfPeriodStatesForSimulation() = simCapitalGrid

    // There is only one shock, and use that one to get the
    def getDistributionForState(shockLevels: Array[Int]) = 
                                    daDistributionByState(shockLevels(0))
  }

  class Model extends AbstractModel[Config, State]() {

    var employmentDistByProductivity: DoubleArray = _
    var aggLabourByProductivity: DoubleArray = _
    var wageRatiosbyProductivity: DoubleArray = _

    // These arrays are for convenience - they are constant and are used to 
    // calculate wages and returns
    var expectedL: DoubleArray = _
    var expectedA: DoubleArray = _

    var interestGrid: DoubleArray = _

    var normalisedLiquidAssets: DoubleArray = _
    var simNormalisedLiquidAssets: DoubleArray = _
    
    override def initialise(): Unit = {

      super.initialise()

      // Calculate the proportion of employed/unemployed in each aggregate 
      // state, and also the resulting
      // aggregate labour supply
      initEmploymentDist

      // Determine the relative wages earned by employed/unemployed in each 
      // aggregate exogenous state
      initWageRatios

      // These arrays are for convenience - they are constant and are used to 
      // calculate wages and returns
      expectedL = createAggregateExpectationGrid(1)
      expectedL\1 << aggLabourByProductivity

      expectedA = createAggregateExpectationGrid(1)
      expectedA\1 << _config.aggregateProductivityShocks

      addAggregateExpectationListener(afterAggregateExpectationUpdate _)
      
      /* Prepare the grids of aggregate prices, which vary along the aggregate 
       * capital and productivity levels
       */
      interestGrid = createAggregateVariableGrid()
      val wageMatrix = createAggregateVariableGrid()

      for (K_idx <- 0 until _config.aggregateCapitalLevels.numberOfElements) {
        for (P_idx <- 0 until 
            _config.aggregateProductivityShocks.numberOfElements) {

          // Get the capital and Labour supply at the current point
          val K = _config.aggregateCapitalLevels(K_idx)
          val L = aggLabourByProductivity(P_idx)

          interestGrid(K_idx, P_idx,0) = grossInterest(P_idx, K, L)
          wageMatrix(K_idx, P_idx,0) = wage(P_idx, K, L)
        }
      }

      // Individual wages also vary by individual employment state (i.e. shock)
      val individualWages = createArrayOfSize(
          _config.individualShockLevels.numberOfElements +: wageMatrix.size:_*)

      // Fill with aggregate wage matrix
      individualWages\(1, 2) << wageMatrix

      // Multiply by the individual wage factor
      individualWages\(0, 2) *= wageRatiosbyProductivity
      
      // The liquid assets at the beginning of the period are prior capital 
      // with interest + wages
      normalisedLiquidAssets = createIndividualTransitionGrid()
      normalisedLiquidAssets\(0) << _config.individualCapitalLevels

      // Normalise the capital carried over by the permanent shock
      normalisedLiquidAssets\(normalisedLiquidAssets.numberOfDimensions - 2) /=
        _config.permanentAggregateShockLevels

      // Multiply the capital by the rate of return
      normalisedLiquidAssets\(2, 3) *= interestGrid

      // Add the wage
      normalisedLiquidAssets\(1, 2, 3) += individualWages

      val laSize = normalisedLiquidAssets.size().clone()
      laSize(0) = _config.individualCapitalLevelsForSimulation.numberOfElements

      // We need to do the same for the simulation grid, which has a different
      // size
      simNormalisedLiquidAssets = createArrayOfSize(laSize:_*)
      simNormalisedLiquidAssets\0 << 
        _config.getIndividualEndogenousStatesForSimulation().get(0)
      simNormalisedLiquidAssets\(
          simNormalisedLiquidAssets.numberOfDimensions - 2) /= 
            _config.permanentAggregateShockLevels
      simNormalisedLiquidAssets\(2, 3) *= interestGrid
      simNormalisedLiquidAssets\(1, 2, 3) += individualWages
    }

    // Update aggregates in each iteration
    override def shouldUpdateAggregates(state: State) = true

    // Just the mean of the capital distribution
    override def calculateAggregateStates(
        distribution: SimState, 
        aggregateExogenousStates: 
        IntegerArray, 
        state: State) = Array(distribution.mean(
            _config.getIndividualEndogenousStatesForSimulation().get(0)))
    
    // Store the final distribution used for DA to start the sim with later
    override def writeAdditional(state: State, writer: NumericsWriter) {
        state.daDistributionByState(0).write(writer, "finalGradDensity")
    }
    
    /**
     * Called by the Toolkit when reading state, this is overridden to convert
     * non-agg-risk (NA) state into appropriate initial state with agg risk
     */
    override def readAdditional(state: State, reader: NumericsReader): Unit = {

      // If the policy that has been read has only one shock level it is an NA 
      // state - need to expand to fit with-agg-risk grid
      if (state.getIndividualPolicy().size()(3) == 1) {

        val indPolicy = createIndividualTransitionGrid()
        indPolicy\(0,1,2) << state.getIndividualPolicy()($, $, $, 0, 0, 0)

        state.setIndividualPolicy(indPolicy)

        // Also need to initialise the aggregate transition, because the one 
        // read is the NA (constant) transition
        initAggregateTransition(state)
      }

      // Calculate expectations from the aggregate transition
      adjustExpectedAggregates(state)
    }
    
    /**
     * Creates distributions appropriate to determine the gradient in each 
     * state, by adjusting the level of unemployment expected in that state
     */
    def prepareDistributionsForGrad(simState: DiscretisedDistribution) = {
      val gradSimStates = new Array[DiscretisedDistribution](
          _config.aggregateProductivityShocks.numberOfElements())

      for (i <- 0 until _config.aggregateProductivityShocks.numberOfElements) {
        gradSimStates(i) = adjustDistributionToState(simState, i)
      }
      
      // Start sim from the same distribution used for derivative aggregation
      _config.setInitialSimState(gradSimStates(0))

      gradSimStates
    }

    // Sum the number of unemployed and employed separately
    def determineEmploymentDistribution(distribution: DiscretisedDistribution) = 
       distribution._overflowProportions + (distribution._density\(0) sum )

    /**
     * Given a distribution, adjusts is to the unemployment level expected in a
     * given aggregate state 
     * 
     * ''Note'': The starting distribution is one from the no-agg-risk model 
     * and that has U between 4% and 10%)
     */
    def adjustDistributionToState(
        distribution: DiscretisedDistribution, 
        state: Int) = {
      val simState = distribution.clone()

      val targetUnemp = employmentDistByProductivity(0, state)
        
      // Get the unemployment level in the provided distribution
      val empProps = determineEmploymentDistribution(simState)
      
      // Adjust the distribution to the appropriate unemployment level
      if (empProps(0) - targetUnemp > 1e-10) {
        // Scale down unemployed if there are too many
        val unempFactor = targetUnemp / empProps(0)
        
        simState._density($, 0) *= unempFactor
        simState._density($, 1) += simState._density($, 0) * (1d - unempFactor)
        
        simState._overflowProportions($, 0) *= unempFactor
        simState._overflowProportions($, 1) += 
          simState._overflowProportions($, 0) * (1d - unempFactor)
        
      } else {
        // Scale down employed if there are too many
        val empFactor = (1d - targetUnemp) / empProps(1)
        simState._density($, 1) *= empFactor
        simState._density($, 0) += simState._density($, 1) * (1d - empFactor)

        simState._overflowProportions($, 1) *= empFactor
        simState._overflowProportions($, 0) += 
          simState._overflowProportions($, 1) * (1d - empFactor)
      }

      simState
    }

    override def initialState(): State = {

      // Create a state object for this configuration 
      val state = new State(_config)

      // State class needs these for interpolation
      state.normalisedLiquidAssets = normalisedLiquidAssets
      state.simLiquidAssets = simNormalisedLiquidAssets
      
      // The end of period states are just the capital levels to be carried over
      val simCapitalGrid = createSimulationGrid()

      simCapitalGrid.across(0) << 
        (_config.getIndividualEndogenousStatesForSimulation().get(0))

      state.simCapitalGrid = simCapitalGrid
            
      state.daDistributionByState = prepareDistributionsForGrad(
          _config.noAggRiskSteadyState)

      state
    }

    /**
     * Initialises the aggregate transition (a.k.a. forecasting function)
     */
    def initAggregateTransition(state: State) {
      val aggTrans = createAggregateVariableGrid()

      // Squeeze the values inward a bit from the full grid
      aggTrans.across(0) << squeezeToMean(_config.aggregateCapitalLevels, 0.1)

      // Then adjust it for the expected shock
      aggTrans\(1) *= _config.aggregateProductivityShocks 
      
      // Finally, make sure none of the capital levels expected is outside the 
      // grid bounds
      aggTrans ->=  ( _.max(_config.aggregateCapitalLevels.first).min(
          _config.aggregateCapitalLevels.last) )
    
      state.setAggregateTransition(aggTrans)
    }

    /**
     * Adjusts the expected capital level for permanent shocks (in the next 
     * period) and also calculates expected prices given the expected factor 
     * inputs
     */
    def afterAggregateExpectationUpdate(
        oldVal: DoubleArray, 
        newVal: DoubleArray, 
        state: State) {

      // Adjust for future permanent shocks
      state.getExpectedAggregateStates\2 /= 
        _config.permanentAggregateShockLevels

      // Update expected prices
      val Array(w, r) = 
        (state.getExpectedAggregateStates() :: expectedL :: expectedA) :-> (
            in => {
          val Array(k,l,p) = in

          Array(wage(p, k, l), grossInterest(p,k,l))
        })

      state.expectedR = r
      state.expectedW = w
    }

    // Methods to calculate interest given different parameters
    def grossInterest(productivity: Double, K: Double, L: Double): Double = 
      1 + _config.productionFunction.netReturn(productivity, K, L)
    
      def grossInterest(pIdx: Int, K: Double, L: Double): Double = 
      grossInterest(_config.aggregateProductivityShocks(pIdx), K, L)
      
    def grossInterest(pIdx: Int, K: Double): Double = 
      grossInterest(_config.aggregateProductivityShocks(pIdx), K, 
          aggLabourByProductivity(pIdx))
    
    // Ditto for wages
    def wage(productivity: Double, K: Double, L: Double): Double = 
      _config.productionFunction.wage(productivity, K, L)
    
    def wage(pIdx: Int, K: Double, L: Double): Double = 
        wage(_config.aggregateProductivityShocks(pIdx), K, L)

    def wage(pIdx: Int, K: Double): Double = 
      wage(_config.aggregateProductivityShocks(pIdx), K, 
          aggLabourByProductivity(pIdx))



    def initWageRatios {
      wageRatiosbyProductivity = 
        createArrayOfSize(employmentDistByProductivity.size(): _*)

      // The hours worked, so that aggregate labour supply is 1 in bad state
      val hoursWorked = 1d / employmentDistByProductivity(1, 0)

      // Tax per hour to pay for unemployment insurance
      val taxes = _config.unemploymentInsuranceRate * (
          employmentDistByProductivity(0, $)\0 /
            employmentDistByProductivity(1, $) )

      // Unemployed just get the unemployment insurance
      wageRatiosbyProductivity(0, $) += _config.unemploymentInsuranceRate * 
        hoursWorked

      // employed get the taxed hourly wage times hours worked
      wageRatiosbyProductivity(1, $) += (1d - taxes) * hoursWorked
    }

    def initEmploymentDist {
      var ggTrans = _config.getExogenousStateTransition()($, 1, $, 1, 0)
      var bbTrans = _config.getExogenousStateTransition()($, 0, $, 0, 0)

      // DON'T MODIFY SELF BECAUSE THAT WOULD MODIFY EXOGENOUS TRANSITON (ABOVE)
      // Normalise them so that total probs from each state are 1
      ggTrans = ggTrans\0 / (ggTrans\1 sum)
      bbTrans = bbTrans\0 / (bbTrans\1 sum)

      // Find the steady state of the stochastic processes
      for (i <- 1 until 30) {
        ggTrans = ggTrans %* ggTrans
        bbTrans = bbTrans %* bbTrans
      }

      // Normalise again to get rid of small errors
      ggTrans\0 /= (ggTrans\1 sum)
      bbTrans\0 /= (bbTrans\1 sum)

      // Create an array to hold the steady state distributions
      employmentDistByProductivity = createArrayOfSize(2, 2)

      // Fill it with the steady states for bad and good aggregate productivity 
      // respectively
      employmentDistByProductivity($, 0) << bbTrans(0, $)
      employmentDistByProductivity($, 1) << ggTrans(0, $)

      // Make sure the employment proportions add to 1 in each state
      employmentDistByProductivity(0, $) << 
                            1d - employmentDistByProductivity(1, $)

      // Also calculate the aggregate labour supply
      // This is normalised to one in the bad state, so that hours worked per employee is not one!
      aggLabourByProductivity = createArray(1, 
          employmentDistByProductivity(1, 1)/employmentDistByProductivity(1, 0))
    }      
  }

  class IM_EGM_Solver(model: Model, config: Config) 
    extends EGMIndividualProblemSolver[Config, State, Model](model, config) {

    // The end of period states are just the capital levels to be carried over
    setEndOfPeriodStates(_model.createIndividualVariableGrid()\(0) << 
        _config.individualCapitalLevels)
    
    setStartOfPeriodStates(_model.normalisedLiquidAssets)
     
    // This calculates the future part of the Euler equation, and is called 
    // once for each possible future stochastic state and current agg state
    override def calculateFutureConditionalExpectations(
        exogenousTransition: Array[Int], 
        currentAggs: Array[Int], 
        state: State): DoubleArray = {

      val Array(_,currentAggStateIndex, futureIndStateIndex, 
          futureAggStateIndex, futurePermShockIndex) = exogenousTransition
      val aggCapIndex = currentAggs(0)

      /* Collect the data needed for the calculation
       */
      //The expected future capital carried over for individuals in this state
      val kpp = state.getExpectedIndividualTransition()(
          currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, 
            aggCapIndex, $, futureIndStateIndex)

      //Collect expected future aggregates
      val R = state.expectedR(currentAggStateIndex, futureAggStateIndex, 
            futurePermShockIndex, aggCapIndex, 0)
      val w = state.expectedW(currentAggStateIndex, futureAggStateIndex, 
            futurePermShockIndex, aggCapIndex, 0) *
            // Need to adjust the aggregate wage for the employment state
              _model.wageRatiosbyProductivity(futureIndStateIndex, 
                  futureAggStateIndex) 

      // Permanent shock
      val perm = _config.permanentAggregateShockLevels(futurePermShockIndex)

      /* Calculate the value of the euler condition conditional on the future 
       * states
       */
      //First, calculate consumption as starting capital times interest plus 
      // wages minus ending capital
      val individualCapitalLevels: DoubleArray = _config.individualCapitalLevels

      // Apply interest and normalise by permanent shocks
      val kpMult = R / perm

      // Calculate consumption at this point
      // !!! KPP is from the function interpolated to normalised grid 
      // Need not take the permanent shock into account yet again!
      val c = (individualCapitalLevels :: kpp) -> (
          (kp, kpp) => kp * kpMult + w - kpp * _config.growthRate)

      // Need to normalize by growth and future permanent shock
      val growthFactor = _config.growthRate * perm
      
      /* Now determine the future euler value, i.e. the discounted marginal 
       * utility of consumption
       */
      val fce = c -> (c => R * 
          _config.utilityFunction.marginalUtility(0, growthFactor * c))

      // Return that value
      fce
    }

    /**
     * Given expectations over the future euler, determine the implied current 
     * liquid assets
     */
    override def calculateImpliedStartOfPeriodState(
        individualExpectations: JavaDoubleArray, state: State) = {

      // The last dimension is of size 1, so select it away to match sizes
      val impliedLiquidAssets = (individualExpectations :: _eopStates) -> 
      (( futureEuler, kp ) => {

        // Invert implied marginal utility
        val c = _config.utilityFunction.inverseMarginalUtility(
                                      _config.discountRate * futureEuler)

        /* USING THE GROWTH FACTOR HERE MEANS THAT kp is 'normalised' by it */
        // Liquid assets must be equal to consumption plus final capital
        c + kp * _config.growthRate
      })

      impliedLiquidAssets
    }

    /**
     * Overridden to reduce # of calcs
     */
    override def updateError(
        oldPolicy: JavaDoubleArray, newPolicy: JavaDoubleArray, state: State) {
      if(state.getPeriod % 10 == 5) {
        state.setIndividualError(
           maximumRelativeDifferenceSpecial(newPolicy(20, $), oldPolicy(20, $)))
      }
    }
  }
      
  class IM_DA_Solver(model: Model, config: Config, 
      simulator: DiscretisedDistributionSimulator) 
    extends DefaultDASolver[Config, State, Model](model, config, simulator) {

    // Use a log linear rather than a linear transition
    useLogs(true)

    // These grids needs to be the size of the simulation grid
    // dF/dk = 1 (it is the mean)
    setAggregationByIndividualStateDerivative(0, 0, 
        model.createSimulationGrid() << 1)
        
    // dk/dK = k (proportional wealth increase)
    setTransformationByIndStateDerivative(0, 0, 
        model.createSimulationGrid()\(0) << 
          config.getIndividualEndogenousStatesForSimulation()(0))
  }
}