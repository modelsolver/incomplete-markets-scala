package com.meliorbis.economics.incompletemarkets

import java.io.File
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.Options
import org.apache.commons.lang.ArrayUtils
import com.meliorbis.economics.aggregate.derivagg.DerivativeAggregationSolver
import com.meliorbis.economics.aggregate.derivagg.DerivAggCalcState
import com.meliorbis.economics.core.CRRAUtility
import com.meliorbis.economics.core.CobbDouglasProduction
import com.meliorbis.economics.core.ProductionFunction
import com.meliorbis.economics.core.UtilityFunction
import com.meliorbis.economics.individual.egm.EGMIndividualProblemSolver
import com.meliorbis.economics.infrastructure.AbstractModel
import com.meliorbis.economics.infrastructure.AbstractStateBase
import com.meliorbis.economics.infrastructure.simulation.SimulationObserver
import com.meliorbis.economics.infrastructure.ModelConfigBase
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.economics.infrastructure.simulation.SimState
import com.meliorbis.economics.infrastructure.simulation.SimulationResults
import com.meliorbis.economics.model.ModelRunner
import com.meliorbis.economics.scala.ModelHelpers.arrayChangedConv
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.generic.MultiDimensionalArray
import com.meliorbis.numerics.generic.impl.GenericBlockedArray
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.numerics.generic.primitives.impl.Interpolation._
import com.meliorbis.numerics.io.NumericsReader
import com.meliorbis.numerics.io.NumericsWriter
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.utils.Utils
import com.meliorbis.numerics.markov.DiscreteStochasticProcessFactory
import java.util.Arrays
import com.meliorbis.utils.Pair
import com.meliorbis.economics.infrastructure.simulation.RepresentativeAgentSimState
import com.meliorbis.economics.aggregate.RAAggregateSolver
import com.meliorbis.economics.aggregate.AggregateProblemSolver
import org.apache.commons.math3.util.Precision
import com.meliorbis.economics.aggregate.transformedDist.TransformedDistributionSolver
import scala.collection.JavaConversions._
import com.meliorbis.numerics.Numerics
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.IntArrayFactories._
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulator
import com.meliorbis.numerics.convergence.Criterion
import com.meliorbis.economics.aggregate.ks.KrusellSmithSolver
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulatorImpl
import scala.util.Random

/**
 * An implementation of the standard incomplete markets model a ''la Krusell & Smith (98)'' using the
 * method of endogenous gridpoints and derivative aggregation
 *
 * @author Tobias Grasl
 */
object IncompleteMarketsModel {

  type DerivationInformation = (DoubleArray, DoubleArray, DoubleArray, DoubleArray)
  
  var _useDHData = false
  var _aggRisk = false
  var _indRisk = false
  var _steady = false
  var _raShocks = false
  var _updateDTheta = false
  var _transformedDist = false
  var _permanentShocks = false
  var _ks = false
  
  def main(args: Array[String]) {
    println(s"Running ${this.getClass.getName}")

    Runner.run(args)
  }

  object Runner extends ModelRunner[Model, Config, State] {
    
    override def createOptions(): Options =
      {
        val options = super.createOptions();

        options.addOption("noAgg", false, "Solve a model without aggregate uncertainty")
        options.addOption("noInd", false, "Solve a model without idiosyncratic risk")
        options.addOption("dhdata", false, "Use the shock sequence and initial distribution from the den Haan comparison paper")
        options.addOption("raShocks", false, "Use the shock sequence for the representative agent model shared with Dynare")
        options.addOption("steady", false, "Simulate some 0s to find the stochastic steady state")
        options.addOption("gradSimSteps", true, "Number of steps to perform during each derivative agg. iteration")
        options.addOption("secondOrder", false, "Use a second order, rather than a first order, approximation")
        options.addOption("largeShocks", false, "Use a calibration with large shocks, 10% each way")
        options.addOption("dtheta", false, "Update dtheta as the calc progresses")
        options.addOption("transformedDist", false, "Transform dists to desired capital levels directly rather.")
        options.addOption("useLogs", false, "Calculate the aggregate transition in logs, rather than level.")
        options.addOption("permShocks", false, "Include permanent shocks in the model.")
        options.addOption("ks", false, "Use KS Solution Approach")
        
        return options
      }

    override def simulateModel(model: Model, state: State, periods: Int, burnIn: Int, stateDir: File, simPath: String): SimulationResults[_, _] = {
      
      if(_indRisk) {
        return simulateModelSpecialised(model, state, periods, burnIn, stateDir, simPath)
      }
      else {
        return simulateRAModel(model, state, periods, burnIn, stateDir, simPath)
      }
    }
    
        /**
     * Parametrised method so that the internal assumption on what type of simstate is used  can be
     * consistent
     */
    def simulateRAModel[D <: SimState](model: Model, state: State, periods: Int, burnIn: Int, stateDir: File, simPath: String): SimulationResults[D, java.lang.Double] = {
          
      val initial = model.initialSimState(state).asInstanceOf[Pair[D, java.lang.Double]]
          
      // Was simulation with the shared representative agent shocks data requested?
      if(_raShocks){
        
        // Yes! Read the data, and simulate the relevant shocks
        var zStream = ClassLoader.getSystemResourceAsStream("raShocks.csv")

        var shocksFromCSV = Numerics.instance().readFormattedCSV[Double](zStream).asInstanceOf[DoubleArray]

        // Adjust the shocks because our numbering scheme differs from that
        // in the data
        var allShocks = createArrayOfSize(shocksFromCSV.size()(0), 2)
        allShocks($,0) << shocksFromCSV
        allShocks ->= (x => Math.exp(x))
        // Get the initial sim state and exogenous state(s) from the model
        
         // Get an appropriate simulator
        val simulator = getSimulator(initial)
        
        return simulator.simulateShocks(initial.getLeft, allShocks, model,
          state, SimulationObserver.silent(), stateDir, simPath)
        
      } else if (_steady) {

        // Get the initial sim state and exogenous state(s) from the model
        val initial = model.initialSimState(state).asInstanceOf[Pair[D, java.lang.Double]]

        // Get an appropriate simulator
        val simulator = getSimulator(initial)
        
        // A 'zero' shock is actually 1 (i.e. the mean productivity)
        // There are 2 shocks each period due to the permanent shock, also 'zero'
        var zeros = createArrayOfSize(100,2)
        zeros += 1d
        
        var simResults : SimulationResults[D, java.lang.Double] = null
        var lastInitial = initial.getLeft
        var relDiff = 1d
        var count = 0
        do {
          count = count + 1
          // If there are already results, use the end point as the new start
          if( simResults != null ) lastInitial = simResults.getFinalState
          
          simResults = simulator.simulateShocks(lastInitial, zeros, model, state, SimulationObserver.silent())
          
          relDiff = Math.abs(lastInitial.asInstanceOf[RepresentativeAgentSimState].getStates()(0)/simResults.getStates().last() - 1d )
        } while ( relDiff > 1e-8 )
        
        println(s"Number of Iterations: ${count}")
        println("Stochastic Steady State K: "+simResults.getStates().last())
        
        simResults
      } else  {
        // No - call the normal simulation
        return super.simulateModel(model, state, periods, burnIn, stateDir, simPath).asInstanceOf[SimulationResults[D, java.lang.Double]]
      }
    }

    
    /**
     * Parametrised method so that the internal assumption on what type of simstate is used  can be
     * consistent
     */
    def simulateModelSpecialised[D <: SimState](model: Model, state: State, periods: Int, burnIn: Int, stateDir: File, simPath: String): SimulationResults[D, Integer] = {

      // Was simulation with the den Haan data requested?
      if(_useDHData){
        // Yes! Read the data, and simulate the relevant shocks
        var zStream = ClassLoader.getSystemResourceAsStream("Z_Formatted.txt")

        var shocksFromCSV = Numerics.instance().readFormattedCSV[Integer](zStream).asInstanceOf[IntegerArray]

        // Adjust the shocks because our numbering scheme differs from that
        // in the data
        var allShocks = createIntArrayOfSize(shocksFromCSV.size()(0), 2)
        allShocks.at(-1, 0).fill(shocksFromCSV.add(Integer.valueOf(-1)))

        // Get the initial sim state and exogenous state(s) from the model
        val initial = model.initialSimState(state).asInstanceOf[Pair[D, Integer]]
        
         // Get an appropriate simulator
        val simulator = getSimulator(initial)
        
        val rand = new java.util.Random(125273)
        
        if( _permanentShocks ) {
          allShocks.set(1,0,1)
          
          for(i <- 1 until allShocks.size()(0)) {            
            allShocks.set(
                Utils.drawRandomState(createArray(.01,.98,.01), rand)(0), i,1)
          }
        }
          
        
        return simulator.simulateShocks(initial.getLeft, allShocks, model,
          state, SimulationObserver.silent(), stateDir, simPath);
        
        //return simFromInitial(initial)
      } else  {
        
        // Yes! Read the data, and simulate the relevant shocks
        var zStream = ClassLoader.getSystemResourceAsStream("Z_alternative.csv")

        var shocksFromCSV = Numerics.instance().readFormattedCSV[Integer](zStream).asInstanceOf[IntegerArray]

        // Adjust the shocks because our numbering scheme differs from that
        // in the data
        var allShocks = createIntArrayOfSize(shocksFromCSV.size()(0), 2)
        allShocks.at(-1, 0).fill(shocksFromCSV)

        // Get the initial sim state and exogenous state(s) from the model
        val initial = model.initialSimState(state).asInstanceOf[Pair[D, Integer]]
        
         // Get an appropriate simulator
        val simulator = getSimulator(initial)
        
        return simulator.simulateShocks(initial.getLeft, allShocks, model,
          state, SimulationObserver.silent(), stateDir, simPath);
        // No - call the normal simulation
        //return super.simulateModel(model, state, periods, burnIn, stateDir, simPath).asInstanceOf[SimulationResults[D, Integer]]
      }
    }    

    override def createConfig(commandLine: CommandLine): Config = {

      val largeShocks = commandLine.hasOption("largeShocks")

      _useDHData = commandLine.hasOption("dhdata")
      _raShocks = commandLine.hasOption("raShocks")
      _steady = commandLine.hasOption("steady")
      
      _aggRisk = !commandLine.hasOption("noAgg")
      _indRisk = !commandLine.hasOption("noInd")

      _transformedDist = commandLine.hasOption("transformedDist")

      _updateDTheta = commandLine.hasOption("dtheta")
      _permanentShocks = commandLine.hasOption("permShocks")
      _ks = commandLine.hasOption("ks")
      
      if(_steady && !(_aggRisk && !_indRisk)) {
        println("Warning: -steady only applies to representative agent model with aggregate uncertainty")  
        _steady = false
      }
     
      var capitalShare: Double = Double.NaN
      var depreciationRate: Double = Double.NaN
      var relRiskAversion: Double = Double.NaN

      val config = new Config()

      config.setInitialExogenousStates(createIntArray(0,0))
      
      // No permament shocks in any scenario right now
      config.setAggregateNormalisingExogenousStates(createArray(1d));

      val transition = if (!_indRisk) { 
        /* If there is no idiosyncratic risk, configure a representative agent (RA) model
         */

        // Just a single value possible for productivity
        config.setIndividualExogenousStates(createArray(1) << 1d)
 
        capitalShare = 1d / 3d
        depreciationRate = .025
        relRiskAversion = .5

        config._growth = 1.025 ^ .25

        config._discountRate = 0.98

        // The analytically calculated steady-state capital level
        val k_0 = 17.983129176634720 //*1.0101

        val kMin = k_0 - 4
        val kMax = k_0 + 4

        val kCount = 31
        val KCount = 31
        
        
        /* Individual and Aggregate capital levels are identical
         */
        val capitalLevels = pullToMean(createArray(kMin to (kMax, kCount - 1):_*),.5)
        val aggCapitalLevels = pullToMean(createArray(kMin to (kMax, KCount - 1):_*),.5)
        
        config.setIndividualEndogenousStates(capitalLevels)
        config.setAggregateEndogenousStates(aggCapitalLevels)

        if (!_aggRisk) {

          /* With no aggregate risk, only one agg prod level...
           */
          config.setAggregateExogenousStates(createArray(1) << 1d)

          // ... and a transition with certainty of achieving the one available point
          createArrayOfSize(1, 1, 1, 1, 1) << 1
        } else {

          val rho = .979
          val sigma_innovation = .0072
          val nAggProdLevels = 21

          val initialShocks = createIntArray(10)
          config.setInitialExogenousStates(initialShocks)
          
          // Calculate the std dev of the process from that of the innovation
          val sigma = ((sigma_innovation ^ 2) / (1 - (rho ^ 2))) ^ .5

//          val stochProc = new DiscreteStochasticProcessFactory(getNumerics()).tauchen(21, rho, sigma, 4)
          val stochProc = new DiscreteStochasticProcessFactory().rouwenhorst(21, rho, sigma_innovation)

          // The provided process is that of log-productivity, so need to exp
          config.setAggregateExogenousStates(stochProc.getLevels -> Math.exp _)
          
          // The joint individual states (constant!) and the just-created aggregate shock process
          createArrayOfSize(1, nAggProdLevels, 1, nAggProdLevels, 1) << stochProc.getTransitionProbabilities
          
          
        }
      } else {
        if (commandLine.hasOption("gradSimSteps")) {
          config._gradSimSteps = Integer.parseInt(commandLine.getOptionValue("gradSimSteps"))
        }

        config._secondOrder = commandLine.hasOption("secondOrder")

        config._discountRate = 0.99

        capitalShare = 0.36
        depreciationRate = 0.025
        relRiskAversion = 1.0

        // No growth
        config._growth = 1;

        config.setIndividualExogenousStates(createArray(0.15, (1 - 0.15 * 0.05) / 0.95));

        val capLevels = nLogSequence(200, 250, 1);
        val simCapLevels = nLogSequence(200, 2001, 0);

        config.setIndividualEndogenousStates(capLevels);
        config.setIndividualEndogenousStatesForSimulation(simCapLevels);

        // Create a capital grid with 15 points spaced around the mean 39.85, but with the points closer to the mean a little denser

        if (largeShocks) {

          config.setAggregateEndogenousStates(pullToMean(createArray(27.0 to (53.0, 29): _*), 0.2));
        } else {
          config.setAggregateEndogenousStates(pullToMean(createArray(0.9 * 39.85 to (1.1 * 39.85, 15): _*), 0.2));
        }

        var dhTrans = createArrayOfSize(2, 2, 2, 2, 1)

        // Transition from denHaan comparison paper
        dhTrans << (21d / 40d, 1d / 32d, 7d / 20d, 3d / 32d,
          3d / 32d, 7d / 24d, 1d / 32d, 7d / 12d,
          7d / 180d, 1d / 480d, 301d / 360d, 59d / 480d,
          7d / 768d, 7d / 288d, 89d / 768d, 245d / 288d)

        if (_aggRisk) {

          // The distribution to be used for derivative aggregation
          config._daDist = new DiscretisedDistribution(new File("Solutions/KS_NA/ergodicDist.mat"))

          if( _permanentShocks ) {
            config.setAggregateNormalisingExogenousStates(createArray(0.95,1d,1.05))
            
            var expandedTrans = createArrayOfSize(2, 2, 2, 2, 3)
            expandedTrans\(0,1,2,3) << dhTrans
            
            
            expandedTrans\(4) *= createArray(.01,.98,.01)
            
            dhTrans = expandedTrans
          }
          else {
            config.setAggregateNormalisingExogenousStates(createArray(1d))
          }
          
          if (largeShocks) {
            config.setAggregateExogenousStates(createArray(.9d, 1.1d));
          } else {
            config.setAggregateExogenousStates(createArray(.99d, 1.01d));
          }

          // Use the vanilla transition from the comparison paper
          dhTrans

        } else {

          config.setAggregateExogenousStates(createArray(1d))

          // Take the sum of the good-good transition, and the bad-bad one
          val indTrans = dhTrans($, 0, $, 0) + dhTrans($, 1, $, 1)

          // Normalise to total transition probability is 1
          indTrans \ 1 /= (indTrans \ 1).sum

          // Create a new array with the required singleton dimensions
          // fill and return
          createArrayOfSize(2, 1, 2, 1, 1) << indTrans
        }
      }

      // No permanent shock so nothing to multiply in
      config.setExogenousStateTransiton(transition)

      config._utilityFunction = new CRRAUtility(relRiskAversion)
      config._productionFunction = new CobbDouglasProduction(capitalShare, 1d - capitalShare, depreciationRate);

      config
    }

    /**
     * Which model class is returned depends on the aggregate and individual
     * shock processes selected 
     */
    override def getModelClass(): Class[_ <: Model] = {
      if(_aggRisk) {
        if(_indRisk)
          classOf[HetAgentAggRiskModel]
        else
          classOf[RAAggRiskModel]
      }
      else {
        if(_indRisk)
          classOf[HetAgentNoAggModel]
        else
          classOf[RANoAggModel]
      }
    }
  }

  abstract class Model extends AbstractModel[Config, State]() {

    var _employDist: DoubleArray = _
    var _aggLabour: Array[Double] = _
    var _wageRatios: DoubleArray = _

    // These arrays are for convenience - they are constant and are used to calculate wages and returns
    var _expectedLabour: DoubleArray = _
    var _expectedProductivity: DoubleArray = _

    var _interestMatrix: DoubleArray = _

    override def initialise(): Unit = {

      super.initialise()

      _interestMatrix = createArrayOfSize(
        _config.getAggregateCapitalLevels().numberOfElements(),
        _config.getAggregateProductivityShocks.numberOfElements())

      // Calculate the proportion of employed/unemployed in each aggregate state, and also the resulting
      // aggregate labour supply
      initEmploymentDist

      // Determine the relative wages earned by employed/unemployed in each state
      initWageRatios

      // These arrays are for convenience - they are constant and are used to calculate wages and returns
      _expectedLabour = createAggregateExpectationGrid(1);
      _expectedLabour.across(1) << createArray(_aggLabour: _*)

      _expectedProductivity = createAggregateExpectationGrid(1);
      _expectedProductivity.across(1) << _config.getAggregateProductivityShocks()

      addAggregateExpectationListener(afterAggregateExpectationUpdate _)

    }

    /**
     * If dtheta is not being updated, always update aggregates; otherwise, allow
     * 50 periods for the calc to settle
     */
    override def shouldUpdateAggregates(state: State): Boolean = 
      (!_ks && (!_updateDTheta || state.getPeriod > 50)) ||
      (_ks && state.getIndividualCriterion.getValue < 1e-6)

    override def calculateAggregateStates(distribution: SimState, aggregateExogenousStates: IntegerArray, state: IncompleteMarketsModel.this.State): Array[Double] = {
      return Array(distribution.mean(_config.getIndividualEndogenousStatesForSimulation().get(0)))
    }

    /**
     * Provided in subclasses
     */
    def initEmploymentDist

    override def readAdditional(state: State, reader: NumericsReader): Unit = {

      // If the policy that has been read has only one shock level it is an NA state - need to expand
      if (state.getIndividualPolicy().size()(3) == 1) {

        val indPolicy = createIndividualTransitionGrid()
        indPolicy.across(0, 1, 2) << state.getIndividualPolicy()($, $, $, 0, 0, 0)

        state.setIndividualPolicy(indPolicy)

        // Also need to initialise the aggregate transition, because that will be the NA (constant) transition
        initAggregateTransition(state)
      }

      // Calculate expectations from the aggregate transition
      adjustExpectedAggregates(state)

      try {
        var gradSimState = new DiscretisedDistribution()
        gradSimState.read(reader, "finalGradDensity")

        // Make sure the simulation starts from the same distribution used for derivative aggregation
        _config.setInitialSimState(gradSimState)
        
        println("Simulation will start from last grad sim state")
      } catch {
        case e: Exception => {
          println("No grad density read")
        }
      }
    }

    /**
     * Calculates the after-tax wages implied by aggregate employment, unemployment insurance and
     * a book-balancing government
     */
    def initWageRatios

    /**
     * Returns the initial state of the simulation
     */
    def initialSimState(state: State) : Pair[_, _]
    
    /**
     * Creates distributions appropriate to determine the gradient in each state, by adjusting the
     * level of unemployment expected in that state
     */
    def prepareDistributionsForGrad(simState: DiscretisedDistribution): Array[DiscretisedDistribution] =
      {
        val aggProdStates = _config.getAggregateProductivityShocks();

        val gradSimStates = new Array[DiscretisedDistribution](aggProdStates.numberOfElements())

        for (i <- 0 until aggProdStates.numberOfElements()) {
          gradSimStates(i) = adjustDistributionToState(simState, i);
        }

        
        // Make sure the simulation starts from the same distribution used for derivative aggregation
        _config.setInitialSimState(gradSimStates(0))

        return gradSimStates
      }

    def determineEmploymentDistribution(distribution: DiscretisedDistribution): DoubleArray =
      {
        distribution._density.across(0).sum().add(distribution._overflowProportions)
      }

    def adjustDistributionToState(distribution: DiscretisedDistribution, state: Int): DiscretisedDistribution =
      {
        val simState = distribution.clone()

        val unemp = _employDist(0, state)

        // Get the unemployment level in the provided distribution
        val empProps = determineEmploymentDistribution(simState);

        // Adjust the distribution to the appropriate unemployment level
        if (empProps(0) - unemp > 1e-10) {
          simState._density($, 1).modifying().add(simState._density($, 0).multiply(1d - unemp / empProps.get(0)))
          simState._density($, 0).modifying().multiply(unemp / empProps.get(0))

          simState._overflowProportions($, 1).modifying().add(simState._overflowProportions($, 0).multiply(1d - unemp / empProps.get(0)))
          simState._overflowProportions($, 0).modifying().multiply(unemp / empProps.get(0))
        } else {
          simState._density($, 0).modifying().add(simState._density($, 1).multiply(1d - (1d - unemp) / empProps.get(1)));
          simState._density($, 1).modifying().multiply((1d - unemp) / empProps.get(1));

          simState._overflowProportions($, 0).modifying().add(simState._overflowProportions($, 1).multiply(1d - (1d - unemp) / empProps.get(1)));
          simState._overflowProportions($, 1).modifying().multiply((1d - unemp) / empProps.get(1));
        }

        return simState;
      }

    def initialState(): IncompleteMarketsModel.this.State = {

      // Create a state object for this configuration 
      val state = new State(_config)

      /* Create the initial guess for the individual capital transition
      */
      val initialTransition = createIndividualTransitionGrid()

      initialTransition.across(0) << _config.getIndividualCapitalLevels() / _config._growth

      // Adjust the transition for possible future permanent shocks
      // This dimension is the second to last (the last is of size 1 and is the number of variables in the
      // grid/function)
      initialTransition.modifying().across(initialTransition.numberOfDimensions - 2).divide(
        _config.getPermanentAggregateShockLevels())

      val delta = 0.025;

      // Adjust it according to individual shock
      val kpAdjust = createArray(1d - delta, 1d + delta / 10d);

      initialTransition.modifying().across(1).multiply(kpAdjust)

      // Set the transition on the initial state
      state.setIndividualPolicy(initialTransition)

      state._dtheta = new Array[DoubleArray](2)
      state._doubleDerivs = new Array[DoubleArray](2)
      state._dthetaDists = new Array[DiscretisedDistribution](4)
      
      state._derivativeCalcInfo = new Array[DerivationInformation](2)
      /* Prepare the grids of aggregate prices, which vary along the aggregate capital and
       * productivity levels
       */
      val wageMatrix = createArrayOfSize(_interestMatrix.size(): _*)

      for (K_idx <- 0 until _config.getAggregateCapitalLevels().numberOfElements()) {
        for (P_idx <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {

          // Get the capital and Labour supply at the current point
          val K = _config.getAggregateCapitalLevels()(K_idx)
          val L = _aggLabour(P_idx)

          _interestMatrix(K_idx, P_idx) = grossInterest(P_idx, K, L)
          wageMatrix(K_idx, P_idx) = wage(P_idx, K, L)
        }
      }

      val individualWages = createArrayOfSize(_config.getIndividualExogenousStates().get(0).numberOfElements(),
        _config.getAggregateCapitalLevels().numberOfElements(), _config.getAggregateProductivityShocks().numberOfElements())

      // Fill the wage matrix across the aggregate capital & productivity dimensions
      individualWages.across(1, 2) << wageMatrix

      // Multiply the aggregate wage by the actual wage individual gets, due to unemp benefit and taxes
      individualWages.modifying().across(0, 2).multiply(_wageRatios);

      // The liquid assets at the beginning of the period are prior capital with interest + wages
      state._normalisedLiquidAssets = createIndividualTransitionGrid()
      state._normalisedLiquidAssets.across(0) << _config.getIndividualCapitalLevels()

      // Normalize the capital carried over by the permanent shock
      state._normalisedLiquidAssets.modifying().across(state._normalisedLiquidAssets.numberOfDimensions - 2).divide(
        _config.getAggregateNormalisingExogenousStates().get(0))

      // Mutliply the capital by the rate of return
      state._normalisedLiquidAssets.modifying().across(2, 3).multiply(_interestMatrix)

      // Add the wage
      state._normalisedLiquidAssets.modifying().across(1, 2, 3).add(individualWages)

      // We need to do the same for the simulation grid, which may have a different size
      val laSize = state._normalisedLiquidAssets.size().clone()
      laSize(0) = _config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements()

      val simLA = createArrayOfSize(laSize: _*)
      simLA.across(0) << _config.getIndividualEndogenousStatesForSimulation().get(0)

      simLA.modifying().across(simLA.numberOfDimensions - 2).divide(
        _config.getAggregateNormalisingExogenousStates().get(0))

      // Mutliply the capital by the rate of return
      simLA.modifying().across(2, 3).multiply(_interestMatrix)

      // Add the wage
      simLA.modifying().across(1, 2, 3).add(individualWages)

      state._simLiquidAssets = simLA
      initAggregateTransition(state)

      // Calculate expectations from the aggregate transition
      adjustExpectedAggregates(state)

      // The end of period states are just the capital levels to be carried over
      val simCapitalGrid = createSimulationGrid()

      simCapitalGrid.across(0) << (_config.getIndividualEndogenousStatesForSimulation().get(0))

      state._simCapitalGrid = simCapitalGrid

      return state
    }

    private def initAggregateTransition(state: com.meliorbis.economics.incompletemarkets.IncompleteMarketsModel.State): Unit = {
      val aggTrans = createAggregateVariableGrid()

      /* Now initialise the aggregate capital transition 
       */
      if (getConfig().hasAggUncertainty) {
        aggTrans.across(0) << (squeezeToMean(_config.getAggregateCapitalLevels(), 0.1))

        // Then adjust it for the expected shock
        aggTrans.modifying().across(1).multiply(_config.getAggregateProductivityShocks()).
          // Finally, make sure none of the capital levels transitioned to is outside the grid bounds
          map(cutToBounds(_config.getAggregateCapitalLevels().first, _config.getAggregateCapitalLevels().last))
      } else {
        // In the case with no aggregate uncertainty,  
        // K is constant so any capital level just stays the same
        aggTrans.across(0) << (_config.getAggregateCapitalLevels())
      }

      //      aggTrans.<<(0,1)(_numerics.readFormattedCSV[Double]("java_agg_trans.csv").asInstanceOf[DoubleArray])
      state.setAggregateTransition(aggTrans)
    }

    /**
     * Adjusts the expected capital level for permanent shocks (in the next period) and also calculates
     * expected prices given the expected factor inputs
     */
    def afterAggregateExpectationUpdate(oldVal: DoubleArray, newVal: DoubleArray, state: State) {

      // Adjust for future permanent shocks
      state.getExpectedAggregateStates().modifying().across(2).divide(_config.getAggregateNormalisingExogenousStates().get(0))

      // Update expected prices
      val Array(w, r) = (state.getExpectedAggregateStates() :: _expectedLabour :: _expectedProductivity) :-> (in => {
          val Array(k,l,p) = in

          Array(wage(p, k, l), grossInterest(p,k,l))
        })

      state._expectedInterest = r
      state._expectedWage = w
        
    }

    def grossInterest(pIdx: Int, K: Double, L: Double): Double = grossInterest(_config.getAggregateProductivityShocks()(pIdx), K, L)

    def wage(pIdx: Int, K: Double, L: Double): Double = wage(_config.getAggregateProductivityShocks()(pIdx), K, L)

    def grossInterest(pIdx: Int, K: Double): Double = grossInterest(_config.getAggregateProductivityShocks()(pIdx), K, _aggLabour(pIdx))

    def wage(pIdx: Int, K: Double): Double = wage(_config.getAggregateProductivityShocks()(pIdx), K, _aggLabour(pIdx))

    def grossInterest(productivity: Double, K: Double, L: Double): Double = 1 + _config._productionFunction.netReturn(productivity, K, L)

    def wage(productivity: Double, K: Double, L: Double): Double = _config._productionFunction.wage(productivity, K, L)
  }

  /* Common stuff for the heterogeneous-agent scenarios
   */
  abstract class HAModel extends Model {

    override def initWageRatios {
      _wageRatios = createArrayOfSize(_employDist.size(): _*);

      // The hours worked, so that aggregate labour supply is 1 in bad state
      val hoursWorked = 1d / _employDist(1, 0);

      // Tax per hour to pay for unemployment insurance
      val taxes = _employDist(0, $).across(0).divide(_employDist(1, $)) * _config._unemploymentInsuranceRate

      // Unemployed just get the unemployment insurance
      _wageRatios(0, $) += _config._unemploymentInsuranceRate * hoursWorked

      // employed get the taxed hourly wage times hours worked
      _wageRatios(1, $) += (taxes * (-1) + 1) * hoursWorked
    }
    
    /**
     *  Write some additional values not captured by the framework to the saved state
     */
    override def writeAdditional(state: State, writer: NumericsWriter): Unit = {

      if (getConfig().hasAggUncertainty()) {
        state._gradSimStates(0).write(writer, "finalGradDensity")
        state._gradSimStates(1).write(writer, "finalGradDensity_1")
        
        if( _updateDTheta ) {
          writer.writeArray("dtheta", state._dtheta(0))
          writer.writeArray("dtheta_1", state._dtheta(1))
          
          for(i <- 0 until 4) {
           state._dthetaDists(i).write(writer,s"""dthetaDist$i""") 
          }
          
          writer.writeArray("d2theta0",  state._doubleDerivs(0))
          writer.writeArray("d2theta1",  state._doubleDerivs(1))
        }
        
        if(!_ks && !_transformedDist) {
          val derivation = createArrayOfSize(2, if(_config._secondOrder) 4 else 3)
  
          for(i <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {
            
            derivation(i, 0) = state._derivativeCalcInfo(i)._1(0)
            derivation(i, 1) = state._derivativeCalcInfo(i)._2(0)
            derivation(i, 2) = state._derivativeCalcInfo(i)._3(0)
            
            if( _config._secondOrder) {
              derivation(i, 3) = state._derivativeCalcInfo(i)._4(0)
            }
          }
          
          writer.writeArray( "derivation", derivation )
        }
      }
    }
    
    override def initialState = {
      
      val state = super.initialState
      
        if (_config.hasAggUncertainty) {
          state._gradSimStates = prepareDistributionsForGrad(_config._daDist)
        } else {
          _config.setInitialSimState(initialDensity(state))
        }
      
      state
    }
  
    def initialDensity(state: State): com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution = {

      val youngDist = getNumerics().readCSV(ClassLoader.getSystemResourceAsStream("pdistyoung.txt"), '\t')

      /* Copy the second two columns, the first contains cap levels
       */
      val initialDist = createArrayOfSize(2001, 2)

      initialDist($, 0) << youngDist($, 1)
      initialDist($, 1) << youngDist($, 2)

      initialDist.modifying().across(1).divide(initialDist.across(0).sum())

      initialDist.modifying().across(1).multiply(_employDist($, 0))

      val simState = getSimulator().createState()

      simState._density = initialDist
      simState._overflowProportions = createArrayOfSize(1, 2)
      simState._overflowAverages = createArrayOfSize(1, 2)

      return simState
    }

    override def initialSimState(state: State) : Pair[DiscretisedDistribution, Integer] = {
      return new Pair(initialDensity(state), 0)
    }
  }

  class HetAgentAggRiskModel extends HAModel {

    override def initEmploymentDist {
      var ggTrans = _config.getExogenousStateTransition()($, 1, $, 1, 0)
      var bbTrans = _config.getExogenousStateTransition()($, 0, $, 0, 0)

      // Normalise them so that total probs from each state are 1
      ggTrans = ggTrans.across(0).divide(ggTrans.across(1).sum())
      bbTrans = bbTrans.across(0).divide(bbTrans.across(1).sum())

      // Find the steady state of the stochastic processes
      for (i <- 1 until 30) {
        ggTrans = ggTrans %* ggTrans
        bbTrans = bbTrans %* bbTrans
      }

      // Normalise again to get rid of small errors
      ggTrans.modifying().across(0).divide(ggTrans.across(1).sum())
      bbTrans.modifying().across(0).divide(bbTrans.across(1).sum())

      // Create an array to hold the steady state distributions
      _employDist = createArrayOfSize(2, 2)

      // Fill it with the steady states for bad and good aggregate productivity respectively
      _employDist($, 0) << bbTrans(0, $)
      _employDist($, 1) << ggTrans(0, $)

      // Make sure the employment proportions add to 1 in each state
      _employDist(0, $) << _employDist(1, $) * (-1) + 1d

      // Also calculate the aggregate labour supply
      // This is normalised to one in the bad state, so that hours worked per employee is not one!
      _aggLabour = Array(1, _employDist(1, 1) / _employDist(1, 0))
    }

    override def initialDensity(state: State): com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution =  {
      
      // If the final density from DA was found, use that
      if ( state._gradSimStates != null ) {
        
        println("Using final grad density for simulation")
        
        return state._gradSimStates(0)
      }
      
      // Otherwise, do what that man Den Haan did
      return super.initialDensity( state )
    }
  }

  class HetAgentNoAggModel extends HAModel {
    
    override def initEmploymentDist {

      var trans: DoubleArray = _config.getExogenousStateTransition().at(-1, 0, -1, 0, 0).copy()

      // Normalise to total probs from each state are 1
      trans = trans.across(0).divide(trans.across(1).sum());

      // Converge the transition matrix to the steady state
      for (i <- 1 to 30) {
        trans = trans %* trans
      }

      // Normalise again to get rid of small errors
      trans.modifying().across(0).divide(trans.across(1).sum())

      // Create an array to hold the ergodic employment distribution
      _employDist = createArrayOfSize(2, 1)
      _employDist << trans(0, $)

      // Make sure the proportions add to 1
      _employDist(0, 0) = 1d - _employDist(1, 0)
      _aggLabour = Array(1d);
    }

  }

  abstract class RAModel extends Model {
 
    override def initialState() : State = {
      val theState = super.initialState()
      
      // Set the aggregate error to 0, since the individual error
      // captures the solution progress
      theState.setAggregateError(0)
      theState.setIndividualError(1)
      
      theState
    }
    
    override def initEmploymentDist {

      val nAggShocks = _config.getAggregateProductivityShocks().numberOfElements()

      // Create an appropriately sized array, depending on agg uncertainty
      _employDist = if (_config.hasAggUncertainty)
        createArrayOfSize(1, nAggShocks)
      else
        createArrayOfSize(1, 1)

      // Employment is always 1, since here is only one type of agent!
      _employDist << 1

      // Ditto for labour supply
      _aggLabour = Utils.repeatArray(1d, nAggShocks)
    }

    override def initWageRatios {
      // Everyone gets one in all states
      _wageRatios = createArrayOfSize(_employDist.size(): _*) << 1.0
    }

    override def initialSimState(state: State) : Pair[RepresentativeAgentSimState, java.lang.Double] = {
      return new Pair(
            new RepresentativeAgentSimState(createArray(17.983128815303306)), 0d)
    }
  }

  class RANoAggModel extends RAModel {
    
    
    override def adjustExpectedAggregates(state: State) {

      /* This is not really the ideal place to do it, but Representative Agent
       * Model behaviour has not yet been perfected    
       */
      val aggTrans = state.getAggregateTransition
      val indTrans = state.getIndividualPolicy

      // The agg trans is just the individual trans at the point where the individual and agg capital levels
      // are both the same as the assumed individual capital level
      for (idx <- 0 until _config.getAggregateCapitalLevels.numberOfElements) {
        aggTrans(idx, 0, 0) = indTrans(idx, 0, idx, 0, 0, 0)
      }

      super.adjustExpectedAggregates(state)
    }
  }

  class RAAggRiskModel extends RAModel {
    override def adjustExpectedAggregates(state: State) {

      var stepSize = (_config.getIndividualCapitalLevels().numberOfElements()-1)/(_config.getAggregateCapitalLevels().numberOfElements()-1)
      /* This is not really the ideal place to do it, but Representative Agent
       * Model behaviour has not yet been perfected    
       */
      val aggTrans = state.getAggregateTransition
      val indTrans = state.getIndividualPolicy

      // The agg trans is just the individual trans at the point where the individual and agg capital levels
      // are both the same as the assumed individual capital level
      for (idx <- 0 until _config.getAggregateCapitalLevels.numberOfElements) {
        aggTrans(idx, $, 0) << indTrans(idx*stepSize, 0, idx, $, 0, 0)
      }

      super.adjustExpectedAggregates(state)
    }
  }

  class IM_EGM_Solver(model: Model, config: Config) extends EGMIndividualProblemSolver[Config, State, Model](model, config) {

      // The end of period states are just the capital levels to be carried over
    setEndOfPeriodStates(
        _model.createIndividualVariableGrid()\0 << 
          _config.getIndividualCapitalLevels())

    override def initialise(state : State) {
      setStartOfPeriodStates(state._normalisedLiquidAssets)
    }

    override def calculateFutureConditionalExpectations(exogenousTransition: Array[Int], currentAggs: Array[Int], state: State): DoubleArray = {

      val currentAggStateIndex = exogenousTransition(1);
      val futureIndStateIndex = exogenousTransition(2);
      val futureAggStateIndex = exogenousTransition(3);

      val futurePermShockIndex = exogenousTransition(4);

      val aggCapIndex = currentAggs(0);

      /* The expected future capital carried over for individuals in this state
  		 */
      val kpp = state.getExpectedIndividualTransition()(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, $, futureIndStateIndex)

      /* Collect expected future aggregates
  		 */
      val R = state._expectedInterest(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, 0)

      // Need to adjust the aggregate wage for the individuals employment state and dt
      val w = state._expectedWage(currentAggStateIndex, futureAggStateIndex, futurePermShockIndex, aggCapIndex, 0) * _model._wageRatios(futureIndStateIndex, futureAggStateIndex)
      val permShock = _config.getAggregateNormalisingExogenousStates().get(0)(futurePermShockIndex)

      /* Calculate the value of the euler condition conditional on the future states
       */
      return calculateConditionalFutureEuler(kpp, R, w, permShock, _config._discountRate, _config._growth)
    }

    def calculateConditionalFutureEuler(kpp: DoubleArray, futureR: Double, futureW: Double, futureStochasticNormalisation: Double,
      discountRate: Double, growth: Double): DoubleArray =
      {
        /* First, calculate consumption as starting capital times interest plus wages minus ending capital
       */
        val individualCapitalLevels: DoubleArray = _config.getIndividualCapitalLevels()

        // start of period capital needs to be multiplied with R but divided by the permanent shock
        val kpMult = futureR / futureStochasticNormalisation

        // Calculate consumption at this point
        // !!! KPP is from the function interpolated to the grid with the permanent shock, so need not take the 
        // permanent shock into account yet again!
        val c = (individualCapitalLevels :: kpp) -> ((kp: Double, kpp: Double) => kp * kpMult + futureW - kpp * _config._growth)

        // Need to normalize by growth and future permanent shock
        val growthFactor = growth * futureStochasticNormalisation
        
        /* Now determine the future euler value, i.e. the discounted marginal utility of consumption
         */
        val fce = c map ((c: Double) => futureR * _config._utilityFunction.marginalUtility(0, growthFactor * c))

        // Return that value
        fce
      }

    /**
     * Given expectations over the future euler, determine the implied current liquid assets
     */
    override def calculateImpliedStartOfPeriodState(individualExpectations: com.meliorbis.numerics.generic.primitives.DoubleArray[_], state: State): DoubleArray = {

      // The last dimension is of size 1, so select it away to match sizes
      val impliedLiquidAssets = (individualExpectations :: _eopStates) -> (
          (futureEuler: Double, kp: Double) => {

        // We calculated the marginal utility in the euler calculation, now invert it for c
        val c = _config._utilityFunction.inverseMarginalUtility(_config._discountRate * futureEuler);

        /* WE USE THE GROWTH FACTOR HERE, WHCH MEANS THAT kp_ is 'normalised'
						 * by it
						 */
        // Liquid assets must be equal to consumption plus final capital, adjusted for growth
        c + kp * _config._growth;
      })

      return impliedLiquidAssets
    }

    
     override def updateError(
        oldPolicy: JavaDoubleArray, newPolicy: JavaDoubleArray, state: State) {
      if(state.getPeriod % 10 == 5) {
        state.setIndividualError(
           maximumRelativeDifferenceSpecial(newPolicy(20, $), oldPolicy(20, $)))
      }
    }
  }

   class IM_Transformed_Solver(
       model: Model, 
       config: Config,
       sim: DiscretisedDistributionSimulator) extends 
         TransformedDistributionSolver[Config, State, Model](model, config, sim) {
     
     var _capitalArray: DoubleArray = _
     
     addTransitionListener((oldVal: DoubleArray, newVal: DoubleArray, state: State) => {
       state.setAggregateError(maximumRelativeDifference(oldVal, newVal))
     })
      
     override def initialise(state: State) {
       super.initialise(state)
       
       _capitalArray = createArrayOfSize(config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements(), config.getIndividualExogenousStates().get(0).numberOfElements())
       _capitalArray\(0) << _config.getIndividualEndogenousStatesForSimulation().get(0) 
     }
     
     def adjustDistributionToAggregates(distribution: DiscretisedDistribution, currentAggs: Array[Double], targetAggs: Array[Double]): DiscretisedDistribution = {
      
       // There is only one aggregate, capital, which needs to be adjusted; calculate the adjustment factor
       val factor = targetAggs(0)/currentAggs(0)
       
       val transformed = distribution.createSameSized()
       
     // Create a transition function which changes capital by the given factor
       val function = createArrayOfSize(_capitalArray.size()(0), _capitalArray.size()(1),1)
       function\(0) << (_config.getIndividualEndogenousStatesForSimulation().get(0)  * factor )
       
       // Create an identity transition, where the state never changes
       val transProbs = createArrayOfSize(2,2)
       transProbs << Array(1d,0d,0d,1d)
       
       
       _simulator.transition(distribution, transformed, _capitalArray, 
           function, transProbs, _model)
       
       transformed
     } 
     
    override def prepareAggregatePolicyCalculation( state : State) {
      
      // Do nothing if simulation has not been requested
      if (_config._gradSimSteps == 0) {
        return
      }
      
      /* Need to update the distributions for calculating grad here
       */
        
      // Don't do this in parallel as the starting point of successive states is the initial point of the last
      for(level <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {
        
        updateGradDensity(level, state)
        
      }
    }
     
     def updateGradDensity(level: Int, state: State) {
       
        val startingProdLevel = (if(level == 0) _config.getAggregateProductivityShocks().numberOfElements() else level) - 1
        val startingDist = state._gradSimStates(startingProdLevel)

        // Expected duration of good/bad periods in this calibration
        // Note that the first period is the 'starting' period and does not count
        val periods = _config._gradSimSteps + 1;

        var shockArray = createIntArrayOfSize(periods, 2)
        var fillArray = createIntArrayOfSize(periods).add(level.asInstanceOf[Integer])
        
        // Set the shock to be simulated to the level the density was requested for
        shockArray.at(-1, 0).fill(fillArray)

        // The first 4 should be the other shock state
        for (j <- 0 until (5)) {
          shockArray.set(startingProdLevel.asInstanceOf[Integer],j,0)
        }        
        
        // Then alternatingly 8 periods each until the last for, which are this shock state
        for (j <- 0 until (periods / 8)) {
          if (j % 2 == 1) {
            for (i <- j * 8+5 until (j + 1) * 8 + 5) {
              shockArray.set(startingProdLevel.asInstanceOf[Integer], i, 0)
            }
          }
        }
        
        state._gradSimStates(level) = _simulator.simulateShocks(startingDist, 
              shockArray, _model, state, 
              SimulationObserver.silent[DiscretisedDistribution, Integer]()).
                getFinalState()
      }
   }

      
   class IM_KS_Solver(model: Model, config: Config) 
     extends KrusellSmithSolver[Config, State, Model, DiscretisedDistribution](
     /*Creating a new Simulator excludes the observers, which are not needed*/
         model, config, new DiscretisedDistributionSimulatorImpl) {
     setSimPeriods(1100)
     setDiscardPeriods(100)
     setNewWeight(.3)
     useLogs(true)
     keepDist(true)
     
     addTransitionListener(
         (oldVal: JavaDoubleArray,newVal: JavaDoubleArray,state: State) => {
       state.setIndividualError(1) // Ensure individual problem is solved again
       println(s"""${state.getAggregateCriterion}""")
     })
   }
      
  class IM_DA_Solver(model: Model, config: Config, 
      sim: DiscretisedDistributionSimulator) extends 
        DerivativeAggregationSolver[Config, State, Model](model, config, sim) {

    // Use a log linear rather than a linear transition
    useLogs(true)

    secondOrder(_config._secondOrder)

    // Damp the aggregate updates down by a factor of 5 to prevent large jumps
    setNewWeight(.2)

    // This grid needs to be the size of the simulation grid
    var _ones: DoubleArray = _
    var _zeros: DoubleArray = _

    // This grid needs to be the size of the simulation grid
    var _thetaDeriv: DoubleArray = _

    val thetaDists = new Array[DiscretisedDistribution](2)

    var _state: State = _

    override def initialise(state: State): Unit = {
      _ones = createArrayOfSize(config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements(),
        config.getIndividualExogenousStates().get(0).numberOfElements())
      _ones += 1d

      _zeros = createArrayOfSize(config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements(),
        config.getIndividualExogenousStates().get(0).numberOfElements())

      // This grid needs to be the size of the simulation grid
      _thetaDeriv = createArrayOfSize(config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements(),
        config.getIndividualExogenousStates().get(0).numberOfElements())

      // The derivative is just the capital holdings at each point
      _thetaDeriv.fillDimensions(config.getIndividualEndogenousStatesForSimulation().get(0), 0)
      
      _state = state
    }

    def deriveAggregationByAggregateState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      // Nothing to do - only one aggregation
      null
    }

    def deriveAggregationByIndividualState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double], current: Boolean): DoubleArray= {
      // The aggregation is just a simple integration, so that the derivative with respect to each individual state is just one
      _ones
    }

    def deriveIndividualTransformationByTheta(transformation: Int, indState: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {

      if(_config._gradSimSteps == 0 || !_updateDTheta) {
        return _thetaDeriv
      }
      
      val currentProdLevel = aggExoStates(0)
      
      val centralDist = _state._gradSimStates( currentProdLevel )
      val centralK = aggregateStates(0)
      
      // Get the distribution used for DA with the requested level of productivity
      var dist = thetaDists(currentProdLevel)//_state._gradSimStates(aggExoStates(0))
      
      _state._dthetaDists(2*currentProdLevel) = dist
      
      // Simulate the same level of productivity for one more period
      val shocks = getNumerics().newIntArray(2,2)
      
      shocks.set(currentProdLevel,0,0)
      shocks.set(currentProdLevel,1,0)

      var exoStatesArray = createIntArray(aggExoStates:_*)
      
      var secondDist = _simulator.simulateShocks(
          centralDist, shocks, _model, _state, 
            SimulationObserver.silent()).getFinalState()
      
      _state._dthetaDists(2*currentProdLevel+1) = secondDist
      
      val K = _model.calculateAggregateStates(dist, exoStatesArray, null)(0)
      val secondK = _model.calculateAggregateStates(secondDist, exoStatesArray, null)(0)
      
      // Calculate the transformed k_i for each k_i, and then the numerical derivative implied
      // Note that this assumes dtheta to be 1 in this case.
      
      val ki = transformedki(dist, secondDist)
      val dki = (ki)\(0) - _config.getIndividualEndogenousStatesForSimulation().get(0)

      dki(0,$) << 0d
      
      // Measuring the dki in the direction away from dist
      dki /= ( secondK - K )
      
      /* The commented section below is an attempt at achieving a second order dtheta, but to no avail
       */
//      if(_config._secondOrder) {
//        
//        val dki1 = transformedki(centralDist,secondDist)\(0) - _config.getIndividualEndogenousStatesForSimulation().get(0)
//        val dK1 = (secondK - centralK) 
//        dki1 /= dK1
//        dki1(0,$) << 0d
//        
//        val dki2 = transformedki(dist,centralDist)\(0) - _config.getIndividualEndogenousStatesForSimulation().get(0)
//        
//        val dK2 = (centralK - K)
//        // Measuring dki2 in the direction away from dist
//        dki2 /= dK2 
//        dki2(0,$) << 0d
//        
//        val factor = 1d/((secondK - K)/2d)*(if(currentProdLevel == 0) -1d else 1d) //* This is from assuming the derivs are accurate at dtheta = 1/2 and (dtheta2)/2*/
//        
//        _state._doubleDerivs(currentProdLevel) = (dki1::dki2)->((a: Double, b: Double) => if(a == 0d || b == 0d) 0d else (a-b)*factor)
//      }
      
      _state._dtheta(currentProdLevel) = dki
      
      return dki
    }

    /**
     * Calculates the cumulative density fn along the wealth dimension from the provided probability density fn
     */
    def toCumulativeDensityFn(pdf: DiscretisedDistribution) : DoubleArray = {
      
      var cdf = createArrayOfSize(pdf._density.size(): _*)

      var cumulative = createArrayOfSize(pdf._density.size()(1))

      // Calculate the cumulative density function of the distribution
      for (i <- 0 until cdf.size()(0)) {
        
        cumulative +=  pdf._density(i,$)

        cdf(i,$) << cumulative
      }
      
      cdf
    }
    
    /**
     * This function essentially computes dk_i by considering the source and the final distribution 
     * after a transformation T is implied. 
     * 
     * It assumes that the transformation is monotonous, so that the order of k_i's is preserved. Thus it computes
     * the corresponding T(k_i) by finding the same point in the cumulative probability density function
     */
    def transformedki(dist: DiscretisedDistribution, transformedDist: DiscretisedDistribution): DoubleArray = {
      
      // Calculate the cumulative density function for the transformed distribution
      var transformedCdf = toCumulativeDensityFn(transformedDist)
      
      var transformed_k_i = createArrayOfSize(transformedDist._density.size(): _*)
      
      // Handle each individual productivity level separately
      for (indShock <- 0 to 1) {

        var currentPos = 0
        var cummulative = 0d

        var lastki =  0d
        
        // For each invidiual wealth point...
        for (indK <- 0 until transformed_k_i.size()(0)) {
          
          // calculate the cdf up to that point in the original dist
          cummulative += dist._density(indK, indShock)

          
          // find the first point in the transformed distribution to exceed that cumulative probability
          while (currentPos < _config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements() &&
                  (cummulative > transformedCdf(currentPos, indShock))) {
            currentPos += 1
          }

          if(Math.abs(cummulative - transformedCdf(indK,indShock)) < 1e-6) {
            transformed_k_i(indK, indShock) = transformed_k_i(indK-1, indShock) + 
              (transformed_k_i(indK-1, indShock) - transformed_k_i(indK-2, indShock))*
              (_config.getIndividualEndogenousStatesForSimulation().get(0).get(indK) - _config.getIndividualEndogenousStatesForSimulation().get(0).get(indK-1))/
              (_config.getIndividualEndogenousStatesForSimulation().get(0).get(indK-1) - _config.getIndividualEndogenousStatesForSimulation().get(0).get(indK-2));
              
          }
          // If the identified point in the transformed distribution is the first one (i.e. the constraint binds), then 
          // the constraint value is the transformed value.
          else if (currentPos == 0) {
            transformed_k_i(indK, indShock) = 0d
          }
          else {
            val lowerDensity = transformedCdf(currentPos - 1, indShock)
            val lowerK = _config.getIndividualEndogenousStatesForSimulation().get(0).get(currentPos - 1)
  
            val (upperK, upperDensity) = 
              // If the transformed grid was exceeded, interpolate to the point between the uppermost wealth level and
              // overflow implied by the two cdf values
              if (currentPos == _config.getIndividualEndogenousStatesForSimulation().get(0).numberOfElements()) {
                
                // The cumulative density will not be 1 because of the proportions in other shock states
                (transformedDist._overflowAverages(0, indShock), lowerDensity + transformedDist._overflowProportions(0,indShock))
              } 
              // Otherwise, interpolate between the point identified and the prior point, which lie either side of the
              // source cdf value
              else {
                (_config.getIndividualEndogenousStatesForSimulation().get(0).get(currentPos), transformedCdf(currentPos, indShock))
              }
            
            val upperProportion =  (cummulative - lowerDensity) / (upperDensity - lowerDensity)
            
            transformed_k_i(indK, indShock) = upperProportion * upperK + (1d - upperProportion) * lowerK      
          }
        }
      }

      transformed_k_i
    }

    override def doubleDeriveAggregationByIndividualStates(aggretationIndex: Int, indIndex: Int, indIndex2: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      _zeros
    }

    override def doubleDeriveAggregationByIndividualAndAggregateState(aggretationIndex: Int, indIndex: Int, aggIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      _zeros
    }

    override def doubleDeriveAggregationByAggregateStates(aggretationIndex: Int, aggIndex: Int, aggIndex2: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
      _zeros
    }

    override def doubleDeriveIndividualTransformationByTheta(transformation: Int, transformation2: Int, indState: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {

      _zeros
//      if( _config._gradSimSteps > 0 ){
//        _state._doubleDerivs(aggExoStates(0))
//      }
//      else
//      {
//          _zeros
//      }
     }

    override def prepareAggregatePolicyCalculation( state : State) {
      
      // Do nothing if simulation has not been requested
      if (_config._gradSimSteps == 0) {
        return
      }
      
      /* Need to update the distributions for calculating grad here
       */
        
      // Don't do this in parallel as the starting point of successive states is the initial point of the last
      for(level <- 0 until _config.getAggregateProductivityShocks().numberOfElements()) {
        
        updateGradDensity(level, state)
        
      }
    }

    def updateGradDensity(level: Int, state: State) {
       
        val startingProdLevel = (if(level == 0) _config.getAggregateProductivityShocks().numberOfElements() else level) - 1
        val startingDist = state._gradSimStates(startingProdLevel)

        // Expected duration of good/bad periods in this calibration
        // Note that the first period is the 'starting' period and does not count
        val periods = _config._gradSimSteps + 1;

        var shockArray = createIntArrayOfSize(periods-1, 2)
        var fillArray = createIntArrayOfSize(periods-1).add(level.asInstanceOf[Integer])
        
        // Set the shock to be simulated to the level the density was requested for
        shockArray.at(-1, 0).fill(fillArray)

        // The first 4 should be the other shock state
        for (j <- 0 until (5)) {
          shockArray.set(startingProdLevel.asInstanceOf[Integer],j,0)
        }        
        
        // Then alternatingly 8 periods each until the last for, which are this shock state
        for (j <- 0 until (periods / 8)) {
        	if (j % 2 == 1) {
            for (i <- j * 8+5 until (j + 1) * 8 + 5) {
              shockArray.set(startingProdLevel.asInstanceOf[Integer], i, 0)
            }
          }
        }
        
        thetaDists(level) = _simulator.simulateShocks(
            startingDist, 
            shockArray, 
            _model, 
            state, 
            SimulationObserver.silent[DiscretisedDistribution, Integer]()
            ).getFinalState()

        shockArray = createIntArrayOfSize(2, 2)
        fillArray = createIntArrayOfSize(2).add(level.asInstanceOf[Integer])
        shockArray.at(-1, 0).fill(fillArray)

        state._gradSimStates(level) = _simulator.simulateShocks(
            thetaDists(level),
            shockArray,
            _model,
            state,
            SimulationObserver.silent[DiscretisedDistribution, Integer]()
            ).getFinalState()
      }
  }

  class State(config: Config) extends AbstractStateBase[Config](config) 
    with DerivAggCalcState[Config] {

    var _normalisedLiquidAssets: DoubleArray = _

    var _simCapitalGrid: DoubleArray = _
    var _simLiquidAssets: DoubleArray = _

    var _expectedInterest: DoubleArray = _
    var _expectedWage: DoubleArray = _

    var _gradSimStates: Array[DiscretisedDistribution] = _

    var _dtheta: Array[DoubleArray] = _
    var _doubleDerivs: Array[DoubleArray] = _

    var _dthetaDists: Array[DiscretisedDistribution] = _

    var _derivativeCalcInfo: Array[DerivationInformation] = _

    override def getIndividualPolicyForSimulation(): DoubleArray = {

      if (_individualTransitionForSimulation == null) {
        // Need to update the policy for the simulation grid, 
        // which is used by derivative aggregation

        _individualTransitionForSimulation = interpolateFunction(
            _normalisedLiquidAssets, getIndividualPolicy(), 0, 
              _simLiquidAssets, params.constrained);
      }

      return _individualTransitionForSimulation
    }

    override def getEndOfPeriodStatesForSimulation(): DoubleArray = _simCapitalGrid

    def getDistributionForState(shockLevels: Array[Int]): DiscretisedDistribution = {
      
      // There is only one shock, and use that one to get the
    		return _gradSimStates(shockLevels(0))
    }

    def setDerivatives(shockIndex: Array[Int], currentAggs: JavaDoubleArray, futureAggs: JavaDoubleArray, derivatives: JavaDoubleArray*): Unit = {

      // Store the set of arrays for the shock level, so they can be saved later
      _derivativeCalcInfo(shockIndex(0)) = (currentAggs,futureAggs, derivatives(0), if (config._secondOrder) derivatives(1) else null)

    }
  }

  class Config extends ModelConfigBase {

    var _growth: Double = 1
    var _discountRate: Double = 1
    var _unemploymentInsuranceRate = 0.15

    var _utilityFunction: UtilityFunction = _
    var _productionFunction: ProductionFunction = _

    var _daDist: DiscretisedDistribution = _

    var _gradSimSteps: Int = 0

    var _secondOrder = false

    var _indSolverClass: Class[_ <: IM_EGM_Solver] = classOf[IM_EGM_Solver]

    def setIndSolverClass[S <: IM_EGM_Solver](clazz: Class[S]): Unit = _indSolverClass = clazz

    def getIndividualCapitalLevels(): DoubleArray = {
      return getIndividualEndogenousStates().get(0);
    }

    def getAggregateCapitalLevels(): DoubleArray = {
      return getAggregateEndogenousStates().get(0);
    }

    def getAggregateProductivityShocks(): DoubleArray = {
      return getAggregateExogenousStates().get(0);
    }

    def getPermanentAggregateShockLevels(): DoubleArray = {
      return getAggregateNormalisingExogenousStates().get(0);
    }

    override def isConstrained: Boolean = hasIndUncertainty

    override def readParameters(x$1: com.meliorbis.numerics.io.NumericsReader): Unit = {

    }

    override def writeParameters(x$1: com.meliorbis.numerics.io.NumericsWriter): Unit = {

    }

    /**
     *  The aggregate problem solver to use; this depends on the configuration:
     *  
     *  - with no individual risk the [[RAAggregateSolver]] solver is used
     *  - with individual risk the default is the [[IM_DA_Solver]] (''Derivative Aggregation'')
     *  - with individual risk and -transformedDist, the [[IM_Transformed_Solver]] is used
     */
    def getAggregateProblemSolver(): Class[_ <: AggregateProblemSolver[State]] = {
      
      if(_indRisk) {
        if (_ks) {
          classOf[IM_KS_Solver]
        }
        else if(_transformedDist) {
          classOf[IM_Transformed_Solver]  
        }
        else {
          classOf[IM_DA_Solver]
        }
      }
      else {
        classOf[RAAggregateSolver[Config, State, RAModel]]
      }
    }
    
    def getIndividualSolver(): Class[IM_EGM_Solver] = classOf[IM_EGM_Solver]
  }
}

