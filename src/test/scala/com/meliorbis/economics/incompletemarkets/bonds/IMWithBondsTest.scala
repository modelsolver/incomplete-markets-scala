package com.meliorbis.economics.incompletemarkets.bonds

import org.jmock.Mockery
import org.junit.Test
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest._
import org.scalatest.Matchers._
import com.meliorbis.numerics.scala.Matchers._
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.economics.infrastructure.Base
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.DoubleArrayFactories._

class IMWithBondsTest extends Base with AssertionsForJUnit {
  
  val DELTA = 1e-10
  val context = new Mockery()
  
  @Test def testUnconditionalExpectations() {
    
    val model = new IMWithBonds.Model()
    val config = new IMWithBonds.Config()
    
    config.setIndividualExogenousStates(createArray(1,2))
    config.setAggregateExogenousStates(createArray(3,4))
    config.setAggregateNormalisingExogenousStates(createArray(1))
    
    config.setIndividualEndogenousStates(createArray(0,.1,.2,.3,.4,.5))
    config.setAggregateEndogenousStates(createArray(2,3,4,5))
    config.setAggregateControls(createArray(.99,.995,.999))
        
    config._bondBorrowingLimit = 1
    
    val transitionProbs = createArrayOfSize(2,2,2,2,1)
    transitionProbs << 1
    transitionProbs\(0,2) *= createArray(.9,.1,.1,.9)
    transitionProbs\(1,3) *= createArray(.8,.2,.2,.8)
    
    config.setExogenousStateTransiton(transitionProbs)
    
    transitionProbs\(2,3,4) sum() should equalArray(createArrayOfSize(2,2)<<1, DELTA)
    model.setConfig(config)
    model.initialise()
    
    var createGrid : (()=>DoubleArray) = null
    
    val egmSolver = new IMWithBonds.IM_EGM_Solver(model, config) {
          createGrid = super.createFutureConditionalContributionsGrid
    }
    egmSolver should not be (null)
    
    val state = new IMWithBonds.State(config)
    egmSolver.initialise(state)
    
    val expectedInterest = model.createAggregateExpectationGrid(1)
    expectedInterest\(1) << (1.1,1.2)
    
    state._expectedInterest = expectedInterest
    
    model.updateBondReturns(state)
    
    val expectations = createGrid()
    expectations\(5) << (.9,.8,.7,.6,.5,.4)
    
    val (expIfBad, expIfGood) = egmSolver.summariseByAggregate(expectations, state)
    
    // Almost, but not completely symmetric, due to the fact that the aggregate state 
    // is persistent
    expIfBad($,$,0) should equalArray(expIfGood($,$,1), DELTA)
    expIfBad($,$,1) should equalArray(expIfGood($,$,0), DELTA)
    
    // Taking the asymmetry into account: B->B 4x more likely than B->G
    expIfBad($,$,0) should equalArray(expIfBad($,$,1)*4, DELTA)
    
    val multiplier = egmSolver.badExpectationMultiplier( state )
    
    // p_t, A_t,Z_t+1, K_t, 1
    multiplier.size should equal(Array(3,2,1,4,1))
    
    // Constancy of expected interest makes this pretty easy to check
    val expectedMult = (1.1 - state._bondReturns)/(state._bondReturns - 1.2)
    
    multiplier should equalArray(expectedMult, DELTA)
    
//    val uncond = egmSolver.individualUnconditionalExpectation(expectations, state)
//    
//    uncond should equalArray(uncond, DELTA)
  }
  
}