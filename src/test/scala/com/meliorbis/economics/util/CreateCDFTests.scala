package com.meliorbis.economics.util

import org.scalatest.junit.AssertionsForJUnit
import org.junit.Test
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.scala.DoubleArray._
import org.scalatest._
import org.scalatest.Matchers._
import com.meliorbis.numerics.scala.Matchers._
import org.junit.Before
import com.meliorbis.numerics.DoubleArrayFactories

class CreateCDFTests extends AssertionsForJUnit {
  
  val _numerics = DoubleNumerics.instance()
  
  @Test
  def testCreatesCDF() {
    
    val dist = new DiscretisedDistribution()
    
    dist._density = DoubleArrayFactories.createArrayOfSize(10,1)
    
    dist._density << .1
    
    val cdf = Util.toCumulativeDensityFn(dist)
    
    cdf should equalArray (DoubleArrayFactories.createArrayOfSize(10,1) << (.1 to (1d~9):_*),1e-9)
  }
}