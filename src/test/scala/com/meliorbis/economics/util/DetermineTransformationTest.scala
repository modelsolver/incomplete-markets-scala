package com.meliorbis.economics.util

import org.scalatest.junit.AssertionsForJUnit
import org.junit.Test
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.numerics.scala.Matchers._
import org.scalatest._
import org.scalatest.Matchers._
import org.junit.Before
import com.meliorbis.numerics.DoubleArrayFactories

/**
 * @author toby
 */
class DetermineTransformationTest extends AssertionsForJUnit {
  
    val _numerics = DoubleNumerics.instance()
    
    /**
     * Source and target distributions to calculate the transformations from
     */
    var _source : DiscretisedDistribution = _
    var _target : DiscretisedDistribution = _
    
    /**
     * The endogenous var levels along dimension 0
     */
    var _levels : DoubleArray = _
    
    @Before
    def setUp() {
      _source = new DiscretisedDistribution()
      
      _source._density = DoubleArrayFactories.createArrayOfSize(10,1)
      _source._overflowAverages = DoubleArrayFactories.createArrayOfSize(1,1)
      _source._overflowProportions = DoubleArrayFactories.createArrayOfSize(1,1)
      
      _target = _source.createSameSized()
      
      _levels = DoubleArrayFactories.createArrayOfSize(10)
      _levels << (1d to 10d~9:_*)
    }
    
    @Test
    def testCorrectSize() {
      
      val res = Util.determineTransformation(_source,_target,_levels)
      
      res should not be (null)
      
      res.size() should be (Array(10,1))
    }
    
    @Test(expected = classOf[Exception])
    def testInconsistentSizesCauseException() {
      
      val wrongTarget = new DiscretisedDistribution()
      wrongTarget._density = DoubleArrayFactories.createArrayOfSize(9,1)
        
      val res = Util.determineTransformation(_source,wrongTarget,_levels)
    }
    
    @Test(expected = classOf[Exception])
    def testWrongLevelSizeCauseException() {
        
      val wrongLevels = DoubleArrayFactories.createArrayOfSize(8)
      val res = Util.determineTransformation(_source,_target,wrongLevels)
    }
    
    @Test
    def sameDistResultsInIdentity() {
      
      _source._density << .1 
      _target._density << .1
      
      val res = Util.determineTransformation(_source,_target,_levels)
      
      res($,0) should equalArray (_levels,1e-9)
    } 
    
    @Test
    def constrainAtLowerBound() {
      
      _source._density << .09 
      _target._density << .05
      _target._density(0) = .2
      
      val res = Util.determineTransformation(_source,_target,_levels)
      
      res(0,0) should equal (_levels(0))
      res(1,0) should equal (_levels(0))
      
      // should no longer be constrained
      res(2,0) should equal (_levels(1) + (_levels(2) - _levels(1))*.4 +- 1e-9)
    } 
    
    @Test
    def interpolateBetweenGridPoints() {
      
      _source._density << .09 
      _target._density << .09
      _target._density(9) = .1
      
      val res = Util.determineTransformation(_source,_target,_levels)
      
      val expected = _levels.copy()
      expected(9) = _levels(8) + .9 * (_levels(9) - _levels(8))
      
      res($,0) should equalArray (expected,1e-9)
    } 
}